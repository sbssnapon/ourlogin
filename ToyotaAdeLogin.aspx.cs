using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace PortalManager
{
    public partial class ToyotaAdeLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            switch (rdoEnvironment.SelectedValue.ToString())
            {
                case "TNEBETA":
                    Response.Redirect("https://beta.toyotatoolsandequipment.com?tok=" + rdoDealer.SelectedValue, false);
                    break;

                case "TNEPROD":
                    Response.Redirect("https://toyotatoolsandequipment.com?tok=" + rdoDealer.SelectedValue, false);
                    break;

                case "ADEBETA":
                    Response.Redirect("https://toyotaadebeta.snapon.com?tok=" + rdoDealer.SelectedValue, false);
                    break;

                case "ADEPROD":
                    Response.Redirect("https://toyotaade.snapon.com?tok=" + rdoDealer.SelectedValue, false);
                    break;

                case "LTNEBETA":
                    Response.Redirect("https://beta.toyotatoolsandequipment.com?tok=" + rdoDealer.SelectedValue, false);
                    break;

                case "LTNEPROD":
                    Response.Redirect("https://toyotatoolsandequipment.com?tok=" + rdoDealer.SelectedValue, false);
                    break;

                case "LADEBETA":
                    Response.Redirect("https://toyotaadebeta.snapon.com?tok=" + rdoDealer.SelectedValue, false);
                    break;

                case "LADEPROD":
                    Response.Redirect("https://toyotaade.snapon.com?tok=" + rdoDealer.SelectedValue, false);
                    break;
            }
            
        }

    }
}
