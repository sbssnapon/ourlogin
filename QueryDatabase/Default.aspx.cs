using System;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Data.SqlClient;

namespace QueryDatabase
{
    public partial class _Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            tdErrorMessage.InnerText = string.Empty;

            if (!Page.IsPostBack)
            {
                CheckAccess();
                initialize();
            }
        }

        private void CheckAccess()
        {
            // The user should have this in session after having just verified using Authy. 
            if (Session["SnaponSbsPortalManagerOtpVerified"] == null)
            {
                FormsAuthentication.SignOut();
                Response.Redirect("/OurLogin/Login.aspx?ReturnUrl=/TestScripts/QueryDatabase/Default.aspx", false);
            }
        }

        private void initialize()
        {
            string url = Request.Url.AbsoluteUri.ToLower();

            if (!removedSelections())
            {
                if (url.Contains("sbsprograms.snapon.com"))
                {
                    if (lstEnvironment.Items[0].Value.Contains("KENO-MSQL"))
                    {
                        lstEnvironment.Items[0].Value = "LISL-MSQL-14-PV\\SBS";
                        lstEnvironment.Items[0].Text = "LISL-MSQL-14-PV\\SBS";
                        //lstEnvironment.Items[1].Value = "LISL-MSQL-05-PV";
                        //lstEnvironment.Items[1].Text = "LISL-MSQL-05-PV";
                    }
                    hrefServer.NavigateUrl = "https://sbsprogramsrd.snapon.com/TestScripts/QueryDatabase/Default.aspx";
                    hrefServer.Text = "GO TO KENO-MSQL-15-TV\\SBS";
                }
            }
        }

        private bool removedSelections()
        {
            string userName = HttpContext.Current.User.Identity.Name.ToLower();
            bool RC = false;

            switch(userName)
            {
                case "xf0247":
                case "bm8968":
                    {
                        for (int iRow = 0; iRow < lstEnvironment.Items.Count; iRow++)
                        {
                            if (lstEnvironment.Items[iRow].Value.Equals("LISL-MSQL-14-PV\\SBS"))
                            {
                                lstEnvironment.Items.RemoveAt(iRow);
                            }
                        }
                        hrefServer.Visible = false;
                    }
                    RC = true;
                    break;
            }
            return RC;
        }


        protected void btnGO_Click(object sender, EventArgs e)
        {
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
                tdErrorMessage.InnerText = "Please enter user name/password to login";
            else if (lstQueryType.Value == "read" || chkTableList.Checked)
                GetData();
            else
                UpdateData();
        }


        private void GetData()
        {
            tdErrorMessage.InnerText = string.Empty;
            DataSet ds = new DataSet();

            try
            {
                using (SqlConnection conn = GetSQLConnection())
                {

                    SqlDataAdapter sqlDbDA = new SqlDataAdapter();
                    SqlCommand sqlDbCmd;

                    if (chkColumnList.Checked)
                        sqlDbCmd = new SqlCommand("select column_name,* from information_schema.columns where table_name = '" + txtQuery.Text + "' order by ordinal_position", conn);
                    else if (chkTableList.Checked)
                        sqlDbCmd = new SqlCommand("select * from sys.tables order by name", conn);
                    else
                        sqlDbCmd = new SqlCommand(txtQuery.Text, conn);

                    sqlDbCmd.CommandTimeout = 0;
                    sqlDbDA.SelectCommand = sqlDbCmd;
                    sqlDbDA.Fill(ds, "gvResults");
                    sqlDbCmd.Dispose();

                    gvResults.DataSource = ds;
                    gvResults.DataBind();
                    Response.Write(gvResults);
                    Response.Flush();
                    gvResults.Dispose();
                    ds.Dispose();

                    if (ds.Tables[0].Rows.Count == 0)
                        tdErrorMessage.InnerText = "No data found!";
                }

            }
            catch (Exception ex)
            {
                tdErrorMessage.InnerText = "impersonate  " + System.Security.Principal.WindowsIdentity.GetCurrent().Name + System.Environment.NewLine + System.Environment.NewLine;
                tdErrorMessage.InnerText += ex.ToString();
            }
        }

        private SqlConnection GetSQLConnection()
        {
            string dbConnection = string.Empty;

            if (IsIntegratedSecurityServer(lstDatabase.Value.ToString().ToLower()))
            {
                dbConnection = "Data Source=" + lstEnvironment.Value.ToString() + ";Initial Catalog=" + lstDatabase.Value.ToString() + ";Integrated Security=SSPI;";
            }
            else
                dbConnection = "Data Source=" + lstEnvironment.Value.ToString() + ";Initial Catalog=" + lstDatabase.Value.ToString() + ";user id = eqsuser;password =Luv2wrk";

            SqlConnection oconn = new SqlConnection(dbConnection);
            oconn.Open();

            return oconn;
        }

        private bool IsIntegratedSecurityServer(string databaseName)
        {
            Boolean RC = false;

            switch (databaseName.Substring(0,4))
            {
                case "sbs_":
                    RC = true;
                    break;
            }
            return RC;
        }

        private void UpdateData()
        {
            tdErrorMessage.InnerText = string.Empty;

            try
            {
                using (SqlConnection conn = GetSQLConnection())
                {

                    SqlDataAdapter sqlDbDA = new SqlDataAdapter();
                    SqlCommand sqlDbCmd = new SqlCommand(txtQuery.Text, conn);

                    sqlDbCmd.CommandTimeout = 0;
                    sqlDbCmd.ExecuteNonQuery();
                    sqlDbCmd.Connection.Close();
                    sqlDbCmd.Dispose();
                    tdErrorMessage.InnerText = "Successful update to table!";
                }
            }
            catch (Exception ex)
            {
                tdErrorMessage.InnerText = ex.ToString();
            }
        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                Response.ContentType = "application/vnd.ms-excel";
                // Show download dialog to Open/Save with MIME type dialog
                Response.AddHeader("content-disposition", "attachment; filename=QueryResults.xls");
                pnlIputs.Visible = false;
                GetData();
            }
            else
            {
                tdErrorMessage.InnerText = "Please enter user name/password to login";
            }
        }

        protected void chkColumnList_CheckedChanged(object sender, EventArgs e)
        {
            if (chkColumnList.Checked)
                txtQuery.Text = "Enter table name here";
            else
                txtQuery.Text = string.Empty;
        }

    }
}
