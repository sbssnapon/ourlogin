<%@ Page Language="C#" AutoEventWireup="true" Codebehind="Default.aspx.cs" Inherits="QueryDatabase._Default" ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Query Database</title>
    <style type="text/css">
        #txtQuery
        {
            width: 442px;
            height: 85px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server" style="background-color: Silver;">
        <table style="font-family: Verdana; width: 100%; font-size: 9pt;" cellspacing="0">
            <asp:Panel ID="pnlIputs" runat="server" Visible="true">
                <tr bgcolor="#4A3C8C">
                    <td colspan="2">
                        <img src="/Images/spacer.gif" width="300pt" height="1pt" />
                        <asp:Label ID="Label1" runat="server" Font-Bold="true" ForeColor="White" Font-Size="13pt" Text="Query Database Tool"></asp:Label>
                    </td>
                 </tr>
                <tr>
                    <td id="tdSelection" runat="server">
                        <br />
                        <br />
                        <table style="font-family: Verdana; width: 550pt; height: 170pt; font-size: 9pt;"
                            cellspacing="0">
                            <tr style="color: #F7F7F7; background-color: #4A3C8C;">
                                <td>
                                    &nbsp;</td>
                                <td>
                                    <label>
                                        <b>Server:</b></label><br />
                                    <select id="lstEnvironment" runat="server" style="width: 220px">
                                        <option value="KENO-MSQL-10-TV\SBS">KENO-MSQL-10-TV\SBS</option>
                                        <option value=""></option>
                                    </select>
                                    <br /><br />
                                    <asp:HyperLink ID="hrefServer" runat="server" ForeColor="Red"  NavigateUrl="https://sbsprograms.snapon.com/TestScripts/QueryDatabase/Default.aspx" Text="GO TO LISL-MSQL-14-PV\SBS"></asp:HyperLink>
                                    <br /><br />
                                    <label>
                                        <b>Database:</b></label><br />
                                    <select id="lstDatabase" runat="server" style="width: 220px">
                                        <option value="SBS_Addresses">SBS_Addresses</option>  
                                        <option value="SBS_GMCatManEquipment">SBS_GMCatManEquipment</option>                                                                                       
                                        <option value="SBS_GMEcommerce">SBS_GMEcommerce</option>    
                                        <option value="SBS_HdCatManEquipment">SBS_HdCatManEquipment</option>                                                                                                                         
                                        <option value="SBS_HdEcommerce">SBS_HdEcommerce</option>    
                                        <option value="SBS_HondaCatalog">SBS_HondaCatalog</option>  
                                        <option value="SBS_HondaCatalogMarine">SBS_HondaCatalogMarine</option>  
                                        <option value="SBS_HondaCatalogMotorcycle">SBS_HondaCatalogMotorcycle</option>  
                                        <option value="SBS_HondaCatalogPowerEquipment">SBS_HondaCatalogPowerEquipment</option>  
                                        <option value="SBS_HondaEcommerceLAC">SBS_HondaEcommerceLAC</option>  
                                        <option value="SBS_HondaEcommerceMarine">SBS_HondaEcommerceMarine</option> 
                                        <option value="SBS_HondaEcommerceMotorcycle">SBS_HondaEcommerceMotorcycle</option>
                                        <option value="SBS_HondaEcommercePowerEquipment">SBS_HondaEcommercePowerEquipment</option>
                                        <option value="SBS_HondaEcommerceUSA">SBS_HondaEcommerceUSA</option>   
                                        <option value="SBS_HondaForms">SBS_HondaForms</option>   
                                        <option value="SBS_HondaSurvey">SBS_HondaSurvey</option>   
                                        <option value="SBS_IndQuoting">SBS_IndQuoting</option>   
                                        <option value="SBS_IndTaaCoo">SBS_IndTaaCoo</option>                                                                                
                                        <option value="SBS_InventoryManagement">SBS_InventoryManagement</option>
                                        <option value="SBS_KiaCatManEquipment">SBS_KiaCatManEquipment</option>
                                        <option value="SBS_KiaCatManTools">SBS_KiaCatManTools</option>
                                        <option value="SBS_KiaEcommerce">SBS_KiaEcommerce</option>
                                        <option value="SBS_KiaTechInfo">SBS_KiaTechInfo</option>
                                        <option value="SBS_MoparCatManEquipment">SBS_MoparCatManEquipment</option>    
                                        <option value="SBS_MoparCatManTools">SBS_MoparCatManTools</option>    
                                        <option value="SBS_MoparEcommerce">SBS_MoparEcommerce</option>    
                                        <option value="SBS_MoparEuroCatManEquipment">SBS_MoparEuroCatManEquipment</option>    
                                        <option value="SBS_MoparEuroEcommerce">SBS_MoparEuroEcommerce</option>    
                                        <option value="SBS_MoparSurvey">SBS_MoparSurvey</option>    
                                        <option value="SBS_MoparTechAuthority">SBS_MoparTechAuthority</option>    
                                        <option value="SBS_PorscheCatManEquipment">SBS_PorscheCatManEquipment</option>    
                                        <option value="SBS_PorscheCatManTools">SBS_PorscheCatManTools</option>    
                                        <option value="SBS_PorscheEcommerce">SBS_PorscheEcommerce</option>    
                                        <option value="SBS_PortalApps">SBS_PortalApps</option>    
                                        <option value="SBS_Pricing">SBS_Pricing</option>  
                                        <option value="SBS_RivianEcommerce">SBS_RivianEcommerce</option>  
                                        <option value="SBS_SalesAnalysis">SBS_SalesAnalysis</option>
                                        <option value="SBS_SharedResource">SBS_SharedResource</option>  
                                        <option value="SBS_ToyotaCatManEquipment">SBS_ToyotaCatManEquipment</option>                                              
                                        <option value="SBS_ToyotaCatManEquipmentCaEn">SBS_ToyotaCatManEquipmentCaEn</option>                                              
                                        <option value="SBS_ToyotaCatManEquipmentCaFc">SBS_ToyotaCatManEquipmentCaFc</option>                                              
                                        <option value="SBS_ToyotaEcommerce">SBS_ToyotaEcommerce</option>                                              
                                        <option value="SBS_ToyotaEcommerceCaEn">SBS_ToyotaEcommerceCaEn</option>                                              
                                        <option value="SBS_ToyotaEcommerceCaFc">SBS_ToyotaEcommerceCaFc</option>                                    
                                        <option value="SBS_ToyotaEcommerceTechstream">SBS_ToyotaEcommerceTechstream</option>                                                                                      
                                        <option value="SBS_ToyotaTechInfo">SBS_ToyotaTechInfo</option>                                              
                                        <option value="SBS_VwCatManEquipment">SBS_VwCatManEquipment</option>  
                                        <option value="SBS_VwCatManTools">SBS_VwCatManTools</option>
                                        <option value="SBS_VwEcommerce">SBS_VwEcommerce</option>
                                    </select>
                                    <br />
                                    <br />
                                    <label>
                                        <b>Query Type:</b></label><br />
                                    <select id="lstQueryType" runat="server" style="width: 220px">
                                        <option value="read" selected>Read</option>
                                        <option value="update">Update</option>
                                    </select>
                                    <br />
                                    <br />
                                    <table>
                                        <tr><td width="130pt">
                                        <label>
                                            <b>Table List</b></label><br />
                                        <asp:CheckBox ID="chkTableList" runat="server" />
                                        </td>
                                        <td>
                                        <label>
                                            <b>Column List</b></label><br />
                                        <asp:CheckBox ID="chkColumnList" runat="server" AutoPostBack="true" oncheckedchanged="chkColumnList_CheckedChanged" />
                                        </td></tr>
                                    </table>
                                    <br />
                                    <br />
                                </td>
                                <td valign="top">
                                    <b>Input Query:</b><br />
                                    <asp:TextBox ID="txtQuery" runat="server" TextMode="MultiLine"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="3" align="center">
                                    <asp:Button ID="btnExportToExcel" runat="server" SkinID="BtnSkin2" Text="Export to Excel"
                                        OnClick="btnExportToExcel_Click" />
                                    <asp:Button ID="btnGO" runat="server" Text="GO" OnClick="btnGO_Click" SkinID="BtnSkin2" /></td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td id="tdErrorMessage" runat="server" colspan="3" style="color:red;font-size:11pt;" >
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </asp:Panel>
            <tr>
                <td>
                    <asp:GridView ID="gvResults" runat="server" BackColor="White" Font-Size="8pt" BorderColor="#E7E7FF"
                        BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Horizontal">
                        <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
                        <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                        <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                        <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                        <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
                        <AlternatingRowStyle BackColor="#F7F7F7" />
                    </asp:GridView>
                </td>
            </tr>
            <tr bgcolor="#4A3C8C"><td align="center">&nbsp;</td></tr>
        </table>
    </form>
</body>
</html>

