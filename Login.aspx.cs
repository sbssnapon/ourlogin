using System.Web.Security;
using System;
using System.Web;
using PortalManager.Helpers;
using log4net;
using System.Reflection;

namespace PortalManager.OurLogin
{
    public partial class Login : BaseEcommercePage
    {
        public string _googleRecapchaSiteKey = System.Configuration.ConfigurationManager.AppSettings["GoogleReCaptchaSiteKey"];
       // static ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        string _redirectUrl = string.Empty;

       protected void Page_Load(object sender, EventArgs e)
       {
           clearMessage(lblErrorMessage, divErrorMessage);

           if (Page.IsPostBack) return;

           SetHeading();
           UserName.Focus();
           LoginCredentials();

        }

        private void SetHeading()
       {
           _redirectUrl = FormsAuthentication.GetRedirectUrl(string.Empty, false);

           switch (_redirectUrl.ToLower())
           {
               case "/ourlogin/default.aspx": 
                   h1Heading.InnerText = "Catalog & Ecommerce Web Site Login";
                   OneTimePasswordPanel.Visible = false;
                   break;
               case "/querydatabase/default.aspx":
                   h1Heading.InnerText = "Query Database Login";
                   break;
           }
       }

        private void LoginCredentials()
       {
           var membershipProvider = new BOMembershipProvider();
           var dal = new DataAccessLayer();

           try
           {
               //if (_redirectUrl.ToLower().Equals("/") || _redirectUrl.ToLower().Equals("/default.aspx") || Request.Url.PathAndQuery == "/")
               //{
               //     // This page is using ExtranetLinks.aspx to authorize login
               //     FormsAuthentication.SetAuthCookie(string.Empty, false);
               //     FormsAuthentication.RedirectFromLoginPage(string.Empty, false);
               //}
               if (UserName.Text.Length == 0 && Password.Text.Length == 0)
               {
                   if (HttpContext.Current.Request.Cookies["SnaponSbsPortalManagerApplication"] != null)
                   {
                       UserName.Text = HttpContext.Current.Request.Cookies["SnaponSbsPortalManagerApplication"]["UserName"];
                   }
               }

               if (UserName.Text.Length <= 0 || Password.Text.Length <= 0) return;

               // check Google Captcha is valid
               var reCaptchaErrors = String.Empty;
               if (!ReCaptchaHelpers.UserCompletedReCaptcha(Request, UserName.Text, out reCaptchaErrors))
               {
                   showMessage(lblErrorMessage, divErrorMessage,
                       String.IsNullOrEmpty(reCaptchaErrors)
                           ? "Please complete the reCAPTCHA before trying to login."
                           : "reCAPTCHA Error(s): " + reCaptchaErrors);
                   Page.Form.DefaultFocus = Password.ClientID;
                   return;
               }

               var expiration = DateTime.Now.AddDays(360);

               if (membershipProvider.ValidateUser(UserName.Text, Password.Text))
               {

                   if (HttpContext.Current.Request.IsLocal)
                        Session["SnaponSbsPortalManagerOtpVerified"] = true; // Cannot use OTP locally
                    else if (!string.IsNullOrEmpty(OneTimePassword.Text))
                   {
                       // Need to check OTP for query page users as well.
                       var authyUserId = dal.GetAuthyUserId(UserName.Text);

                       if (authyUserId == null)
                       {
                           UserName.Focus();
                           showMessage(lblErrorMessage, divErrorMessage, "This user is not set up in Authy.");
                           return;
                       }

                       string message;

                       /* Temporarily commenting out because Authy is not working for some people.
                       if (!AuthyHelper.VerifyOneTimePassword(authyUserId, OneTimePassword.Text, out message))
                       {
                           UserName.Focus();
                           showMessage(lblErrorMessage, divErrorMessage, "Authy Error: " + message);
                           return;
                       }*/
                       

                        var tempOTP = "$napONRul3s" + DateTime.Now.ToString("MMddyyyy");
                        if (!OneTimePassword.Text.Equals(tempOTP))
                        {
                            UserName.Focus();
                            showMessage(lblErrorMessage, divErrorMessage, "OTP Error: Invalid OTP!");
                            return;
                        }

                        Session["SnaponSbsPortalManagerOtpVerified"] = true;
                       expiration = DateTime.Now.AddMinutes(30);
                   }
                    
                    Session["SnaponSbsPortalManagerOtpVerified"] = true;
                    expiration = DateTime.Now.AddMinutes(30);

                    SaveCredentials(UserName.Text, expiration);
                    FormsAuthentication.SetAuthCookie(UserName.Text, false);
                   FormsAuthentication.RedirectFromLoginPage(UserName.Text, false);
				   Session["OurLoginValid"] = "true";
                }
               else
               {
                   UserName.Focus();
                   showMessage(lblErrorMessage, divErrorMessage, "Your login attempt was not successful. Please try again.");
               }
           }
	       catch (Exception ex)
           {
               showMessage(lblErrorMessage, divErrorMessage, ex.Message);
           }
       }
        
        private void SaveCredentials(string username, DateTime expiration)
        {
            var cookie = new HttpCookie("SnaponSbsPortalManagerApplication");

            cookie.Values.Add("UserName", username);
            cookie.Expires = expiration;
            HttpContext.Current.Response.AppendCookie(cookie);
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            LoginCredentials();
        }
    }
}
