using System;
using System.Data;
using System.Globalization;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Text;
using PortalManager.BusinessObjects;
using System.Web;

namespace PortalManager
{
    public class DataAccessLayer
    {
        private const string CSP_GET_SITE_ROLES = "csp_getSiteRoles";
        private static string _ItemNumber = string.Empty;
        private static string _SearchItemNumber = string.Empty;

        private string GetURLBasedConnectionKey()
        {
            string url = System.Web.HttpContext.Current.Request.Url.AbsoluteUri;

            string connectionKey = "dev_EqsPortalApps";
            if (url.Contains("www.") || url.Contains("sbsprograms."))
            {
                connectionKey = "prod_EqsPortalApps";
            }
            return connectionKey;

        }

        private string GetOleDbConnectionKey(string connectionType)
        {
            string connectionString = string.Empty;

            switch (connectionType)
            {
                case "EqsActiveSuppliers":
                    connectionString = "prod_EqsActiveSuppliers";
                    break;
                case "OracleBaanOliBaanProd":
                    connectionString = "prod_Oracle_BAAN_OliBaanProd";
                    break;
                case "OracleLnDwProd":
                    connectionString = "prod_Oracle_LN_DwProd";
                    break;
                case "OracleLnQuery":
                    connectionString = "prod_Oracle_LN_Query";
                    break;
            }
            return connectionString;

        }

        private SqlConnection GetSQLConnection()
        {

            string connectkey = GetURLBasedConnectionKey();
            SqlConnection oconn =
                new SqlConnection(
                    System.Configuration.ConfigurationManager.ConnectionStrings[connectkey].ConnectionString);
            
            oconn.Open();
            return oconn;

        }

        private OleDbConnection GetOleDbConnection(string connectionType)
        {

            string connectkey = GetOleDbConnectionKey(connectionType);
            OleDbConnection oconn =
                new OleDbConnection(
                    System.Configuration.ConfigurationManager.ConnectionStrings[@connectkey].ConnectionString);
            oconn.Open();
            return oconn;
        }

        public void InsertData(string sql)
        {

            SqlCommand sqlcmd = new SqlCommand();
            sqlcmd.Connection = GetSQLConnection();
            sqlcmd.CommandText = sql;
            sqlcmd.ExecuteNonQuery();
            sqlcmd.Connection.Close();

        }

        /// <summary>
        /// First authenticate external user for the site
        /// Secondly authenticate interal user for the site
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public string IsAuthenticated(string username, string password)
        {
            string RC = "invalid_login";

            if (HttpContext.Current.Request.IsLocal  || AuthenticationManager.AuthenticateExtranetUser(username, password))
            {
                using (SqlConnection connection = GetSQLConnection())
                {
                    string sql = "Select userID from tblPortalUserAccounts where UserID = @userId";

                    SqlDataAdapter da = new SqlDataAdapter(sql, connection);
                    da.SelectCommand.Parameters.Add("@userId", SqlDbType.VarChar, 100);
                    da.SelectCommand.Parameters["@userId"].Value = username;
                    da.SelectCommand.Parameters.Add("@password", SqlDbType.VarChar, 100);
                    da.SelectCommand.Parameters["@password"].Value = password;
                    da.SelectCommand.CommandTimeout = 0;
                    DataSet ds = new DataSet();
                    da.Fill(ds, "tblPortalUsers");

                    if (ds.Tables[0].Rows.Count > 0) RC = "valid_internal_user";
                 }
            }
            return RC;
        }

        /// <summary>
        /// Increment logincounter each time user logs in
        /// </summary>
        /// <param name="index"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        public void SetCounter(string username, string password)
        {
            using (SqlConnection connection = GetSQLConnection())
            {
                string currentDate = DateTime.Today.ToShortDateString();
                int index = 0;

                string strCounterSql =
                    "select logincounter from tblportaluseraccounts where UserID =@userId and Password =@password ";

                SqlCommand cmd = new SqlCommand(strCounterSql, connection);
                cmd.Parameters.Add(new SqlParameter("@password", SqlDbType.VarChar, 50));
                cmd.Parameters["@password"].Value = password;

                cmd.Parameters.Add(new SqlParameter("@userId", SqlDbType.VarChar, 100));
                cmd.Parameters["@userId"].Value = username;

                object obj = cmd.ExecuteScalar();

                if (obj != System.DBNull.Value)
                {
                    index = Convert.ToInt32(obj.ToString());
                }

                index += 1;

                string strSql =
                    "update tblPortalUserAccounts set firstdate = @currentDate where UserID =@userId and Password =@password and firstdate is null";
                string sql =
                    "update tblPortalUserAccounts set logincounter = @index  where UserID =@userId and Password =@password";

                SqlCommand sqlCmd = new SqlCommand(sql, connection);

                sqlCmd.Parameters.Add("@userId", SqlDbType.VarChar, 100);
                sqlCmd.Parameters["@userId"].Value = username;

                sqlCmd.Parameters.Add("@password", SqlDbType.VarChar, 50);
                sqlCmd.Parameters["@password"].Value = password;

                sqlCmd.Parameters.Add("@index", SqlDbType.Int);
                sqlCmd.Parameters["@index"].Value = index;

                int recAffected = sqlCmd.ExecuteNonQuery();

                SqlCommand sqlDateCmd = new SqlCommand(strSql, connection);

                sqlDateCmd.Parameters.Add("@userId", SqlDbType.VarChar, 100);
                sqlDateCmd.Parameters["@userId"].Value = username;

                sqlDateCmd.Parameters.Add("@password", SqlDbType.VarChar, 50);
                sqlDateCmd.Parameters["@password"].Value = password;

                sqlDateCmd.Parameters.Add("@currentDate", SqlDbType.DateTime);
                sqlDateCmd.Parameters["@currentDate"].Value = currentDate;

                int recsAffected = sqlDateCmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// To list all site links
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public DataSet GetSiteNames(string username)
        {
            using (SqlConnection connection = GetSQLConnection())
            {

                string sql =
                    "Select sitename,sitelink,siteuserid,sitepassword,showuserpwd,description from tblsiteuserinfo s " +
                    "inner join tblportaluseraccounts p on p.UserID=s.UserID where p.userid=@userId";
                SqlDataAdapter da = new SqlDataAdapter(sql, connection);
                da.SelectCommand.Parameters.Add("@userId", SqlDbType.VarChar, 100);
                da.SelectCommand.Parameters["@userId"].Value = username;
                da.SelectCommand.CommandTimeout = 0;
                DataSet ds = new DataSet();
                da.Fill(ds, "tblsitenames");

                return ds;
            }
        }

        /// <summary>
        /// To get information for last password request
        /// </summary>
        /// <param name="firstname"></param>
        /// <param name="lastname"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        public DataSet GetEmailInfo(string firstname, string lastname, string email)
        {
            using (SqlConnection connection = GetSQLConnection())
            {
                string sql =
                    "Select firstname,lastname,email,userid,password from tblportaluseraccounts p where p.firstname =@firstName  and p.lastname=@lastName and p.email=@email";

                SqlDataAdapter da = new SqlDataAdapter(sql, connection);
                da.SelectCommand.Parameters.Add("@firstName", SqlDbType.VarChar, 50);
                da.SelectCommand.Parameters["@firstName"].Value = firstname;

                da.SelectCommand.Parameters.Add("@lastName", SqlDbType.VarChar, 50);
                da.SelectCommand.Parameters["@lastName"].Value = lastname;

                da.SelectCommand.Parameters.Add("@email", SqlDbType.VarChar, 50);
                da.SelectCommand.Parameters["@email"].Value = email;
                da.SelectCommand.CommandTimeout = 0;

                DataSet ds = new DataSet();
                da.Fill(ds, "tblEmailinfo");

                return ds;
            }
        }

        public DataSet GetUsers(string sitename)
        {
            string sql = string.Empty;

            DataSet ds = new DataSet();
            using (SqlConnection conn = GetSQLConnection())
            {
                if (sitename == "All")
                {
                    sql =
                        "select firstname + ' ' + lastname as Name,username ,s.sitename,r.Role,u.userId from tblExtranetUsers u inner join tblextranetusersiteroles r on r.userid = u.userid " +
                        "inner join tblextranetsiteurl s on r.siteid = s.siteid order by lastname,firstname,sitename";
                }
                else if (sitename != null)
                {
                    sql =
                        "select username ,r.Role,s.sitename,firstname + ' ' + lastname as Name,u.userId from tblExtranetUsers u inner join tblextranetusersiteroles r on r.userid = u.userid " +
                        "inner join tblextranetsiteurl s on r.siteid = s.siteid and s.sitename = @sitename order by username";
                }
                else
                {
                    sql =
                        "select username ,'' as role,'' as sitename,firstname + ' ' + lastname as Name,u.userId from tblExtranetUsers order by username";
                }

                SqlCommand sqlCmd = new SqlCommand(sql, conn);
                if (sitename != null)
                {
                    SqlParameter paramSite = new SqlParameter("@sitename", SqlDbType.VarChar, 50);
                    paramSite.Value = sitename;
                    sqlCmd.Parameters.Add(paramSite);
                }
                SqlDataAdapter sqlDad = new SqlDataAdapter();
                sqlDad.SelectCommand = sqlCmd;
                sqlDad.SelectCommand.CommandTimeout = 0;
                sqlDad.Fill(ds, "Users");

            }
            return ds;
        }

        public DataSet SearchUsers(string searchTerm)
        {
            string sql = string.Empty;

            DataSet ds = new DataSet();
            using (SqlConnection conn = GetSQLConnection())
            {
                sql =
                    "select firstname + ' ' + lastname as Name,username ,s.sitename,r.Role,u.userId from tblExtranetUsers u inner join tblextranetusersiteroles r on r.userid = u.userid " +
                    "inner join tblextranetsiteurl s on r.siteid = s.siteid where (FirstName + ' ' + LastName) like @searchTerm OR username like @searchTerm order by lastname,firstname,sitename";

                SqlCommand sqlCmd = new SqlCommand(sql, conn);
                SqlParameter paramSite = new SqlParameter("@searchTerm", SqlDbType.VarChar, 50);
                paramSite.Value = "%" + searchTerm + "%";
                sqlCmd.Parameters.Add(paramSite);
                SqlDataAdapter sqlDad = new SqlDataAdapter();
                sqlDad.SelectCommand = sqlCmd;
                sqlDad.SelectCommand.CommandTimeout = 0;
                sqlDad.Fill(ds, "Users");

            }
            return ds;
        }

        public string GetRole(string user)
        {
            string role = string.Empty;
            using (SqlConnection conn = GetSQLConnection())
            {
                string sql = "select username ,Role from tblFinanceMetricsFileUsers where username=@User";
                SqlCommand sqlCmd = new SqlCommand(sql, conn);
                SqlParameter paramUser = new SqlParameter("@User", SqlDbType.VarChar, 50);
                paramUser.Value = user;
                sqlCmd.Parameters.Add(paramUser);

                sqlCmd.CommandType = CommandType.Text;
                SqlDataReader dr = null;
                dr = sqlCmd.ExecuteReader();

                while (dr.Read())
                {
                    role = dr[1].ToString();
                }
            }
            return role;
        }

        public DataSet GetSitesRoles(string user)
        {
            using (SqlConnection conn = GetSQLConnection())
            {
                string str = "Declare @role as varchar(50) " +
                             "Begin 	select @role = role from tblextranetusers where username = @username " +
                             "if lower(@role) = 'super user' select sitename2,url from dbo.tblextranetsiteurl where sitename = sitename2 order by sitename2 " +
                             "else " +
                             "select sitename2,url,username,r.role from dbo.tblextranetusersiteroles r  inner join dbo.tblextranetsiteurl s on s.siteid = r.siteid " +
                             "inner join  tblextranetusers u on r.userid = u.userid and u.username=@username where s.sitename = s.sitename2 order by sitename2 End";
                //string str = "select sitename,url,username,r.role from dbo.tblextranetusersiteroles r  inner join dbo.tblextranetsiteurl s on s.siteid = r.siteid " +
                //             "inner join  tblextranetusers u on r.userid = u.userid and u.username=@username";

                SqlCommand sqlCmd = new SqlCommand(str, conn);
                SqlParameter paramUser = new SqlParameter("@username", SqlDbType.VarChar, 50);
                paramUser.Value = user;
                sqlCmd.Parameters.Add(paramUser);

                sqlCmd.CommandType = CommandType.Text;
                //sqlCmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = sqlCmd;
                da.SelectCommand.CommandTimeout = 0;

                DataSet ds = new DataSet();
                da.Fill(ds, "SitesRoles");
                return ds;

            }
        }

        public DataSet GetSitesRoles(string user, string sitename)
        {
            using (SqlConnection conn = GetSQLConnection())
            {
                string sql =
                    "select s.siteid, s.sitename2, r.role from dbo.tblextranetusersiteroles r  inner join dbo.tblextranetsiteurl s on s.siteid = r.siteid " +
                    " inner join  tblextranetusers u on r.userid = u.userid and u.username=@username where s.sitename <> s.sitename2 and s.sitename=@sitename order by sitename2";

                SqlCommand sqlCmd = new SqlCommand(sql, conn);
                SqlParameter paramUser = new SqlParameter("@username", SqlDbType.VarChar, 50);
                paramUser.Value = user;
                sqlCmd.Parameters.Add(paramUser);

                SqlParameter paramSite = new SqlParameter("@sitename", SqlDbType.VarChar, 50);
                paramSite.Value = sitename;
                sqlCmd.Parameters.Add(paramSite);

                sqlCmd.CommandType = CommandType.Text;
                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = sqlCmd;
                da.SelectCommand.CommandTimeout = 0;
                DataSet ds = new DataSet();
                da.Fill(ds, "SitesRoles");
                return ds;

            }
        }

        public bool InsertData(string user, string password, string role, int siteid, string firstName, string lastName)
        {
            bool blnValid = false;
            SqlCommand sqlcmd = new SqlCommand();
            using (sqlcmd.Connection = GetSQLConnection())
            {
                string sql;

                sql = "DECLARE @userid int;Declare @NEWID int;   " +
                      "If exists (select username from tblExtranetusers where username = @User) " +
                      "begin " +
                      "select @userid = userid from tblextranetusers where username = @User and password = @Password " +
                      "insert into tblExtranetUserSiteRoles (siteId,userId,role) values(@siteid,@userid,@role); " +
                      "end " +
                      "else " +
                      "begin " +
                      "insert into tblExtranetUsers (userName,FirstName,LastName,password) values (@User ,@FirstName,@LastName,@Password)" +
                      "SELECT @NEWID = SCOPE_IDENTITY() " +
                      "insert into tblExtranetUserSiteRoles (siteId,userId,role) values(@siteid,@NEWID,@role) " +
                      "end ";
                sqlcmd.CommandText = sql;

                SqlParameter paramUser = new SqlParameter("@User", SqlDbType.VarChar, 50);
                paramUser.Value = user;
                sqlcmd.Parameters.Add(paramUser);

                SqlParameter paramPwd = new SqlParameter("@Password", SqlDbType.VarChar, 50);
                paramPwd.Value = password;
                sqlcmd.Parameters.Add(paramPwd);

                SqlParameter paramRole = new SqlParameter("@Role", SqlDbType.Char, 20);
                paramRole.Value = role;
                sqlcmd.Parameters.Add(paramRole);

                SqlParameter paramSite = new SqlParameter("@SiteId", SqlDbType.Int);
                paramSite.Value = siteid;
                sqlcmd.Parameters.Add(paramSite);

                SqlParameter paramFirst = new SqlParameter("@FirstName", SqlDbType.VarChar, 100);
                paramFirst.Value = firstName;
                sqlcmd.Parameters.Add(paramFirst);

                SqlParameter paramLast = new SqlParameter("@LastName", SqlDbType.VarChar, 100);
                paramLast.Value = lastName;
                sqlcmd.Parameters.Add(paramLast);


                int recAffected = sqlcmd.ExecuteNonQuery();
                blnValid = true;
            }
            return blnValid;
        }

        public bool UpdateUser(string user, string role, int siteId, string firstName, string lastName)
        {
            bool blnValid = false;
            SqlCommand sqlcmd = new SqlCommand();
            using (sqlcmd.Connection = GetSQLConnection())
            {
                string sql;

                sql =
                    "update tblExtranetUsers set firstName = @FirstName,lastname = @LastName where userName = @User update tblExtranetUserSiteRoles set role = @Role, siteId = @siteId where siteId = @siteId and userId in(select userId from tblExtranetUsers where userName = @User)";
                sqlcmd.CommandText = sql;

                SqlParameter paramUser = new SqlParameter("@User", SqlDbType.VarChar, 50);
                paramUser.Value = user;
                sqlcmd.Parameters.Add(paramUser);

                SqlParameter paramRole = new SqlParameter("@Role", SqlDbType.Char, 20);
                paramRole.Value = role;
                sqlcmd.Parameters.Add(paramRole);

                SqlParameter paramSite = new SqlParameter("@SiteId", SqlDbType.Int);
                paramSite.Value = siteId;
                sqlcmd.Parameters.Add(paramSite);

                SqlParameter paramFirstName = new SqlParameter("@FirstName", SqlDbType.VarChar, 100);
                paramFirstName.Value = firstName;
                sqlcmd.Parameters.Add(paramFirstName);

                SqlParameter paramLastName = new SqlParameter("@LastName", SqlDbType.VarChar, 100);
                paramLastName.Value = lastName;
                sqlcmd.Parameters.Add(paramLastName);

                int recAffected = sqlcmd.ExecuteNonQuery();
                blnValid = true;
            }
            return blnValid;
        }

        public bool UpdateUsers(string user, string role, int siteId)
        {
            bool blnValid = false;
            SqlCommand sqlcmd = new SqlCommand();
            using (sqlcmd.Connection = GetSQLConnection())
            {
                string sql;
                if (role.ToLower().Trim() == "no access")
                {
                    sql =
                        "delete from tblExtranetUserSiteRoles where userid= (select userid from tblextranetusers where username = @User) and siteid  = @SiteId";
                }
                else
                {
                    sql = "DECLARE @userid int; " +
                          "If exists (select userid from tblExtranetUserSiteRoles where userid in(select userid from tblExtranetUsers where username = @User) and siteid = @SiteId) " +
                          "begin " +
                          "update tblExtranetUserSiteRoles set role = @Role, siteId = @siteId where siteId = @siteId and userId in(select userId from tblExtranetUsers where userName = @User) " +
                          "end " +
                          "else " +
                          "begin " +
                          " Select @userid = userid from tblextranetusers where username = @User " +
                          "insert into tblExtranetUserSiteRoles(userid , role , siteid) values (@userid,@Role,@SiteId) " +
                          "end";
                }
                sqlcmd.CommandText = sql;

                SqlParameter paramUser = new SqlParameter("@User", SqlDbType.VarChar, 50);
                paramUser.Value = user;
                sqlcmd.Parameters.Add(paramUser);

                SqlParameter paramRole = new SqlParameter("@Role", SqlDbType.Char, 20);
                paramRole.Value = role;
                sqlcmd.Parameters.Add(paramRole);

                SqlParameter paramSite = new SqlParameter("@SiteId", SqlDbType.Int);
                paramSite.Value = siteId;
                sqlcmd.Parameters.Add(paramSite);

                int recAffected = sqlcmd.ExecuteNonQuery();
                blnValid = true;
            }
            return blnValid;
        }

        public bool DeleteUser(string user)
        {
            bool blnValid = false;
            SqlCommand sqlcmd = new SqlCommand();
            using (sqlcmd.Connection = GetSQLConnection())
            {
                string sql;

                sql =
                    "delete from tblExtranetUserSiteRoles  where userId in(select userId from tblExtranetUsers where userName = @User)  delete from tblExtranetUsers where username = @User";
                sqlcmd.CommandText = sql;

                SqlParameter paramUser = new SqlParameter("@User", SqlDbType.VarChar, 50);
                paramUser.Value = user;
                sqlcmd.Parameters.Add(paramUser);

                int recAffected = sqlcmd.ExecuteNonQuery();
                blnValid = true;
            }
            return blnValid;
        }



        public DataSet getSites(string sitename)
        {
            string sql = string.Empty;
            DataSet ds = new DataSet();

            using (SqlConnection conn = GetSQLConnection())
            {
                if (sitename == null)
                {
                    sql =
                        "select sitename2,siteid from dbo.tblExtranetSiteURL where sitename2 = sitename order by sitename2";
                }
                else
                {
                    sql =
                        "select sitename2,siteid from dbo.tblExtranetSiteURL where sitename = @sitename and sitename2<>sitename  order by sitename2";
                }
                SqlDataAdapter sqlDad = new SqlDataAdapter();

                SqlCommand sqlCmd = new SqlCommand(sql, conn);
                if (sitename != null)
                {
                    SqlParameter paramSite = new SqlParameter("@sitename", SqlDbType.VarChar, 250);
                    paramSite.Value = sitename;

                    sqlCmd.Parameters.Add(paramSite);
                }
                sqlDad.SelectCommand = sqlCmd;
                sqlDad.SelectCommand.CommandTimeout = 0;
                sqlDad.Fill(ds, "Sites");

            }
            return ds;
        }

        public string getRoleUserSite(string username, string sitename)
        {
            string str = string.Empty;
            SqlDataReader dr = null;
            string role = string.Empty;

            using (SqlConnection conn = GetSQLConnection())
            {
                if (sitename == null)
                {
                    str = "select role from tblExtranetUsers where username=@username ";

                }
                else
                {
                    str =
                        "select r.role from tblExtranetUserSiteRoles r inner join tblExtranetUsers u on r.userid=u.userid and u.username=@username " +
                        "inner join tblExtranetSiteURL s on r.siteid = s.siteid and s.sitename2=@sitename";
                }
                SqlCommand sqlCmd = new SqlCommand(str, conn);
                SqlParameter paramUser = new SqlParameter("@username", SqlDbType.VarChar, 50);
                paramUser.Value = username;

                sqlCmd.Parameters.Add(paramUser);

                if (sitename != null)
                {
                    SqlParameter paramSite = new SqlParameter("@sitename", SqlDbType.VarChar, 250);
                    paramSite.Value = sitename;

                    sqlCmd.Parameters.Add(paramSite);
                }
                sqlCmd.CommandType = CommandType.Text;

                dr = sqlCmd.ExecuteReader();
                while (dr.Read())
                {
                    role = dr["role"].ToString();
                }


                return role;
            }
        }

        public bool checkUser(string username, string password)
        {
            bool blnValid = false;
            using (SqlConnection conn = GetSQLConnection())
            {
                string str =
                    "select username from tblExtranetUsers  where upper(username)=@username and password = @password ";

                SqlCommand sqlCmd = new SqlCommand(str, conn);
                SqlParameter paramUser = new SqlParameter("@username", SqlDbType.VarChar, 50);
                paramUser.Value = username.ToUpper();

                sqlCmd.Parameters.Add(paramUser);

                SqlParameter paramPwd = new SqlParameter("@password", SqlDbType.VarChar, 50);
                paramPwd.Value = password;

                sqlCmd.Parameters.Add(paramPwd);

                sqlCmd.CommandType = CommandType.Text;
                SqlDataReader dr = null;

                dr = sqlCmd.ExecuteReader();

                while (dr.Read())
                {
                    blnValid = true;
                }
                return blnValid;
            }
        }

        public DataSet GetAllUsersOrRoles(string type)
        {
            string sql = string.Empty;
            DataSet ds = new DataSet();

            using (SqlConnection conn = GetSQLConnection())
            {
                if (type.ToLower() == "user")
                {
                    sql =
                        "select lastname + ', ' + firstname + '  (' + username + ')' as users , username from dbo.tblExtranetUserS order by lastname";
                }
                else
                {
                    sql = "select distinct role from dbo.tblExtranetUserSiteRoles order by role";
                }
                SqlDataAdapter sqlDad = new SqlDataAdapter();

                SqlCommand sqlCmd = new SqlCommand(sql, conn);

                sqlDad.SelectCommand = sqlCmd;
                sqlDad.SelectCommand.CommandTimeout = 0;
                sqlDad.Fill(ds, "AllRoles");

            }
            return ds;
        }

        public DataSet GetAllSitesLinks()
        {
            string sql = string.Empty;
            DataSet ds = new DataSet();

            using (SqlConnection conn = GetSQLConnection())
            {
                sql = "Select ID, programname, Link, Title, Description, Notes from dbo.tblsitelist";

                SqlDataAdapter sqlDad = new SqlDataAdapter();
                SqlCommand sqlcmd = new SqlCommand(sql, conn);

                sqlDad.SelectCommand = sqlcmd;
                sqlDad.SelectCommand.CommandTimeout = 0;
                sqlDad.Fill(ds, "SiteLinks");
            }
            return ds;
        }

        public bool DeleteUser(int userId)
        {
            string sql = string.Empty;
            try
            {
                using (SqlConnection conn = GetSQLConnection())
                {
                    sql =
                        "Delete from dbo.tblExtranetUserSiteRoles where userId = @userID  Delete from dbo.tblExtranetUsers where userId = @userID";

                    SqlCommand sqlcmd = new SqlCommand(sql, conn);

                    SqlParameter paramUser = new SqlParameter("@userID", SqlDbType.Int);
                    paramUser.Value = userId;

                    sqlcmd.Parameters.Add(paramUser);

                    int recAffected = sqlcmd.ExecuteNonQuery();
                    return true;

                }
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public DataSet GetEqsActiveSuppliers(string type)
        {
            string sql = string.Empty;
            DataSet ds = new DataSet();

            using (OleDbConnection conn = GetOleDbConnection("EqsActiveSuppliers"))
            {
                if (type == "LIST")
                    sql = "SELECT DISTINCT SUPPLIER_NAME FROM [Supplier Master] WHERE Status = 'A'";
                else if (type == "ALL")
                    sql =
                        "SELECT DISTINCT SUPPLIER_NAME,ADDRESS_1,CITY,STATE,ZIP,ASC_PHONE,SUPPLIER_WEB_ADDRESS,SUPPLIER_NUMBER_PK, BAAN_PREFIX FROM [Supplier Master]" +
                        " WHERE Status = 'A'";
                else
                    sql =
                        "SELECT DISTINCT SUPPLIER_NAME,ADDRESS_1,CITY,STATE,ZIP,ASC_PHONE,SUPPLIER_WEB_ADDRESS,SUPPLIER_NUMBER_PK, BAAN_PREFIX FROM [Supplier Master]" +
                        " WHERE Status = 'A' AND SUPPLIER_NAME = '" + type.Replace("'", "''") + "'";

                OleDbDataAdapter oleDbDA = new OleDbDataAdapter();
                OleDbCommand oleDbCmd = new OleDbCommand(sql, conn);

                oleDbDA.SelectCommand = oleDbCmd;
                oleDbDA.SelectCommand.CommandTimeout = 0;
                oleDbDA.Fill(ds, "EqsActiveSuppliers");
                oleDbCmd.Dispose();
            }
            return ds;
        }

        //jdu 11020101
        private void ApplyWhereOrAnd(StringBuilder sb)
        {
            if (sb.ToString().Contains("WHERE"))
                sb.Append(" AND ");
            else
                sb.Append("WHERE ");
        }

        public DataSet KiaPriorDealersSales(string dealerNo, string itemNo, string fromDate, string toDate)
        {
            DataSet dsLN = new DataSet();

            StringBuilder sb = new StringBuilder();
            sb.Append(
                "select DealerNumber,ShipToNumber,ToolNumber,Description,Department,QuantityShipped,PONumber,TransactionType,Convert(varchar,OrderDate,101) as OrderDate,Convert(varchar,InvoiceDate,101) as InvoiceDate from tblKiaHistorySales ");


            if (fromDate.Length > 0)
            {
                ApplyWhereOrAnd(sb);
                sb.Append("InvoiceDate >= '" + Convert.ToDateTime(fromDate).ToShortDateString() + "'");
            }
            if (toDate.Length > 0)
            {
                ApplyWhereOrAnd(sb);
                sb.Append("InvoiceDate <= '" + Convert.ToDateTime(toDate).ToShortDateString() + "'");
            }
            if (dealerNo.Trim().Length > 0)
            {
                ApplyWhereOrAnd(sb);
                sb.Append("DealerNumber = '" + dealerNo + "'");
            }
            if (itemNo.Trim().Length > 0)
            {
                ApplyWhereOrAnd(sb);
                sb.Append("ToolNumber = '" + itemNo + "'");
            }

            using (SqlConnection conn = GetSQLConnection())
            {

                SqlCommand cmd = new SqlCommand(sb.ToString(), conn);
                SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                adpt.SelectCommand.CommandTimeout = 0;
                adpt.Fill(dsLN, "KiaDealerPriorSalesData");
                adpt.Dispose();
            }
            return dsLN;

        }

        //end jdu 11020101
        public DataSet GetDealerSales(string dealerNo, string itemNo, string fromDate, string toDate,
            string applicationType)
        {
            DataSet dsLN = new DataSet();
            //
            // Convertion from BAAN to LN started on 7/6/2010, so get LN data after 7/5/2010
            // ALL BAAN data was inserted to LN, no longer check dates.
            //
            //if (Convert.ToDateTime(fromDate) >= Convert.ToDateTime("7/6/2010") || Convert.ToDateTime(toDate) >= Convert.ToDateTime("7/6/2010"))
            //{
            using (OleDbConnection conn = GetOleDbConnection("OracleLnDwProd"))
            {

                OleDbDataAdapter oleDbDA = new OleDbDataAdapter();
                OleDbCommand oleDbCmd = new OleDbCommand(GetLnSQL(dealerNo, itemNo, fromDate, toDate, applicationType),
                    conn);

                oleDbDA.SelectCommand = oleDbCmd;
                oleDbDA.SelectCommand.CommandTimeout = 0;
                oleDbDA.Fill(dsLN, "DealerSalesData");
                oleDbCmd.Dispose();
            }
            //}

            return dsLN;
        }

        private string GetLnSQL(string dealerNo, string itemNo, string fromDate, string toDate, string applicationType)
        {
            //
            // KIA Dealer "AK006" or "AL008"
            // VW Dealer "401017" or "401023"
            //
            System.Text.StringBuilder oSQL = new System.Text.StringBuilder();
            // ###############################################
            // THIS QUERY IS TO GET USA(151)/CANADA SALES(102)
            // ###############################################
            oSQL.Append(
                "SELECT /*+ leading(b,a,sp,c) use_nl(a) */ a.invoice INVOICE_NO, TO_CHAR (a.invoice_date, 'MM/DD/YYYY') AS INVOICE_DATE, a.delivered_qty AS SHIPQTY, a.invoice_usd_list_price_unit AS LISTPRICE_UNIT,");
            oSQL.Append(
                "       a.invoice_usd_net_price_ext AS NETPRICE_EXTENDED, b.business_partner AS CUSTNO, c.item_description AS ITEMDESC,");
            oSQL.Append("       sp.name AS CUSTNAME, TRIM (b.bp_name) AS CUSTNAME2, sp.city, state_province AS STATE");

            if (_SearchItemNumber == string.Empty)  // No cross-reference number found in LN, so use the search item number(itemNo) entered
            {
                if (applicationType.ToUpper().Equals("VW"))
                    oSQL.Append(",a.item");
                else
                    oSQL.Append(",CONCAT(CONCAT('''', a.item),'''') AS ITEM"); //Had issue when downloading to Excel for VW would convert quote to &#39; using ExcelPackage
            }
            else
            {
                // _ItemNumber is the actual item number that is associated with the cross-reference number of _SearchItemNumber
                //
                itemNo = _ItemNumber;
                oSQL.Append(",'" + _SearchItemNumber + "' AS ITEM");
            }
            oSQL.Append(
                " FROM snapondw.order_detail a, snapondw.business_partner b, snapondw.item c, snapondw.address sp");

            oSQL.Append(" WHERE a.bp_company = b.company");
            oSQL.Append("   AND a.sold_to_bp = b.business_partner");
            oSQL.Append("   AND a.order_company = c.company");
            oSQL.Append("   AND a.item = c.item");
            oSQL.Append("   AND a.sold_to_address = sp.address");
            // Partitioned by week_start_date = Sunday date
            oSQL.Append("   AND a.week_start_date >= TO_DATE ('" + fromDate + "', 'MM/DD/YYYY')-7 ");
            oSQL.Append("   AND a.week_start_date <= TO_DATE ('" + toDate + "', 'MM/DD/YYYY')+7 ");
            //
            oSQL.Append("   AND a.order_company IN (102, 151)");

            switch (applicationType)
            {
                case "VW":
                    oSQL.Append("   AND b.sold_to_sales_office IN ('648130', '648131', '648132', '604102','648133')");  // 648130=VW, 648131=AUDI, Bentley=648132, 604102=Canada, 648133=Porsche
                    break;
                case "KIA":
                    oSQL.Append("   AND b.sold_to_sales_office IN ('648150')");
                    break;
                case "CHRYSLER":
                    oSQL.Append(" AND b.sold_to_sales_office IN ('648185','648186','608185')"); //608185=Canada
                    break;
            }

            oSQL.Append("   AND a.invoice_date >= TO_DATE ('" + fromDate + "', 'MM/DD/YYYY')");
            oSQL.Append("   AND a.invoice_date <= TO_DATE ('" + toDate + "', 'MM/DD/YYYY')");

            if (dealerNo.Length > 0)
                oSQL.Append("  AND sp.NAME LIKE '%" + dealerNo + "%'");

            if (itemNo.Length > 0)
                oSQL.Append(" AND a.item = '" + itemNo + "' ");

            return oSQL.ToString();
        }

        private string GetBaanSQL(string dealerNo, string itemNo, string fromDate, string toDate, string applicationType)
        {
            System.Text.StringBuilder oSQL = new System.Text.StringBuilder();
            // ###################################
            // THIS QUERY IS TO GET DOMESTIC SALES
            // ###################################
            oSQL.Append("SELECT A.INVOICE_NO,TO_CHAR(A.INVOICE_DATE,'MM/DD/YYYY') AS  INVOICE_DATE,");
            oSQL.Append(
                "       CAST(A.SHIPQTY AS NUMBER) AS SHIPQTY,A.LISTPRICE_UNIT,A.NETPRICE_EXTENDED,A.CUSTNO,C.ITEMDESC,B.CUSTNAME,B.CUSTNAME2,");
            oSQL.Append("       TRIM(SUBSTR(B.CITY_STATE,1,(INSTR(B.CITY_STATE,',') - 1) ) ) AS CITY,");
            oSQL.Append("       TRIM(SUBSTR(B.CITY_STATE,(INSTR(B.CITY_STATE,',') + 1 ) ) ) AS STATE");

            if (_SearchItemNumber == string.Empty)
            {
                oSQL.Append(",A.ITEM");
            }
            else
            {
                // _ItemNumber is the actual item number that is associated with the cross-reference number of _SearchItemNumber
                //
                itemNo = _ItemNumber;
                oSQL.Append(",'" + _SearchItemNumber + "' AS ITEM");
            }

            oSQL.Append("  FROM PWHSE001.INVORDOLI A,PWHSE001.CUSTRIT B,PWHSE001.ITEMTRIT C");
            oSQL.Append(" WHERE A.CUSTNO = B.CUSTNO");
            oSQL.Append("   AND	A.ITEM = C.ITEMNO");

            switch (applicationType)
            {
                case "VW":
                    oSQL.Append("   AND	A.CUSTGRP = '573'");
                    break;
                case "KIA":
                    oSQL.Append("   AND	A.CUSTGRP = '577'");
                    break;
            }

            if (dealerNo.Length > 0)
                oSQL.Append(" AND B.CUSTNAME LIKE '%" + dealerNo + "%'");

            if (itemNo.Length > 0)
                oSQL.Append(" AND A.ITEM = '" + itemNo + "' ");


            oSQL.Append(" AND A.INVOICE_DATE >= TO_DATE('" + fromDate + "','MM/DD/YYYY')");
            oSQL.Append(" AND A.INVOICE_DATE <= TO_DATE('" + toDate + "','MM/DD/YYYY')");
            //
            // No Canada KIA
            //
            if (applicationType != "KIA")
            {
                oSQL.Append(" UNION ALL ");
                // #################################
                // THIS QUERY IS TO GET CANADA SALES
                // #################################
                oSQL.Append("SELECT A.INVOICE_NO,TO_CHAR(A.INVOICE_DATE,'MM/DD/YYYY') AS  INVOICE_DATE,");
                oSQL.Append(
                    "       CAST(A.SHIPQTY AS NUMBER) AS SHIPQTY,A.LISTPRICE_UNIT,A.NETPRICE_EXTENDED,A.CUSTNO,C.ITEMDESC,B.CUSTNAME,B.CUSTNAME2,");
                oSQL.Append("       TRIM(SUBSTR(D.CITY_STATE,1,(INSTR(D.CITY_STATE,',') - 1) ) ) AS CITY,");
                oSQL.Append("       TRIM(SUBSTR(D.CITY_STATE,(INSTR(D.CITY_STATE,',') + 1 ) ) ) AS STATE");

                if (_SearchItemNumber == string.Empty)
                {
                    oSQL.Append(",A.ITEM");
                }
                else
                {
                    // _ItemNumber is the actual item number that is associated with the cross-reference number of _SearchItemNumber
                    //
                    itemNo = _ItemNumber;
                    oSQL.Append(",'" + _SearchItemNumber + "' AS ITEM");
                }

                oSQL.Append("  FROM PWHSE001.INVORDOLI A,PWHSE001.CUSTRIT B,PWHSE001.ITEMTRIT C,");
                oSQL.Append("     ( SELECT DISTINCT A.ORDER_NO, A.ORDER_DATE, B.CITY_STATE");
                oSQL.Append("         FROM PWHSE001.INVORDOLI A,  PWHSE001.SPECIFICDLYTRIT B");
                oSQL.Append("        WHERE A.ORDER_NO = B.ORDER_NO");
                oSQL.Append("          AND A.ORDER_DATE = B.ORDER_DATE");
                oSQL.Append("          AND A.INVOICE_DATE >= TO_DATE('" + fromDate + "','MM/DD/YYYY')");
                oSQL.Append("          AND A.INVOICE_DATE <= TO_DATE('" + toDate + "','MM/DD/YYYY')");

                switch (applicationType)
                {
                    case "VW":
                        oSQL.Append("   AND A.CUSTNO = '000115' )  D");
                        break;
                }

                oSQL.Append(" WHERE A.CUSTNO = B.CUSTNO");
                oSQL.Append("   AND A.ITEM = C.ITEMNO");

                switch (applicationType)
                {
                    case "VW":
                        oSQL.Append("   AND A.CUSTNO = '000115'");
                        break;
                }

                oSQL.Append("   AND A.ORDER_NO = D.ORDER_NO");
                oSQL.Append("   AND A.ORDER_DATE = D.ORDER_DATE");
                oSQL.Append("   AND A.INVOICE_DATE >= TO_DATE('" + fromDate + "','MM/DD/YYYY')");
                oSQL.Append("   AND A.INVOICE_DATE <= TO_DATE('" + toDate + "','MM/DD/YYYY')");

                if (dealerNo.Length > 0)
                    oSQL.Append(" AND B.CUSTNAME LIKE '%" + dealerNo + "%'");

                if (itemNo.Length > 0)
                    oSQL.Append(" AND A.ITEM = '" + itemNo + "' ");
            }

            return oSQL.ToString();

        }

        public string ValidateDealerNumber(string dealerNo)
        {
            string Result = "yes";
            string sql;
            OleDbParameter paramDealerNo;
            OleDbCommand oleDbCmd;
            //
            // Use LN to validate dealer numbers
            //
            if (dealerNo.Length > 0)
            {
                using (OleDbConnection conn = GetOleDbConnection("OracleLnDwProd"))
                {
                    // Need to do a wild card search due to field SHIPPING_TO_POINT_NM having value of "409145***ASK IF FOR RESALE******"
                    //
                    sql = "SELECT CASE WHEN COUNT(1) > 0 THEN 'yes' ELSE 'no' END FROM snapondw.address WHERE NAME LIKE ?";

                    oleDbCmd = new OleDbCommand(sql, conn);
                    paramDealerNo = new OleDbParameter("@dealerNo", OleDbType.Char, 35);
                    paramDealerNo.Value = "%" + dealerNo + "%";
                    oleDbCmd.Parameters.Add(paramDealerNo);

                    Result = (string)oleDbCmd.ExecuteScalar();
                    oleDbCmd.Dispose();
                }
            }
            return Result;
        }

        public string ValidateItemNumber(string itemNo)
        {
            string sql;
            string Result = "yes";
            OleDbCommand oleDbCmd;
            _SearchItemNumber = string.Empty;
            _ItemNumber = string.Empty;
            //
            // Use LN to validate item numbers
            //
            if (itemNo.Length > 0)
            {
                using (OleDbConnection conn = GetOleDbConnection("OracleLnDwProd"))
                {
                    // US test item 214-1166
                    // CAN test item BWCA8276
                    // Query USA(151) and Canada(102) data
                    //
                    sql =
                        "SELECT CASE WHEN COUNT(1) > 0 THEN 'yes' ELSE 'no' END FROM snapondw.item WHERE company IN (102, 151) AND item = ?";

                    oleDbCmd = new OleDbCommand(sql, conn);
                    OleDbParameter paramItemNo = new OleDbParameter("@itemNo", OleDbType.Char, 35);
                    paramItemNo.Value = itemNo;
                    oleDbCmd.Parameters.Add(paramItemNo);

                    Result = (string)oleDbCmd.ExecuteScalar();

                    if (Result == "no")
                    {
                        // Check cross-reference table in LN
                        //
                        sql =
                            "SELECT MAX(item),  CASE WHEN COUNT(1) > 0 THEN 'yes' ELSE 'no' END FROM snapondw.item_by_system ";
                        sql += "WHERE company in (102, 151) AND alternative_item_code = ? ";

                        oleDbCmd.Parameters.Clear();
                        OleDbParameter paramCrossRefItemNo = new OleDbParameter("@crossRefItemNo", OleDbType.Char, 35);
                        paramCrossRefItemNo.Value = itemNo;
                        oleDbCmd.Parameters.Add(paramCrossRefItemNo);

                        oleDbCmd.CommandText = sql;
                        OleDbDataReader oleReader = oleDbCmd.ExecuteReader();
                        oleReader.Read();

                        _ItemNumber = oleReader[0].ToString().Trim();
                        _SearchItemNumber = itemNo;
                        Result = oleReader[1].ToString();

                        oleDbCmd.Dispose();
                        oleReader.Close();
                        oleReader.Dispose();
                    }
                }
            }

            return Result;
        }

        public DataSet GetRegionalManagers(bool IsRegionalSalesManager)
        {
            string sql = "";
            if (!IsRegionalSalesManager)
                sql = "select RegionalManagerId,Name,NameOID,Title,OfficePhone,CellPhone,Address1,Address2,City," +
                      "State,Zip,Email,Imagename,StateTerritoryQuery,StateTerritoryView from " +
                      "SBS_SharedResource.dbo.tblRegionalManagers where (StateTerritoryView  <>'')";
            else
                sql = "select RegionalManagerId,Name,NameOID,Title,OfficePhone,CellPhone,Address1,Address2,City," +
                      "State,Zip,Email,Imagename,StateTerritoryQuery,StateTerritoryView from " +
                      "SBS_SharedResource.dbo.tblRegionalManagers where (StateTerritoryView is null or StateTerritoryView = '')";

            DataSet ds = new DataSet();
            using (SqlConnection conn = GetSQLConnection())
            {
                SqlDataAdapter sqlDad = new SqlDataAdapter();

                SqlCommand sqlCmd = new SqlCommand(sql, conn);
                sqlDad.SelectCommand = sqlCmd;
                sqlDad.SelectCommand.CommandTimeout = 0;
                sqlDad.Fill(ds, "RegionalManagers");

            }
            return ds;
        }

        public string GetAuthyUserId(string userId)
        {
            SqlDataReader dr = null;
            var authyId = string.Empty;

            using (SqlConnection conn = GetSQLConnection())
            {
                string sql =
                    "select AuthyUserId from dbo.tblPortalUserAccounts where UserId = @userId";

                SqlCommand sqlCmd = new SqlCommand(sql, conn);
                SqlParameter paramUser = new SqlParameter("@userId", SqlDbType.VarChar, 50);
                paramUser.Value = userId;
                sqlCmd.Parameters.Add(paramUser);

                sqlCmd.CommandType = CommandType.Text;

                dr = sqlCmd.ExecuteReader();
                while (dr.Read())
                {
                    authyId = dr["AuthyUserId"].ToString();
                }

                return authyId;
            }
        }

        #region -- Reports --

        public DataSet GetOrderCounts(DateTime startDate, DateTime endDate)
        {
            var connectionKey = GetURLBasedConnectionKey();
            var taDatabase = "SBS_MoparTechAuthority";

            if (connectionKey.Equals("dev_EqsPortalApps", StringComparison.InvariantCultureIgnoreCase))
            {
                taDatabase = "SBS_MoparTechAuthorityRD";
            }
            var query = @"select 'Honda (all programs)' as 'ProgramType', isnull(sum(a.OrderCount),0) as 'OrderCount', isnull(sum(Total),0) as Total
                          from (
                                      select 'HondaLAC' as ProgramType, CONVERT(varchar(10), OrderedDate, 120) as OrderDate, sum(1) as OrderCount, sum(Total - TotalTax) as Total
                                        from SBS_HondaEcommerceLAC.dbo.tblOrders
                                      where CONVERT(varchar(10), OrderedDate, 120) >= @startDate AND CONVERT(VARCHAR(10), OrderedDate, 120) <= @endDate
                                      group by CONVERT(varchar(10), OrderedDate, 120)
 
                                      union all
 
                                      select 'HondaMarine' as ProgramType, CONVERT(varchar(10), OrderedDate, 120), sum(1), sum(Total - TotalTax) as Total
                                        from SBS_HondaEcommerceMarine.dbo.tblOrders
                                      where CONVERT(varchar(10), OrderedDate, 120) >= @startDate AND CONVERT(varchar(10), OrderedDate, 120) <= @endDate
                                        and (status is null or  status = 'PaymentTech Final')
                                      group by CONVERT(varchar(10), OrderedDate, 120)
 
                                      union all
 
                                      select 'HondaCycle' as ProgramType, CONVERT(varchar(10), OrderedDate, 120), sum(1), sum(Total - TotalTax) as Total
                                        from SBS_HondaEcommerceMotorcycle.dbo.tblOrders
                                      where CONVERT(varchar(10), OrderedDate, 120) >= @startDate AND CONVERT(varchar(10), OrderedDate, 120) <= @endDate
                                        and (status is null or  status = 'PaymentTech Final')
                                      group by CONVERT(varchar(10), OrderedDate, 120)
 
                                      union all
 
                                      select 'HondaPandE' as ProgramType, CONVERT(varchar(10), OrderedDate, 120), sum(1), sum(Total - TotalTax) as Total
                                        from SBS_HondaEcommercePowerEquipment.dbo.tblOrders
                                      where CONVERT(varchar(10), OrderedDate, 120) >= @startDate AND CONVERT(varchar(10), OrderedDate, 120) <= @endDate
                                        and (status is null or  status = 'PaymentTech Final')
                                      group by CONVERT(varchar(10), OrderedDate, 120)
 
                                      union all
 
                                      select 'HondaAuto' as ProgramType, CONVERT(varchar(10), OrderedDate, 120), sum(1), sum(Total - TotalTax) as Total
                                        from SBS_HondaEcommerceUSA.dbo.tblOrders
                                      where CONVERT(varchar(10), OrderedDate, 120) >= @startDate AND CONVERT(varchar(10), OrderedDate, 120) <= @endDate
                                        and (status is null or  status = 'PaymentTech Final')
                                      group by CONVERT(varchar(10), OrderedDate, 120)
                                      ) as a
 
                        union all
 
                        select 'Toyota/Lexus' as ProgramType, isnull(sum(a.OrderCount),0), isnull(sum(Total),0) as Total
                          from (
                                      select CONVERT(varchar(10), OrderedDate, 120) as OrderDate, sum(1) as OrderCount, sum(Total - TotalTax) as Total
                                        from SBS_ToyotaEcommerce.dbo.tblOrders
                                      where CONVERT(varchar(10), OrderedDate, 120) >= @startDate AND CONVERT(varchar(10), OrderedDate, 120) <= @endDate
                                        and (status is null or  status = 'PaymentTech Final')
                                      group by CONVERT(varchar(10), OrderedDate, 120)
 
                                      union all
 
                                      select CONVERT(varchar(10), DateSubscribed, 120) as OrderDate, sum(1), sum(PayAmt) as Total
                                        from SBS_ToyotaTechInfo.dbo.tblTransactions
                                      where CONVERT(varchar(10), DateSubscribed, 120) >= @startDate AND CONVERT(varchar(10), DateSubscribed, 120) <= @endDate
                                        and (status_HPF is null or status_HPF = 'PaymentTech Final')
                                      group by CONVERT(varchar(10), DateSubscribed, 120)
                                      ) as a
 
                        union all
 
                        select 'VW/Audi/Porsche' as ProgramType, isnull(sum(a.OrderCount),0), isnull(sum(Total),0) as Total
                          from (
                                      select CONVERT(varchar(10), OrderedDate, 120) as OrderDate, sum(1) as OrderCount, sum(Total - TotalTax) as Total
                                        from SBS_PorscheEcommerce.dbo.tblOrders
                                      where CONVERT(varchar(10), OrderedDate, 120) >= @startDate AND CONVERT(varchar(10), OrderedDate, 120) <= @endDate
                                        and (status is null or  status = 'PaymentTech Final')
                                      group by CONVERT(varchar(10), OrderedDate, 120)
 
                                      union all
 
                                      select CONVERT(varchar(10), OrderedDate, 120) as OrderDate, sum(1) as OrderCount, sum(Total - TotalTax) as Total
                                        from SBS_VwEcommerce.dbo.tblOrders
                                      where CONVERT(varchar(10), OrderedDate, 120) >= @startDate AND CONVERT(varchar(10), OrderedDate, 120) <= @endDate
                                        and (status is null or  status = 'PaymentTech Final')
                                      group by CONVERT(varchar(10), OrderedDate, 120)
                                      ) as a
 
                        union all
 
                        select 'KIA Special Tools' as ProgramType, isnull(sum(a.OrderCount),0), isnull(sum(Total),0) as Total
                          from (
                                      select CONVERT(varchar(10), OrderedDate, 120) as OrderDate, sum(1) as OrderCount, sum(Total - TotalTax) as Total
                                        from SBS_KiaEcommerce.dbo.tblOrders
                                      where CONVERT(varchar(10), OrderedDate, 120) >= @startDate AND CONVERT(varchar(10), OrderedDate, 120) <= @endDate
                                        and (status is null or  status = 'PaymentTech Final')
                                      group by CONVERT(varchar(10), OrderedDate, 120)
 
                                      union all
 
                                      select CONVERT(varchar(10), DateSubscribed, 120) as OrderDate, sum(1), sum(PayAmt) as Total
                                        from SBS_KiaTechInfo.dbo.tblTransactions
                                      where CONVERT(varchar(10), DateSubscribed, 120) >= @startDate AND CONVERT(varchar(10), DateSubscribed, 120) <= @endDate
                                        and (status_HPF is null or status_HPF = 'PaymentTech Final')
                                      group by CONVERT(varchar(10), DateSubscribed, 120)
                                      ) as a
 
                        union all
                        
                        select 'Mopar Tool and Equipment' as ProgramType, isnull(sum(a.OrderCount),0), isnull(sum(Total),0) as Total
                          from (
                                      select CONVERT(varchar(10), OrderedDate, 120) as OrderDate, sum(1) as OrderCount, sum(Total - TotalTax) as Total
                                        from SBS_MoparEcommerce.dbo.tblOrders
                                      where CONVERT(varchar(10), OrderedDate, 120) >= @startDate AND CONVERT(varchar(10), OrderedDate, 120) <= @endDate
                                        and (status is null or  status = 'PaymentTech Final')
                                      group by CONVERT(varchar(10), OrderedDate, 120)
                                ) as a

                        union all
                        
                        select 'Mopar Tech Authority' as ProgramType, isnull(sum(a.OrderCount),0), isnull(sum(Total),0) as Total
                          from (
                            select CONVERT(varchar(10), a.DateCreated, 120) as OrderDate, sum(1) as OrderCount, sum(a.Total - a.Taxes) as Total
                                        from " + taDatabase + @".dbo.tblOrder a
										left outer join " + taDatabase + @".dbo.tblOrderPayment b
										  on a.ID = b.OrderId
                                    where CONVERT(varchar(10), a.DateCreated, 120) >= @startDate AND CONVERT(varchar(10), a.DateCreated, 120) <= @endDate
                                      and (b.PaymentTechStatus is null or b.PaymentTechStatus = 'PaymentTech Final')
                                    group by CONVERT(varchar(10), a.DateCreated, 120)
                        ) as a
                        
                        union all
                        
                        select 'Toyota Canada Incorporated' as ProgramType, isnull(sum(a.OrderCount),0), isnull(sum(Total),0) as Total
                          from (
                                      select CONVERT(varchar(10), OrderedDate, 120) as OrderDate, sum(1) as OrderCount, sum(Total - TotalTax) as Total
                                        from SBS_ToyotaEcommerceCaEn.dbo.tblOrders
                                      where CONVERT(varchar(10), OrderedDate, 120) >= @startDate AND CONVERT(varchar(10), OrderedDate, 120) <= @endDate
                                        and (status is null or  status = 'PaymentTech Final')
                                      group by CONVERT(varchar(10), OrderedDate, 120)
                                ) as a

                        union all
                        
                        select 'General Motors' as ProgramType, isnull(sum(a.OrderCount),0), isnull(sum(Total),0) as Total
                          from (
                                      select CONVERT(varchar(10), OrderedDate, 120) as OrderDate, sum(1) as OrderCount, sum(Total - TotalTax) as Total
                                        from SBS_GMEcommerce.dbo.tblOrders
                                      where CONVERT(varchar(10), OrderedDate, 120) >= @startDate AND CONVERT(varchar(10), OrderedDate, 120) <= @endDate
                                        and (status is null or  status = 'PaymentTech Final')
                                      group by CONVERT(varchar(10), OrderedDate, 120)
                                ) as a";


            using (var connection = GetSQLConnection())
            {
                var da = new SqlDataAdapter(query, connection);
                da.SelectCommand.Parameters.Add(new SqlParameter("@startDate", SqlDbType.DateTime, 50));
                da.SelectCommand.Parameters["@startDate"].Value = startDate.ToString("yyyy-MM-dd");
                da.SelectCommand.Parameters.Add(new SqlParameter("@endDate", SqlDbType.DateTime, 50));
                da.SelectCommand.Parameters["@endDate"].Value = endDate.ToString("yyyy-MM-dd");
                da.SelectCommand.CommandTimeout = 0;
                var ds = new DataSet();
                da.Fill(ds, "tblOrderCounts");

                return ds;
            }
        }

        public DataSet GetDealerOptIn(string programList, DateTime startDate, DateTime endDate)
        {
            string connectionKey = GetURLBasedConnectionKey();
            bool appendUnion = false;
            StringBuilder sb = new StringBuilder();

            if (programList.ToLower().Contains("general motors") || programList.ToLower().Contains("all"))
            {
                sb.Append(@"select distinct a.ProgramType, SBS_GMEcommerce.dbo.DecryptAES256String(a.Email,'8UHjPgXZzXCGkhxV2QCnooyJexUzvJrO') as Email, a.DealerNumber,SBS_GMEcommerce.dbo.DecryptAES256String(a.OrderName,'8UHjPgXZzXCGkhxV2QCnooyJexUzvJrO') as OrderName from SBS_GMEcommerce.dbo.tblOrders a 
                             inner join SBS_Addresses.dbo.tblEqsAddresses b 
                                on a.CustomerBaanNumber = b.BizPrtnrID 
                             where a.IsSubscribed = 1 
                               and b.SalesOffice= '648165' -- USA sales office 
                               and b.Country = 'USA'
                               and a.CustomerBaanNumber not in ('201293101') 
                               and convert(varchar(10), a.OrderedDate, 120) >= @startDate and convert(varchar(10), a.OrderedDate, 120) <= @endDate
                             union
                            select distinct Program, EmailAddress, DealerNumber, FirstName + ' ' + LastName from SBS_SharedResource.dbo.tblProgramInformation
                             where Program = 'GM' and (Type = 'DigitalPromotions' or OptInDigitalPromotions = 1)
                               and DealerNumber is not null
                               and convert(varchar(10), CreateDate, 120) >= @startDate and convert(varchar(10), CreateDate, 120) <= @endDate");  

                appendUnion = true;
            }

            if (programList.ToLower().Contains("harley davidson") || programList.ToLower().Contains("all"))
            {
                if (appendUnion)
                    sb.Append(" union ");
				// HD after market user(201083834)
				sb.Append(@"select distinct 'Harley-Davidson' as ProgramType, SBS_HdEcommerce.dbo.DecryptAES256String(a.Email,'8UHjPgXZzXCGkhxV2QCnooyJexUzvJrO') as Email, a.DealerNumber, SBS_HdEcommerce.dbo.DecryptAES256String(a.OrderName,'8UHjPgXZzXCGkhxV2QCnooyJexUzvJrO') as OrderName from SBS_HdEcommerce.dbo.tblOrders a 
                             inner join SBS_Addresses.dbo.tblEqsAddresses b 
                                on a.CustomerBaanNumber = b.BizPrtnrID 
                             where a.IsSubscribed = 1 
                               and a.CustomerBaanNumber not in ('201083834') 
                               and b.Country = 'USA'
                               and convert(varchar(10), a.OrderedDate, 120) >= @startDate and convert(varchar(10), a.OrderedDate, 120) <= @endDate
							union
						   select distinct Program, EmailAddress, DealerNumber, FirstName +' ' + LastName from SBS_SharedResource.dbo.tblProgramInformation
							where Program = 'Harley-Davidson' and (Type = 'DigitalPromotions' or OptInDigitalPromotions = 1)
						      and DealerNumber is not null
							  and convert(varchar(10), CreateDate, 120) >= @startDate and convert(varchar(10), CreateDate, 120) <= @endDate");  


				appendUnion = true;
            }

            if (programList.ToLower().Contains("honda/acura") || programList.ToLower().Contains("all"))
            {
                if (appendUnion)
                    sb.Append(" union ");

                sb.Append(@"select distinct a.ProgramType, SBS_HondaEcommerceUSA.dbo.DecryptAES256String(a.Email,'8UHjPgXZzXCGkhxV2QCnooyJexUzvJrO') as Email, a.DealerNumber, SBS_HondaEcommerceUSA.dbo.DecryptAES256String(a.OrderName,'8UHjPgXZzXCGkhxV2QCnooyJexUzvJrO') as OrderName from SBS_HondaEcommerceUSA.dbo.tblOrders a 
                             inner join SBS_Addresses.dbo.tblEqsAddresses b 
                                on a.CustomerBaanNumber = b.BizPrtnrID 
                             where a.IsSubscribed = 1 
                               and b.Country = 'USA'
                               and a.CustomerBaanNumber not in ('201059957','201059959') 
                               and convert(varchar(10), a.OrderedDate, 120) >= @startDate AND convert(varchar(10), a.OrderedDate, 120) <= @endDate 
                             union
                            select distinct Program, EmailAddress, DealerNumber, FirstName + ' ' + LastName from SBS_SharedResource.dbo.tblProgramInformation
                             where Program in ('honda','acura') 
                               and DealerNumber is not null
                               and convert(varchar(10), CreateDate, 120) >= @startDate and convert(varchar(10), CreateDate, 120) <= @endDate");    // Honda(201059957) and Acura(201059959) after market user
                appendUnion = true;
            }

            if (programList.ToLower().Contains("honda lac") || programList.ToLower().Contains("all"))
            {
                if (appendUnion)
                    sb.Append(" union ");

                sb.Append(@"select distinct 'Honda LAC' as ProgramType, SBS_HondaEcommerceLAC.dbo.DecryptAES256String(a.Email,'8UHjPgXZzXCGkhxV2QCnooyJexUzvJrO') as Email, a.DealerNumber, SBS_HondaEcommerceLAC.dbo.DecryptAES256String(a.OrderName,'8UHjPgXZzXCGkhxV2QCnooyJexUzvJrO') as OrderName from SBS_HondaEcommerceLAC.dbo.tblOrders a 
                             inner join SBS_Addresses.dbo.tblEqsAddresses b 
                                on a.CustomerBaanNumber = b.BizPrtnrID 
                             where a.IsSubscribed = 1 
                               and b.Country = 'USA'
                               and convert(varchar(10), a.OrderedDate, 120) >= @startDate AND convert(varchar(10), a.OrderedDate, 120) <= @endDate ");
                appendUnion = true;
            }

            if (programList.ToLower().Contains("honda marine") || programList.ToLower().Contains("all"))
            {
                if (appendUnion)
                    sb.Append(" union ");

                sb.Append(@"select distinct 'Honda Marine' as ProgramType, SBS_HondaEcommerceMarine.dbo.DecryptAES256String(a.Email,'8UHjPgXZzXCGkhxV2QCnooyJexUzvJrO') as Email, a.DealerNumber, SBS_HondaEcommerceMarine.dbo.DecryptAES256String(a.OrderName,'8UHjPgXZzXCGkhxV2QCnooyJexUzvJrO') as OrderName from SBS_HondaEcommerceMarine.dbo.tblOrders a 
                             inner join SBS_Addresses.dbo.tblEqsAddresses b 
                                on a.CustomerBaanNumber = b.BizPrtnrID 
                             where a.IsSubscribed = 1 
                               and b.Country = 'USA'
                               and convert(varchar(10), a.OrderedDate, 120) >= @startDate AND convert(varchar(10), a.OrderedDate, 120) <= @endDate ");
                appendUnion = true;
            }

            if (programList.ToLower().Contains("honda motorcycle") || programList.ToLower().Contains("all"))
            {
                if (appendUnion)
                    sb.Append(" union ");

                sb.Append(@"select distinct 'Honda Motorcycle' as ProgramType, SBS_HondaEcommerceMotorcycle.dbo.DecryptAES256String(a.Email,'8UHjPgXZzXCGkhxV2QCnooyJexUzvJrO') as Email, a.DealerNumber, SBS_HondaEcommerceMotorcycle.dbo.DecryptAES256String(a.OrderName,'8UHjPgXZzXCGkhxV2QCnooyJexUzvJrO') as OrderName from SBS_HondaEcommerceMotorcycle.dbo.tblOrders a 
                             inner join SBS_Addresses.dbo.tblEqsAddresses b 
                                on a.CustomerBaanNumber = b.BizPrtnrID 
                             where a.IsSubscribed = 1 
                               and b.Country = 'USA'
                               and convert(varchar(10), a.OrderedDate, 120) >= @startDate AND convert(varchar(10), a.OrderedDate, 120) <= @endDate ");
                appendUnion = true;
            }

            if (programList.ToLower().Contains("honda power equipment") || programList.ToLower().Contains("all"))
            {
                if (appendUnion)
                    sb.Append(" union ");

                sb.Append(@"select distinct 'Honda Power Equipment' as ProgramType, SBS_HondaEcommercePowerEquipment.dbo.DecryptAES256String(a.Email,'8UHjPgXZzXCGkhxV2QCnooyJexUzvJrO') as Email, a.DealerNumber, SBS_HondaEcommercePowerEquipment.dbo.DecryptAES256String(a.OrderName,'8UHjPgXZzXCGkhxV2QCnooyJexUzvJrO') as OrderName from SBS_HondaEcommercePowerEquipment.dbo.tblOrders a 
                             inner join SBS_Addresses.dbo.tblEqsAddresses b 
                                on a.CustomerBaanNumber = b.BizPrtnrID 
                             where a.IsSubscribed = 1 
                               and b.Country = 'USA'
                               and convert(varchar(10), a.OrderedDate, 120) >= @startDate AND convert(varchar(10), a.OrderedDate, 120) <= @endDate ");
                appendUnion = true;
            }

            if (programList.ToLower().Contains("kia") || programList.ToLower().Contains("all"))
            {
                if (appendUnion)
                    sb.Append(" union ");

                sb.Append(@"select distinct 'KIA' as ProgramType, SBS_KiaEcommerce.dbo.DecryptAES256String(a.Email,'8UHjPgXZzXCGkhxV2QCnooyJexUzvJrO') as Email, a.DealerNumber, SBS_KiaEcommerce.dbo.DecryptAES256String(a.OrderName,'8UHjPgXZzXCGkhxV2QCnooyJexUzvJrO') as OrderName from SBS_KiaEcommerce.dbo.tblOrders a 
                             inner join SBS_Addresses.dbo.tblEqsAddresses b 
                                on a.CustomerBaanNumber = b.BizPrtnrID 
                             where a.IsSubscribed = 1 
                               and b.Country = 'USA'
                               and a.CustomerBaanNumber not in ('200725039') 
                               and convert(varchar(10), a.OrderedDate, 120) >= @startDate AND convert(varchar(10), a.OrderedDate, 120) <= @endDate "); // KIA after market user(200725039)
                appendUnion = true;
            }

            if (programList.ToLower().Contains("mopar") || programList.ToLower().Contains("all"))
            {
                if (appendUnion)
                    sb.Append(" union ");

                sb.Append(@"select distinct 'Mopar' as ProgramType, SBS_MoparEcommerce.dbo.DecryptAES256String(a.Email,'8UHjPgXZzXCGkhxV2QCnooyJexUzvJrO') as Email, a.DealerNumber, SBS_MoparEcommerce.dbo.DecryptAES256String(a.OrderName,'8UHjPgXZzXCGkhxV2QCnooyJexUzvJrO') as OrderName from SBS_MoparEcommerce.dbo.tblOrders a 
                             inner join SBS_Addresses.dbo.tblEqsAddresses b 
                                on a.CustomerBaanNumber = b.BizPrtnrID 
                             where a.IsSubscribed = 1 
                               and b.SalesOffice in ('648185', '648186')-- USA sales office 
                               and b.Country = 'USA'
                               and a.CustomerBaanNumber not in ('201098614','201102328') 
                               and convert(varchar(10), a.OrderedDate, 120) >= @startDate AND convert(varchar(10), a.OrderedDate, 120) <= @endDate 
                			union
						   select distinct Program, EmailAddress, DealerNumber, FirstName + ' ' + LastName from SBS_SharedResource.dbo.tblProgramInformation
							where Program = 'Mopar' and (Type = 'DigitalPromotions' or OptInDigitalPromotions = 1)
						      and DealerNumber is not null
							  and convert(varchar(10), CreateDate, 120) >= @startDate and convert(varchar(10), CreateDate, 120) <= @endDate");  //  Mopar after market user(201098614,201102328)

                appendUnion = true;
            }

            if (programList.ToLower().Contains("porsche") || programList.ToLower().Contains("all"))
            {
                if (appendUnion)
                    sb.Append(" union ");

                sb.Append(@"select distinct a.ProgramType, SBS_PorscheEcommerce.dbo.DecryptAES256String(a.Email,'8UHjPgXZzXCGkhxV2QCnooyJexUzvJrO') as Email, a.DealerNumber, SBS_PorscheEcommerce.dbo.DecryptAES256String(a.OrderName,'8UHjPgXZzXCGkhxV2QCnooyJexUzvJrO') as OrderName from SBS_PorscheEcommerce.dbo.tblOrders a 
                             inner join SBS_Addresses.dbo.tblEqsAddresses b 
                                on a.CustomerBaanNumber = b.BizPrtnrID 
                             where a.IsSubscribed = 1 
                               and b.SalesOffice = '648133' -- USA sales office 
                               and b.Country = 'USA'
                               and a.CustomerBaanNumber not in ('201083498','200019789') 
                               and convert(varchar(10), a.OrderedDate, 120) >= @startDate AND convert(varchar(10), a.OrderedDate, 120) <= @endDate"); // Porsche after market users(201083498,200019789)
                appendUnion = true;
            }

            if (programList.ToLower().Contains("toyota/lexus") || programList.ToLower().Contains("all"))
            {
                if (appendUnion)
                    sb.Append(" union ");

                if (DateTime.Now > Convert.ToDateTime("1/16/2019", CultureInfo.GetCultureInfo("en-US").DateTimeFormat))
                {
                    sb.Append(@"select distinct a.ProgramType, SBS_ToyotaEcommerce.dbo.DecryptAES256String(a.Email,'8UHjPgXZzXCGkhxV2QCnooyJexUzvJrO') as Email, a.DealerNumber, SBS_ToyotaEcommerce.dbo.DecryptAES256String(a.OrderName,'8UHjPgXZzXCGkhxV2QCnooyJexUzvJrO') as OrderName from SBS_ToyotaEcommerce.dbo.tblOrders a 
                             inner join SBS_Addresses.dbo.tblEqsAddresses b 
                                on a.CustomerBaanNumber = b.BizPrtnrID 
                             where a.IsSubscribed = 1 
                               and b.Country = 'USA'
                               and a.CustomerBaanNumber not in ('200997527','201059965')
                               and convert(varchar(10), a.OrderedDate, 120) >= @startDate AND convert(varchar(10), a.OrderedDate, 120) <= @endDate
							union
						   select distinct Program, EmailAddress, DealerNumber, FirstName +' ' + LastName from SBS_SharedResource.dbo.tblProgramInformation
							where Program in ('Toyota','Lexus') and (Type = 'DigitalPromotions' or OptInDigitalPromotions = 1)
						      and DealerNumber is not null
							  and convert(varchar(10), CreateDate, 120) >= @startDate and convert(varchar(10), CreateDate, 120) <= @endDate");  // Toyota and Lexus after market users(200997527,201059965)

                }
                else
                {
                    sb.Append(@"select distinct a.ProgramType, a.Email, a.DealerNumber, a.OrderName from SBS_ToyotaEcommerce.dbo.tblOrders a 
                             inner join SBS_Addresses.dbo.tblEqsAddresses b 
                                on a.CustomerBaanNumber = b.BizPrtnrID 
                             where a.IsSubscribed = 1 
                               and b.Country = 'USA'
                               and a.CustomerBaanNumber not in ('200997527','201059965')
                               and convert(varchar(10), a.OrderedDate, 120) >= @startDate AND convert(varchar(10), a.OrderedDate, 120) <= @endDate
                             union
                            select distinct Program, EmailAddress, DealerNumber, FirstName + ' ' + LastName from SBS_SharedResource.dbo.tblProgramInformation
                             where Program in ('Toyota','Lexus') and (Type = 'DigitalPromotions' or OptInDigitalPromotions = 1)
                               and DealerNumber is not null
                               and convert(varchar(10), CreateDate, 120) >= @startDate and convert(varchar(10), CreateDate, 120) <= @endDate");  // Toyota and Lexus after market users(200997527,201059965)
                }
                appendUnion = true;
            }

            if (programList.ToLower().Contains("vw/audi") || programList.ToLower().Contains("all"))
            {
                if (appendUnion)
                    sb.Append(" union ");
                if (DateTime.Now > Convert.ToDateTime("1/16/2019", CultureInfo.GetCultureInfo("en-US").DateTimeFormat))
                {
                    sb.Append(@"select distinct a.ProgramType, SBS_VwEcommerce.dbo.DecryptAES256String(a.Email,'8UHjPgXZzXCGkhxV2QCnooyJexUzvJrO') as Email, a.DealerNumber, SBS_VwEcommerce.dbo.DecryptAES256String(a.OrderName,'8UHjPgXZzXCGkhxV2QCnooyJexUzvJrO') as OrderName from SBS_VwEcommerce.dbo.tblOrders a 
                             inner join SBS_Addresses.dbo.tblEqsAddresses b 
                                on a.CustomerBaanNumber = b.BizPrtnrID 
                             where a.IsSubscribed = 1 
                               and b.SalesOffice in ('648130','648131')  -- USA sales office
                               and b.Country = 'USA'
                               and a.CustomerBaanNumber not in ('200725351','201061620','200019789')
                               and convert(varchar(10), a.OrderedDate, 120) >= @startDate AND convert(varchar(10), a.OrderedDate, 120) <= @endDate");  // VW and AUDI after market users(200725351,201061620,200019789)
                }
                else
                {
                    sb.Append(@"select distinct a.ProgramType, a.Email, a.DealerNumber, a.OrderName from SBS_VwEcommerce.dbo.tblOrders a 
                             inner join SBS_Addresses.dbo.tblEqsAddresses b 
                                on a.CustomerBaanNumber = b.BizPrtnrID 
                             where a.IsSubscribed = 1 
                               and b.SalesOffice in ('648130','648131')  -- USA sales office
                               and b.Country = 'USA'
                               and a.CustomerBaanNumber not in ('200725351','201061620','200019789')
                               and convert(varchar(10), a.OrderedDate, 120) >= @startDate AND convert(varchar(10), a.OrderedDate, 120) <= @endDate");  // VW and AUDI after market users(200725351,201061620,200019789)
                }
                appendUnion = true;
            }
            sb.Append(" order by 1,2");

            using (var connection = GetSQLConnection())
            {
                var da = new SqlDataAdapter(sb.ToString(), connection);
                da.SelectCommand.Parameters.Add(new SqlParameter("@startDate", SqlDbType.DateTime, 50));
                da.SelectCommand.Parameters["@startDate"].Value = startDate.ToString("yyyy-MM-dd");
                da.SelectCommand.Parameters.Add(new SqlParameter("@endDate", SqlDbType.DateTime, 50));
                da.SelectCommand.Parameters["@endDate"].Value = endDate.ToString("yyyy-MM-dd");
                da.SelectCommand.CommandTimeout = 0;
                var ds = new DataSet();
                da.Fill(ds, "tblDealerOptIn");
                return ds;
            }
        }

        public DataSet GetDealerContacts(string programList, DateTime startDate, DateTime endDate)
        {
            string connectionKey = GetURLBasedConnectionKey();
            bool appendUnion = false;
            StringBuilder sb = new StringBuilder();

            if (programList.ToLower().Contains("general motors") || programList.ToLower().Contains("all"))
            {
                sb.Append(@"select distinct a.ProgramType, SBS_GMEcommerce.dbo.DecryptAES256String(a.Email,'8UHjPgXZzXCGkhxV2QCnooyJexUzvJrO') as Email, a.DealerNumber from SBS_GMEcommerce.dbo.tblOrders a 
                             inner join SBS_Addresses.dbo.tblEqsAddresses b 
                                on a.CustomerBaanNumber = b.BizPrtnrID 
                             where LEN(a.Email) > 0 and a.Email <> 'mO55W3H6ZV1VY/cLDAK4VA=='  -- Encryption for an empty value
                               and a.CustomerBaanNumber not in ('201293101') 
                               and b.SalesOffice= '648165' -- USA sales office 
                               and b.Country = 'USA'
                               and convert(varchar(10), a.OrderedDate, 120) >= @startDate and convert(varchar(10), a.OrderedDate, 120) <= @endDate");
                appendUnion = true;
            }

            if (programList.ToLower().Contains("harley davidson") || programList.ToLower().Contains("all"))
            {
                if (appendUnion)
                    sb.Append(" union ");

                sb.Append(@"select distinct 'Harley-Davidson' as ProgramType, SBS_HdEcommerce.dbo.DecryptAES256String(a.Email,'8UHjPgXZzXCGkhxV2QCnooyJexUzvJrO') as Email, a.DealerNumber from SBS_HdEcommerce.dbo.tblOrders a 
                             inner join SBS_Addresses.dbo.tblEqsAddresses b 
                                on a.CustomerBaanNumber = b.BizPrtnrID 
                             where LEN(a.Email) > 0 and a.Email <> 'mO55W3H6ZV1VY/cLDAK4VA=='  -- Encryption for an empty value
                               and b.Country = 'USA'
                               and a.CustomerBaanNumber not in ('201083834') 
                               and convert(varchar(10), a.OrderedDate, 120) >= @startDate and convert(varchar(10), a.OrderedDate, 120) <= @endDate");  // HD after market user(201083834)
                appendUnion = true;
            }

            if (programList.ToLower().Contains("honda/acura") || programList.ToLower().Contains("all"))
            {
                if (appendUnion)
                    sb.Append(" union ");

                sb.Append(@"select distinct a.ProgramType, SBS_HondaEcommerceUSA.dbo.DecryptAES256String(a.Email,'8UHjPgXZzXCGkhxV2QCnooyJexUzvJrO') as Email, a.DealerNumber from SBS_HondaEcommerceUSA.dbo.tblOrders a 
                             inner join SBS_Addresses.dbo.tblEqsAddresses b 
                                on a.CustomerBaanNumber = b.BizPrtnrID 
                             where LEN(a.Email) > 0 and a.Email <> 'mO55W3H6ZV1VY/cLDAK4VA=='  -- Encryption for an empty value
                               and b.Country = 'USA'
                               and a.CustomerBaanNumber not in ('201059957','201059959') 
                               and convert(varchar(10), a.OrderedDate, 120) >= @startDate AND convert(varchar(10), a.OrderedDate, 120) <= @endDate"); // Honda(201059957) and Acura(201059959) after market user
                appendUnion = true;
            }

            if (programList.ToLower().Contains("honda lac") || programList.ToLower().Contains("all"))
            {
                if (appendUnion)
                    sb.Append(" union ");

                sb.Append(@"select distinct 'Honda LAC' as ProgramType, SBS_HondaEcommerceLAC.dbo.DecryptAES256String(a.Email,'8UHjPgXZzXCGkhxV2QCnooyJexUzvJrO') as Email, a.DealerNumber from SBS_HondaEcommerceLAC.dbo.tblOrders a 
                             inner join SBS_Addresses.dbo.tblEqsAddresses b 
                                on a.CustomerBaanNumber = b.BizPrtnrID 
                             where LEN(a.Email) > 0 and a.Email <> 'mO55W3H6ZV1VY/cLDAK4VA=='  -- Encryption for an empty value
                               and b.Country = 'USA'
                               and convert(varchar(10), a.OrderedDate, 120) >= @startDate AND convert(varchar(10), a.OrderedDate, 120) <= @endDate ");
                appendUnion = true;
            }

            if (programList.ToLower().Contains("honda marine") || programList.ToLower().Contains("all"))
            {
                if (appendUnion)
                    sb.Append(" union ");

                sb.Append(@"select distinct 'Honda Marine' as ProgramType, SBS_HondaEcommerceMarine.dbo.DecryptAES256String(a.Email,'8UHjPgXZzXCGkhxV2QCnooyJexUzvJrO') as Email, a.DealerNumber from SBS_HondaEcommerceMarine.dbo.tblOrders a 
                             inner join SBS_Addresses.dbo.tblEqsAddresses b 
                                on a.CustomerBaanNumber = b.BizPrtnrID 
                             where LEN(a.Email) > 0 and a.Email <> 'mO55W3H6ZV1VY/cLDAK4VA=='  -- Encryption for an empty value 
                               and b.Country = 'USA'
                               and convert(varchar(10), a.OrderedDate, 120) >= @startDate AND convert(varchar(10), a.OrderedDate, 120) <= @endDate ");
                appendUnion = true;
            }

            if (programList.ToLower().Contains("honda motorcycle") || programList.ToLower().Contains("all"))
            {
                if (appendUnion)
                    sb.Append(" union ");

                sb.Append(@"select distinct 'Honda Motorcycle' as ProgramType, SBS_HondaEcommerceMotorcycle.dbo.DecryptAES256String(a.Email,'8UHjPgXZzXCGkhxV2QCnooyJexUzvJrO') as Email, a.DealerNumber from SBS_HondaEcommerceMotorcycle.dbo.tblOrders a 
                             inner join SBS_Addresses.dbo.tblEqsAddresses b 
                                on a.CustomerBaanNumber = b.BizPrtnrID 
                             where LEN(a.Email) > 0 and a.Email <> 'mO55W3H6ZV1VY/cLDAK4VA=='  -- Encryption for an empty value 
                               and b.Country = 'USA'
                               and convert(varchar(10), a.OrderedDate, 120) >= @startDate AND convert(varchar(10), a.OrderedDate, 120) <= @endDate ");
                appendUnion = true;
            }

            if (programList.ToLower().Contains("honda power equipment") || programList.ToLower().Contains("all"))
            {
                if (appendUnion)
                    sb.Append(" union ");

                sb.Append(@"select distinct 'Honda Power Equipment' as ProgramType, SBS_HondaEcommercePowerEquipment.dbo.DecryptAES256String(a.Email,'8UHjPgXZzXCGkhxV2QCnooyJexUzvJrO') as Email, a.DealerNumber from SBS_HondaEcommercePowerEquipment.dbo.tblOrders a 
                             inner join SBS_Addresses.dbo.tblEqsAddresses b 
                                on a.CustomerBaanNumber = b.BizPrtnrID 
                             where LEN(a.Email) > 0 and a.Email <> 'mO55W3H6ZV1VY/cLDAK4VA=='  -- Encryption for an empty value 
                               and b.Country = 'USA'
                               and convert(varchar(10), a.OrderedDate, 120) >= @startDate AND convert(varchar(10), a.OrderedDate, 120) <= @endDate ");
                appendUnion = true;
            }

            if (programList.ToLower().Contains("kia") || programList.ToLower().Contains("all"))
            {
                if (appendUnion)
                    sb.Append(" union ");

                sb.Append(@"select distinct 'KIA' as ProgramType, SBS_KiaEcommerce.dbo.DecryptAES256String(a.Email,'8UHjPgXZzXCGkhxV2QCnooyJexUzvJrO') as Email, a.DealerNumber from SBS_KiaEcommerce.dbo.tblOrders a 
                             inner join SBS_Addresses.dbo.tblEqsAddresses b 
                                on a.CustomerBaanNumber = b.BizPrtnrID 
                             where LEN(a.Email) > 0 and a.Email <> 'mO55W3H6ZV1VY/cLDAK4VA=='  -- Encryption for an empty value  
                               and b.Country = 'USA'
                               and a.CustomerBaanNumber not in ('200725039') 
                               and convert(varchar(10), a.OrderedDate, 120) >= @startDate AND convert(varchar(10), a.OrderedDate, 120) <= @endDate "); // KIA after market user(200725039)
                appendUnion = true;
            }

            if (programList.ToLower().Contains("mopar") || programList.ToLower().Contains("all"))
            {
                if (appendUnion)
                    sb.Append(" union ");

                sb.Append(@"select distinct 'Mopar' as ProgramType, SBS_MoparEcommerce.dbo.DecryptAES256String(a.Email,'8UHjPgXZzXCGkhxV2QCnooyJexUzvJrO') as Email, a.DealerNumber from SBS_MoparEcommerce.dbo.tblOrders a 
                             inner join SBS_Addresses.dbo.tblEqsAddresses b 
                                on a.CustomerBaanNumber = b.BizPrtnrID 
                             where LEN(a.Email) > 0 and a.Email <> 'mO55W3H6ZV1VY/cLDAK4VA=='  -- Encryption for an empty value 
                               and b.Country = 'USA'
                               and b.SalesOffice in ('648185', '648186')-- USA sales office 
                               and a.CustomerBaanNumber not in ('201098614','201102328') 
                               and convert(varchar(10), a.OrderedDate, 120) >= @startDate AND convert(varchar(10), a.OrderedDate, 120) <= @endDate "); //  Mopar after market user(201098614,201102328)
                appendUnion = true;
            }

            if (programList.ToLower().Contains("porsche") || programList.ToLower().Contains("all"))
            {
                if (appendUnion)
                    sb.Append(" union ");

                sb.Append(@"select distinct a.ProgramType, SBS_PorscheEcommerce.dbo.DecryptAES256String(a.Email,'8UHjPgXZzXCGkhxV2QCnooyJexUzvJrO') as Email, a.DealerNumber from SBS_PorscheEcommerce.dbo.tblOrders a 
                             inner join SBS_Addresses.dbo.tblEqsAddresses b 
                                on a.CustomerBaanNumber = b.BizPrtnrID 
                             where LEN(a.Email) > 0 and a.Email <> 'mO55W3H6ZV1VY/cLDAK4VA=='  -- Encryption for an empty value  
                               and b.Country = 'USA'
                               and b.SalesOffice = '648133' -- USA sales office 
                               and a.CustomerBaanNumber not in ('201083498','200019789') 
                               and convert(varchar(10), a.OrderedDate, 120) >= @startDate AND convert(varchar(10), a.OrderedDate, 120) <= @endDate"); // Porsche after market users(201083498,200019789)
                appendUnion = true;
            }

            if (programList.ToLower().Contains("toyota/lexus") || programList.ToLower().Contains("all"))
            {
                if (appendUnion)
                    sb.Append(" union ");

                if (DateTime.Now > Convert.ToDateTime("1/16/2019", CultureInfo.GetCultureInfo("en-US").DateTimeFormat))
                {
                    sb.Append(@"select distinct a.ProgramType, SBS_ToyotaEcommerce.dbo.DecryptAES256String(a.Email,'8UHjPgXZzXCGkhxV2QCnooyJexUzvJrO') as Email, a.DealerNumber from SBS_ToyotaEcommerce.dbo.tblOrders a 
                             inner join SBS_Addresses.dbo.tblEqsAddresses b 
                                on a.CustomerBaanNumber = b.BizPrtnrID 
                             where LEN(a.Email) > 0 and a.Email <> 'mO55W3H6ZV1VY/cLDAK4VA=='  -- Encryption for an empty value 
                               and b.Country = 'USA'
                               and a.CustomerBaanNumber not in ('200997527','201059965')
                               and convert(varchar(10), a.OrderedDate, 120) >= @startDate AND convert(varchar(10), a.OrderedDate, 120) <= @endDate");  // Toyota and Lexus after market users(200997527,201059965)
                }
                else
                {
                    sb.Append(@"select distinct a.ProgramType, a.Email, a.DealerNumber from SBS_ToyotaEcommerce.dbo.tblOrders a 
                             inner join SBS_Addresses.dbo.tblEqsAddresses b 
                                on a.CustomerBaanNumber = b.BizPrtnrID 
                             where LEN(a.Email) > 0   
                               and b.Country = 'USA'
                               and a.CustomerBaanNumber not in ('200997527','201059965')
                               and convert(varchar(10), a.OrderedDate, 120) >= @startDate AND convert(varchar(10), a.OrderedDate, 120) <= @endDate");  // Toyota and Lexus after market users(200997527,201059965)
                }
                appendUnion = true;
            }

            if (programList.ToLower().Contains("vw/audi") || programList.ToLower().Contains("all"))
            {
                if (appendUnion)
                    sb.Append(" union ");

                if (DateTime.Now > Convert.ToDateTime("1/16/2019", CultureInfo.GetCultureInfo("en-US").DateTimeFormat))
                {
                    sb.Append(@"select distinct a.ProgramType, SBS_VwEcommerce.dbo.DecryptAES256String(a.Email,'8UHjPgXZzXCGkhxV2QCnooyJexUzvJrO') as Email, a.DealerNumber from SBS_VwEcommerce.dbo.tblOrders a 
                             inner join SBS_Addresses.dbo.tblEqsAddresses b 
                                on a.CustomerBaanNumber = b.BizPrtnrID 
                             where LEN(a.Email) > 0 and a.Email <> 'mO55W3H6ZV1VY/cLDAK4VA=='  -- Encryption for an empty value 
                               and b.Country = 'USA'
                               and b.SalesOffice in ('648130','648131')  -- USA sales office
                               and a.CustomerBaanNumber not in ('200725351','201061620','200019789')
                               and convert(varchar(10), a.OrderedDate, 120) >= @startDate AND convert(varchar(10), a.OrderedDate, 120) <= @endDate");  // VW and AUDI after market users(200725351,201061620,200019789)
                }
                else
                {
                    sb.Append(@"select distinct a.ProgramType, a.Email, a.DealerNumber from SBS_VwEcommerce.dbo.tblOrders a 
                             inner join SBS_Addresses.dbo.tblEqsAddresses b 
                                on a.CustomerBaanNumber = b.BizPrtnrID 
                             where LEN(a.Email) > 0  
                               and b.Country = 'USA'
                               and b.SalesOffice in ('648130','648131')  -- USA sales office
                               and a.CustomerBaanNumber not in ('200725351','201061620','200019789')
                               and convert(varchar(10), a.OrderedDate, 120) >= @startDate AND convert(varchar(10), a.OrderedDate, 120) <= @endDate");  // VW and AUDI after market users(200725351,201061620,200019789)
                }
                appendUnion = true;
            }
            sb.Append(" order by 1,2");

            using (var connection = GetSQLConnection())
            {
                var da = new SqlDataAdapter(sb.ToString(), connection);
                da.SelectCommand.Parameters.Add(new SqlParameter("@startDate", SqlDbType.DateTime, 50));
                da.SelectCommand.Parameters["@startDate"].Value = startDate.ToString("yyyy-MM-dd");
                da.SelectCommand.Parameters.Add(new SqlParameter("@endDate", SqlDbType.DateTime, 50));
                da.SelectCommand.Parameters["@endDate"].Value = endDate.ToString("yyyy-MM-dd");
                da.SelectCommand.CommandTimeout = 0;
                var ds = new DataSet();
                da.Fill(ds, "tblDealerContacts");
                return ds;
            }
        }

        public DataSet GetLnWarehouseItemInfo()
        {
            DataSet ds = new DataSet();

            using (OleDbConnection conn = GetOleDbConnection("OracleLnQuery"))
            {

                OleDbDataAdapter oleDbDA = new OleDbDataAdapter();
                OleDbCommand oleDbCmd = new OleDbCommand(GetWarehouseSQL(),conn);

                oleDbDA.SelectCommand = oleDbCmd;
                oleDbDA.SelectCommand.CommandTimeout = 0;
                oleDbDA.Fill(ds, "LnWarehouseItemInfo");
                oleDbCmd.Dispose();
            }
            return ds;
        }
        private string GetWarehouseSQL()
        {
            System.Text.StringBuilder oSQL = new System.Text.StringBuilder();
            oSQL.Append("SELECT Trim(ttcibd001151_item_general.T$ITEM) AS ITEM, tcibd901_shadow.T$OSSI$C, tcibd901_shadow.T$SSIT$C, tcibd901_shadow.T$LCYC$C, ttcibd001151_item_general.T$CSIG, ");
            oSQL.Append("ttcibd001151_item_general.T$DSCA, ttcibd001151_item_general.T$WGHT, ttcibd001151_item_general.T$CTYO, ttcibd001151_item_general.T$CCDE, ");
            oSQL.Append("tcibd004_x_ref.T$AITC, tdipu001_purchase.T$OTBP ");
            oSQL.Append("FROM SSA.ttcibd001151 ttcibd001151_item_general ");
            oSQL.Append("LEFT JOIN SSA.ttdisa001151 tdisa001_sales ");
            oSQL.Append("ON ttcibd001151_item_general.T$ITEM = tdisa001_sales.T$ITEM ");
            oSQL.Append("LEFT JOIN SSA.ttcibd004151 tcibd004_x_ref ");
            oSQL.Append("ON ttcibd001151_item_general.T$ITEM = tcibd004_x_ref.T$ITEM ");
            oSQL.Append("LEFT JOIN SSA.ttdipu001151 tdipu001_purchase ");
            oSQL.Append("ON ttcibd001151_item_general.T$ITEM = tdipu001_purchase.T$ITEM ");
            oSQL.Append("LEFT JOIN SSA.ttcibd901151 tcibd901_shadow ");
            oSQL.Append("ON ttcibd001151_item_general.T$ITEM = tcibd901_shadow.T$ITEM$C ");
            oSQL.Append("WHERE (ttcibd001151_item_general.T$CSIG = ' ' Or ttcibd001151_item_general.T$CSIG Is Null Or ttcibd001151_item_general.T$CSIG = '003' Or ttcibd001151_item_general.T$CSIG = '040') ");
            oSQL.Append("AND(ttcibd001151_item_general.T$KITM = 1 Or ttcibd001151_item_general.T$KITM = 2) AND ttcibd001151_item_general.T$CITG = '580EQS' AND tdisa001_sales.T$QIDD <> 1 ORDER BY ttcibd001151_item_general.T$ITEM");

            return oSQL.ToString();
        }
        #endregion
    }
}