using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net.Mail;


namespace PortalManager
{
    public class BusinessAccessLayer
    {
    
    
    
    }

    public class PortalUser
    {

        private String _firstDate;
        private int _status;
        private string _page;
        private int _loginCounter;
        private string _firstName;
        private string _lastName;
        private string _email;
        private string _password;
        private string _userName;

        private string _user = string.Empty;
        private string _role = string.Empty;
        private int _siteId;
       

        public String FirstDate
        {
            get
            {
                return _firstDate;
            }

            set
            {
                _firstDate = value;
            }
        }


        public int Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }


        public int LoginCounter
        {
            get { return _loginCounter; }
            set { _loginCounter = value; }
        }

        public string Page
        {
            get
            {
                return _page;
            }

            set
            {
                _page = value;
            }
        }


        public string FirstName
        {
            get
            {
                return _firstName;
            }
            set
            {
                _firstName = value;
            }
        }


        public string LastName
        {
            get
            {
                return _lastName;
            }
            set
            {
                _lastName = value;
            }
        }


        public string Email
        {
            get
            {
                return _email;
            }
            set
            {
                _email = value;
            }
        }

        public string UserName
        {
            get
            {
                return _userName;
            }
            set
            {
                _userName = value;
            }
        }

        public string Password
        {
            get
            {
                return _password;
            }
            set
            {
                _password = value;
            }
        }

        
        public string User
        {
            get
            {
                return _user;
            }
            set
            {
                _user = value;
            }
        }

        public string Role
        {
            get
            {
                return _role;
            }
            set
            {
                _role = value;
            }
        }

        public int SiteId
        {
            get
            {
                return _siteId;
            }
            set
            {
                _siteId = value;
            }
        }


   
        /// <summary>
        /// Method call to check the user interanl/external authentication
        /// </summary>
        /// <returns></returns>
        public string VerifyAuthentication()
        {
            DataAccessLayer objdal = new DataAccessLayer();
            return objdal.IsAuthenticated(_userName, _password);
        }

        /// <summary>
        /// Method call to set counter increment by 1 on each login
        /// </summary>
        public void SetLoginCounter()
        {
            DataAccessLayer objdal = new DataAccessLayer();
            objdal.SetCounter(_userName,_password);
        }

        /// <summary>
        /// Method call to get all site names(links)
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public DataSet GetSiteLinks(string user)
        {
            DataSet ds = new DataSet();
            DataAccessLayer objDal = new DataAccessLayer();
            ds = objDal.GetSiteNames(user);
            return ds;
        }

        /// <summary>
        /// Method call to get info on request of lost password
        /// </summary>
        /// <returns></returns>
        public DataSet FetchEmailInfo(string firstName,string lastName,string email)
        {
            DataSet ds = new DataSet();
            DataAccessLayer objDal = new DataAccessLayer();
            ds = objDal.GetEmailInfo(firstName,lastName, email);
            return ds;
        }

        /// <summary>
        /// Method to send an email with new password after request for lost password
        /// </summary>
        /// <param name="email"></param>
        /// <param name="emailSubject"></param>
        /// <param name="emailbody"></param>
        /// <returns></returns>
        public bool SendMail(string email, string emailSubject, string emailbody)
        {
            bool isMailSend;
            string fromMailAddress = ConfigurationManager.AppSettings["FromEmailAddress"];
            string toMailAddress = email;
            string bccMailAddress = ConfigurationManager.AppSettings["BccEmailAddress"];
            MailMessage mailMessage;
            mailMessage = new MailMessage(fromMailAddress, toMailAddress);
            //mailMessage.Bcc.Add(bccMailAddress);
            mailMessage.IsBodyHtml = true;
            mailMessage.Subject = emailSubject;

            mailMessage.Body = emailbody;
            
            try
            {
                SmtpClient smtp = new SmtpClient();
                smtp.Send(mailMessage);
                isMailSend = true;
            }
            catch (SmtpException exception)
            {
                // Consume - Log - Continue
                isMailSend = true;
                HttpContext.Current.Response.Write(exception.Message);
                
            }
            return isMailSend;
        }

        public DataSet GetUsers(string sitename)
        {
            DataSet ds = new DataSet();
            DataAccessLayer objdal = new DataAccessLayer();
            ds = objdal.GetUsers(sitename);
            return ds;
        }

        public DataSet SearchUsers(string searchTerm)
        {
            DataSet ds = new DataSet();
            DataAccessLayer objdal = new DataAccessLayer();
            ds = objdal.SearchUsers(searchTerm);
            return ds;
        }

        public DataSet GetSites(string sitename)
        {
            DataSet ds = new DataSet();
            DataAccessLayer objdal = new DataAccessLayer();
            ds = objdal.getSites(sitename);
            return ds;
        }

        public string GetRole(string user)
        {
            string role = null;
            DataAccessLayer objdal = new DataAccessLayer();
            role = objdal.GetRole(user);

            return role;

        }

        public DataSet GetAllUsersRoles(string type)
        {
            DataSet ds = new DataSet();
            DataAccessLayer objdal = new DataAccessLayer();
            ds = objdal.GetAllUsersOrRoles(type);
            return ds;
        }

        public DataSet GetSitesRoles(string user)
        {
            DataSet ds = new DataSet();
            DataAccessLayer objdal = new DataAccessLayer();
            ds = objdal.GetSitesRoles(user);
            return ds;
            
        }
        public DataSet GetSitesRoles(string user, string sitename)
        {
            DataSet ds = new DataSet();
            DataAccessLayer objdal = new DataAccessLayer();
            ds = objdal.GetSitesRoles(user,sitename);
            return ds;

        }

        public bool InsertUser(int siteId)
        {

            DataAccessLayer objdal = new DataAccessLayer();
            bool blnSuccess = objdal.InsertData(User,Password, Role,siteId,FirstName,LastName);
            return blnSuccess;

        }

        public bool UpdateUser(int siteId)
        {
            DataAccessLayer objdal = new DataAccessLayer();
            bool blnSuccess = objdal.UpdateUser(User, Role, siteId,FirstName,LastName);
            return blnSuccess;
        }
        public bool UpdateUsers()
        {
            DataAccessLayer objdal = new DataAccessLayer();
          
            bool blnSuccess = objdal.UpdateUsers(User, Role, SiteId);
            return blnSuccess;
        }

        public bool DeleteUser()
        {
            DataAccessLayer objdal = new DataAccessLayer();
            bool blnSuccess = objdal.DeleteUser(User);
            return blnSuccess;

        }

        public string GetRoleUserSite(string username, string sitename)
        {
            DataAccessLayer objdal = new DataAccessLayer();
            string role = string.Empty;
            role = objdal.getRoleUserSite(username, sitename);

            return role;
        }

        public bool CheckUser(string username)
        {
            DataAccessLayer objdal = new DataAccessLayer();
            bool blnValid = objdal.checkUser(username,Password);
            return blnValid;
        }

        public bool IsUserAdmin(string loggedUser)
        {
            bool blnValid = false;
            string user = ConfigurationManager.AppSettings["AdminRole"].ToString();
            if (user.ToLower().Trim() == loggedUser.ToLower().Trim())
            {
                blnValid = true;
            }
            return blnValid;

        }

        public DataSet GetAllSitesLinks()
        {
            DataSet ds = new DataSet();
            DataAccessLayer objdal = new DataAccessLayer();
            ds = objdal.GetAllSitesLinks();
            return ds;
        }

        public bool DeleteUser(int userId)
        {
             DataAccessLayer objdal = new DataAccessLayer();
             bool blnValid = objdal.DeleteUser(userId);
             return blnValid;
        }

        public DataSet GetEqsActiveSuppliers(string type)
        {
            DataSet ds = new DataSet();
            DataAccessLayer objdal = new DataAccessLayer();

            ds = objdal.GetEqsActiveSuppliers(type);

            return ds;
        }
        //jdu 11020101
        public DataSet KiaPriorDealersSales(string dealerNo, string itemNo, string fromDate, string toDate)
        {
            DataSet ds = new DataSet();
            DataAccessLayer objdal = new DataAccessLayer();

            ds = objdal.KiaPriorDealersSales(dealerNo, itemNo, fromDate, toDate);

            return ds;
        }        
        //end jdu 11020101
        public DataSet GetDealersSales(string dealerNo, string itemNo, string fromDate, string toDate, string applicationType)
        {
            DataSet ds = new DataSet();
            DataAccessLayer objdal = new DataAccessLayer();

            ds = objdal.GetDealerSales(dealerNo, itemNo, fromDate, toDate, applicationType);

            return ds;
        }
        public string ValidateDealerNumber(string dealerNo)
        {
            DataAccessLayer objdal = new DataAccessLayer();

            return objdal.ValidateDealerNumber(dealerNo);
        }
        public string ValidateItemNumber(string itemNo)
        {
            DataAccessLayer objdal = new DataAccessLayer();

            return objdal.ValidateItemNumber(itemNo);
        }

        public DataSet GetRegionalManagers(bool IsRegionalSalesManager)
        {
            DataAccessLayer objdal = new DataAccessLayer();
            return objdal.GetRegionalManagers(IsRegionalSalesManager);
        }

        public string GetAuthyUserId(string userId)
        {
            DataAccessLayer objdal = new DataAccessLayer();
            return objdal.GetAuthyUserId(userId);
        }
    }
}
