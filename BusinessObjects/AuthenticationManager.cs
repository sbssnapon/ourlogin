﻿using Eqs.Utility;
using System;

namespace PortalManager.BusinessObjects
{
    public static class AuthenticationManager
    {
        public static bool AuthenticateExtranetUser(string username, string password)
        {
            //LDAP first
            var ldapMembership = new LdapMembership();
            if (ldapMembership.ValidateUser(username, password)) return true;

            //Internal
            var objUser = new PortalUser {Password = password};
            return objUser.CheckUser(username);
        }

        public static bool UserCanAccessWebReports(string username)
        {
            var objUser = new PortalUser();
            return objUser.GetRoleUserSite(username, "Web Reports").Trim() == "Administrator" ||
                   objUser.GetRoleUserSite(username, "Web Reports").Trim() == "Manager" ||
                   objUser.GetRoleUserSite(username, "Web Reports").Trim() == "Reader";
        }
    }
}