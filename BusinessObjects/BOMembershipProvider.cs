﻿using Eqs.Utility;

public class BOMembershipProvider 
{
    /// <summary>
    /// VerifyAuthentication will first check if an external user by querying username and password match
    /// if fail, then query username is an internal user 
    /// </summary>
    /// <param name="username"></param>
    /// <param name="password"></param>
    /// <returns></returns>
    public bool ValidateUser(string username, string password)
    {
        // VerifyAuthentication will first check if an external
        PortalManager.PortalUser portalUser = new PortalManager.PortalUser();
        portalUser.UserName = username;
        portalUser.Password = password;

        // This does a lookup in SBS_PortalApps.tblPortalUserAccounts but this table doesn't actually have any passwords (except for maybe B. Mulhern) so in practice, we are only using LDAP for Login here
        string login_type = portalUser.VerifyAuthentication();
        if (login_type.Equals("valid_internal_user"))
            return true;
        else 
            return false;
    }
}
    
    

