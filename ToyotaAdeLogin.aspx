<%@ Page Language="C#" AutoEventWireup="true" Codebehind="ToyotaAdeLogin.aspx.cs" Inherits="PortalManager.ToyotaAdeLogin" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Toyota Ecommerce Site Access</title>
</head>
<body>
    <form id="form1" runat="server">
        <table width="100%" height="100%" style="font-family: verdana;">
            <tr>
                <td align="center">
                    <table>
                                    <tr>
                                        <td align="center" height="50" style="font-weight: bold;font-size: 11pt;">Toyota Ecommerce Site Access</td>
                                    </tr>
                        
                        <tr>
                            <td align="left" valign="top">
                                <table width="100%" bgcolor="#eeebe1">
                                    <tr>
                                        <td valign="middle" align="center" style="border:solid 2pt black">
                                            <table  cellspacing="0" cellpadding="0" width="460" style="font-size: 10pt">
                                                    <tr><td colspan="3">&nbsp;</td></tr>                                            
                                                    <tr>
                                                        <td>
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                        <td align="left" valign="top">
                                                            <asp:Label ID="Label3" runat="server" Text="Dealer Number" Width="156px" Font-Bold="true"
                                                                Font-Names="arial"></asp:Label>
                                                            <br />
                                                            <br />
                                                            <table><tr><td valign="bottom">
                                                            <asp:RadioButtonList ID="rdoDealer" runat="server" Font-Names="Arial">
                                                                <asp:ListItem Selected="True" Value="fddfc286032a97e087ac082a03153d106c0d1602b4e3606732670d8b53b32cb548df75a96e5a3a7b2580cb72c520eb5c2fa93dca3a617f53d35ecc41dfe8380f01bc9d651c261f292f50d447d26ef7ca">04282 (Toyota)</asp:ListItem>
                                                                <asp:ListItem Value="d26180deacb1850fd07c7571ec412c4970e960618272e42e9d55773738c7d96348df75a96e5a3a7b2580cb72c520eb5cd567519c8235b853f38991c79d12d6cb01bc9d651c261f292f50d447d26ef7ca">05030 (Toyota Manager)</asp:ListItem>
                                                                <asp:ListItem Value="fddfc286032a97e087ac082a03153d106c0d1602b4e3606732670d8b53b32cb548df75a96e5a3a7b2580cb72c520eb5c1f1a86bb05ff647a88b06c5b4b91c04301bc9d651c261f292f50d447d26ef7ca">63107 (Lexus)</asp:ListItem>
                                                                <asp:ListItem Value="97353518d17748238e8090cf0bb2a3cd6ba80d58a2066942d5c5c633c38f48aa48df75a96e5a3a7b2580cb72c520eb5cefe4994583902c09b48c5f43d1cc12ff01bc9d651c261f292f50d447d26ef7ca">55555 (Invalid)</asp:ListItem>
                                                                <asp:ListItem Value="tok=97353518d17748238e8090cf0bb2a3cd6ba80d58a2066942d5c5c633c38f48aa48df75a96e5a3a7b2580cb72c520eb5cefe4994583902c09b48c5f43d1cc12ff01bc9d651c261f292f50d447d26ef7ca&source_url=na.tis.toyota.com">na.tis.toyota.com</asp:ListItem>                                                                
                                                                <asp:ListItem Value="tok=97353518d17748238e8090cf0bb2a3cd6ba80d58a2066942d5c5c633c38f48aa48df75a96e5a3a7b2580cb72c520eb5cefe4994583902c09b48c5f43d1cc12ff01bc9d651c261f292f50d447d26ef7ca&source_url=t3.tms.toyota.com">t3.tms.toyota.com</asp:ListItem>                                                                                                                                
                                                            </asp:RadioButtonList></td></tr></table>
                                                        </td>
                                                        <td align="left" valign="top">
                                                            <asp:Label ID="Label4" runat="server" Text="Submit To Server" Width="156px" Font-Bold="true"
                                                                Font-Names="arial"></asp:Label>
                                                            <br />
                                                            <br />
                                                            <asp:RadioButtonList ID="rdoEnvironment" runat="server" Font-Names="Arial">
                                                                <asp:ListItem Value="TNEBETA" Selected="True">BETA (Toyota T&E URL)</asp:ListItem>
                                                                <asp:ListItem Value="TNEPROD">PROD (Toyota T&E URL)</asp:ListItem>
                                                                <asp:ListItem Value="ADEBETA">BETA (Toyota ADE URL)</asp:ListItem>
                                                                <asp:ListItem Value="ADEPROD">PROD (Toyota ADE URL)</asp:ListItem>
                                                                <asp:ListItem Value="LTNEBETA">BETA (Lexus T&E URL)</asp:ListItem>
                                                                <asp:ListItem Value="LTNEPROD">PROD (Lexus T&E URL)</asp:ListItem>
                                                                <asp:ListItem Value="LADEBETA">BETA (Lexus ADE URL)</asp:ListItem>
                                                                <asp:ListItem Value="LADEPROD">PROD (Lexus ADE URL)</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" >
                                                            &nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" align="center">
                                                            <asp:Button ID="btnSubmit" runat="server" Text="GO" Width="80px" OnClick="btnSubmit_Click" Font-Size="8pt" Font-Names="Verdana">
                                                            </asp:Button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3">
                                                            &nbsp;</td>
                                                    </tr>
                                                    

                                           </table>                                           
                                        </td>
                                    </tr>
                                </table>
                              </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
