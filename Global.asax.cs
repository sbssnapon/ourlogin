﻿using System;
using System.Collections.Specialized;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Routing;
using Eqs.Configuration;
using log4net;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using OfficeOpenXml.FormulaParsing.Logging;
using PortalManager.Helpers;

namespace PortalManager
{
    public class Global : System.Web.HttpApplication
    {
        static ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        protected void Application_Start(object sender, EventArgs e)
        {
            log4net.Config.XmlConfigurator.Configure();
        }

        void RegisterRoutes(RouteCollection routes)
        {
            routes.MapPageRoute("",
                "PrivacyPolicy",
                "https://sbs.snapon.com/privacy-policy/");
                //"~/PrivacyPolicy/defaut.htm"); 
        }
        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            // Ensure we are using HTTPS at the global level (NOTE: many pages in this app are handling it page-specific but we can remove those
            if (HttpContext.Current.Request.IsSecureConnection.Equals(false) &&
                HttpContext.Current.Request.IsLocal.Equals(false))
            {
                Response.Redirect("https://" + Request.ServerVariables["HTTP_HOST"]
                                             + HttpContext.Current.Request.RawUrl);
            }

            // Restrict OurLogin access
            if (EqsConfigurationManager.IsProduction())
            {
                RestrictAccessToLoginPages();
            }

            // Prevent 
            CheckQuerystringParamsForXssAttackVector();

            string ServerName = Request.ServerVariables["Server_Name"].ToString().ToLower();
            string PortNumber = Request.ServerVariables["SERVER_PORT_SECURE"].ToString();
            string Path = Request.ServerVariables["PATH_INFO"].ToString().ToLower();
            string http = "http";
            string QueryString = Request.ServerVariables["QUERY_STRING"].ToString().Trim().Length > 0 ? "?" + Request.ServerVariables["QUERY_STRING"].ToString().Trim() : string.Empty;

            HttpContext.Current.Response.AddHeader("X-FRAME-OPTIONS", "SAMEORIGIN");

            if (ServerName.Contains("equipmentsolutions.com") || Path.Contains("privacypolicy"))
            {
                HttpContext.Current.Response.Status = "301 Moved Permanently";
                HttpContext.Current.Response.StatusCode = 301;

                if (PortNumber == "1")
                    http = "https";

                if (Path.Contains("privacypolicy"))
                    HttpContext.Current.Response.AddHeader("Location", "https://sbs.snapon.com/privacy-policy/");
                else if (ServerName.IndexOf("rd.") > 0)
                    HttpContext.Current.Response.AddHeader("Location", http + "://sbsprogramsrd.snapon.com" + Path + QueryString);
                else if (ServerName.IndexOf("beta.") > 0)
                    HttpContext.Current.Response.AddHeader("Location", http + "://sbsprogramsbeta.snapon.com" + Path + QueryString);
                else if (ServerName.IndexOf("test.") > 0)
                    HttpContext.Current.Response.AddHeader("Location", http + "://sbsprogramstest.snapon.com" + Path + QueryString);
                else
                    HttpContext.Current.Response.AddHeader("Location", http + "://sbsprograms.snapon.com" + Path + QueryString);

                Response.Flush();
                Response.End();
            }

        }


        protected void Application_EndRequest(object sender, EventArgs e)
        {
            if (Response.Cookies.Count > 0)
            {
                foreach (string s in Response.Cookies.AllKeys)
                {
                    Response.Cookies[s].Secure = true;
                }
            }
        }
 
        protected void Application_Error(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("Unhandled error occured in application. Sender: ");
            sb.AppendLine(Request.RawUrl);
            sb.Append("Query: ");
            sb.AppendLine(Request.QueryString.ToString());

            var exception = Server.GetLastError().GetBaseException();

            log.Error(sb.ToString(), exception);
            log.Error("\n\n");

            if (exception is HttpException && ((HttpException)exception).GetHttpCode() < 500)
            {
                Server.ClearError();
                Response.Redirect("~/404PageNotFound.aspx");
            }
            else
            {
                Server.ClearError();
                Response.Redirect("~/Error.aspx");
            }
        }


        // Login pages are for accessing sites (even prod ecom sites) from this site but that access should not be allowed in PROD
        private void RestrictAccessToLoginPages()
        {
            if (Request.Url.PathAndQuery?.ToLower()?.StartsWith("/ourlogin/default.aspx") ?? false)
            {
                Response.Redirect("~/404PageNotFound.aspx");
            }
        }

        /// <summary>
        /// Checks if any of the Querystring params contains characters that need to be sanitized and if so, redirects to the Error page
        /// </summary>
        private void CheckQuerystringParamsForXssAttackVector()
        {
            var querystrings = Request.QueryString.ToString();

            if (!String.IsNullOrEmpty(querystrings))
            {
                var nameValues = HttpUtility.ParseQueryString(Request.QueryString.ToString());
                bool queryStringWasModified = false;

                foreach (string key in nameValues)
                {
                    // the value here should already be UrlDecoded because the HttpValueCollection (a psuedo- NameValueCollection) does that for us already
                    var value = nameValues[key];

                    if (String.IsNullOrEmpty(value)) continue;

                    // we are mainly concnered in this application with XSS attack from JS in the URL so we Sanitize it and JavascriptEncode
                    var sanitized = Utils.SanitizeAndJavascriptEncodeUrl(value, false);

                    if (sanitized != value)
                    {
                        // Sanitized value is different than the UrlDecoded value so we assume there are things in there we don't want to get through
                        queryStringWasModified = true; 
                    }
                }

                if (queryStringWasModified)
                {
                    // this will redirect to the Error page and log the exception for us
                    throw new Exception("A querystring parameter was found to be unsafe and the Request redirected to the Error page");
                }
            }
        }

    }
}