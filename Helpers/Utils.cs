﻿using Eqs.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace PortalManager.Helpers
{
    public static class Utils
    {
        private const string httpProtocol = "http://";
        private const string httpsProtocol = "https://";

        /// <summary>
        /// Sanitizes the URL or portion of URL and Javascript encodes it to prevent reflected XSS attacks.
        /// Note that it has some very specific logic for this application where it checks if it's a full url or just a path or portio of the url (http:// or https://) because our sanitization method will remove a colon from URL
        /// </summary>
        /// <param name="url"></param>
        /// <returns>string</returns>
        public static string SanitizeAndJavascriptEncodeUrl(string url, bool isFileName)
        {
            if (url == null) return null;

            Uri uriResult = new Uri("https://test.snapon.com"); // just need to initialize something here
            bool isValidUrl = Uri.TryCreate(url, UriKind.Absolute, out uriResult) && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);

            if (isValidUrl)
            {
                url = uriResult.Scheme == Uri.UriSchemeHttp ? url.Remove(0, httpProtocol.Length) : url.Remove(0, httpsProtocol.Length);
            }

            if (isFileName)
            {
                //// this approach of removing illegal characters from the filename is taken from here https://stackoverflow.com/a/146162
                string regexSearch = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
                Regex r = new Regex(string.Format("[{0}]", Regex.Escape(regexSearch)));
                url = r.Replace(url, "");
            }
            
            var sanitizedValue = Sanitize.SanitizeQuerystringParam(url);

            if (isValidUrl)
            {
                sanitizedValue = uriResult.Scheme == Uri.UriSchemeHttp ? sanitizedValue.Insert(0, httpProtocol) : sanitizedValue.Insert(0, httpsProtocol);
            }

            return HttpUtility.JavaScriptStringEncode(sanitizedValue);
        }

    }

}

