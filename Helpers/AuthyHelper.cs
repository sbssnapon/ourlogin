﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text.Json.Serialization;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace PortalManager.Helpers
{
    public class AuthyHelper
    {
        private static string _authyKey = System.Configuration.ConfigurationManager.AppSettings["AuthySecretAPIKey"];
        public static bool VerifyOneTimePassword(string authyUserId, string oneTimePassword, out string message)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("X-Authy-API-Key", _authyKey);

                    var response = client.GetAsync($"https://api.authy.com/protected/json/verify/{oneTimePassword}/{authyUserId}").Result;
                    var responseContent = response.Content;

                    var json = responseContent.ReadAsStringAsync().Result;
                    var authyResponse = JsonSerializer.Deserialize<AuthyOtpResponse>(json);

                    message = authyResponse.Message;
                    return authyResponse.IsSuccessful();
                }
            }
            catch (Exception ex)
            {
                message = "Something went wrong while verifying the OTP: " + ex;
                return false;
            }
        }

        public static bool TryAddUser(string email, string phone, out int userId, out string message)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("X-Authy-API-Key", _authyKey);

                    var requestContent = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("user[email]", email),
                        new KeyValuePair<string, string>("user[cellphone]", phone),
                        new KeyValuePair<string, string>("user[country_code]", "1"),
                    });

                    var response = client.PostAsync("https://api.authy.com/protected/json/users/new", requestContent)
                        .Result;

                    HttpContent responseContent = response.Content;

                    var json = responseContent.ReadAsStringAsync().Result;
                    var authyResponse = JsonSerializer.Deserialize<AuthyAddUserResponse>(json);

                    if (authyResponse.Success)
                    {
                        userId = authyResponse.User.Id;
                        message = authyResponse.Message;
                        return true;
                    }

                    userId = 0;
                    message = authyResponse.Message;
                    return false;
                }
            }
            catch (Exception ex)
            {
                userId = 0;
                message = "Something went wrong while adding user to authy: " + ex;
                return false;
            }
        }

        public static bool TryGetUserStatus(int authyUserId, out AuthyVerifyUserResponse authyResponse)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("X-Authy-API-Key", _authyKey);

                    var response = client.GetAsync($"https://api.authy.com/protected/json/users/{authyUserId}/status").Result;

                    var responseContent = response.Content;

                    var json = responseContent.ReadAsStringAsync().Result;
                    authyResponse = JsonSerializer.Deserialize<AuthyVerifyUserResponse>(json);
                    return authyResponse.Status.Registered;
                }
            }
            catch (Exception ex)
            {
                authyResponse = null;
                return false;
            }
        }
    }

    #region -- Authy Helper Classes --
    public class AuthyOtpResponse
    {
        [JsonPropertyName("message")]
        public string Message { get; set; }

        [JsonPropertyName("token")]
        public string Token { get; set; }

        [JsonPropertyName("success")]
        public object Success { get; set; }

        [JsonPropertyName("device")]
        public Device Device { get; set; }

        [JsonPropertyName("errors")]
        public Errors Errors { get; set; }

        public bool IsSuccessful()
        {
            bool isSuccessful;
            return bool.TryParse(Success.ToString(), out isSuccessful) && isSuccessful;
        }
    }

    public class AuthyAddUserResponse
    {
        [JsonPropertyName("message")]
        public string Message { get; set; }

        [JsonPropertyName("user")]
        public User User { get; set; }

        [JsonPropertyName("success")]
        public bool Success { get; set; }
    }

    public class AuthyVerifyUserResponse
    {
        [JsonPropertyName("status")]
        public Status Status { get; set; }

        [JsonPropertyName("message")]
        public string Message { get; set; }

        [JsonPropertyName("success")]
        public bool Success { get; set; }
    }

    public class DetailedDevice
    {
        [JsonPropertyName("creation_date")]
        public int CreationDate { get; set; }

        [JsonPropertyName("device_id")]
        public int DeviceId { get; set; }

        [JsonPropertyName("device_type")]
        public string DeviceType { get; set; }

        [JsonPropertyName("last_sync_date")]
        public int LastSyncDate { get; set; }

        [JsonPropertyName("os_type")]
        public string OsType { get; set; }

        [JsonPropertyName("registration_device_id")]
        public object RegistrationDeviceId { get; set; }

        [JsonPropertyName("registration_method")]
        public string RegistrationMethod { get; set; }

        [JsonPropertyName("enabled_unlock_methods")]
        public string EnabledUnlockMethods { get; set; }

        [JsonPropertyName("last_unlock_method_used")]
        public string LastUnlockMethodUsed { get; set; }

        [JsonPropertyName("last_unlock_date")]
        public object LastUnlockDate { get; set; }
    }

    public class DeletedDevice
    {
        [JsonPropertyName("creation_date")]
        public int CreationDate { get; set; }

        [JsonPropertyName("deletion_date")]
        public int DeletionDate { get; set; }

        [JsonPropertyName("device_id")]
        public int DeviceId { get; set; }

        [JsonPropertyName("device_type")]
        public string DeviceType { get; set; }

        [JsonPropertyName("last_sync_date")]
        public int LastSyncDate { get; set; }

        [JsonPropertyName("os_type")]
        public string OsType { get; set; }

        [JsonPropertyName("registration_device_id")]
        public int RegistrationDeviceId { get; set; }

        [JsonPropertyName("registration_method")]
        public string RegistrationMethod { get; set; }
    }

    public class Status
    {
        [JsonPropertyName("authy_id")]
        public int AuthyId { get; set; }

        [JsonPropertyName("confirmed")]
        public bool Confirmed { get; set; }

        [JsonPropertyName("registered")]
        public bool Registered { get; set; }

        [JsonPropertyName("country_code")]
        public int CountryCode { get; set; }

        [JsonPropertyName("phone_number")]
        public string PhoneNumber { get; set; }

        [JsonPropertyName("email")]
        public string Email { get; set; }

        [JsonPropertyName("devices")]
        public List<string> Devices { get; set; }

        [JsonPropertyName("has_hard_token")]
        public bool HasHardToken { get; set; }

        [JsonPropertyName("account_disabled")]
        public bool AccountDisabled { get; set; }

        [JsonPropertyName("detailed_devices")]
        public List<DetailedDevice> DetailedDevices { get; set; }

        [JsonPropertyName("deleted_devices")]
        public List<DeletedDevice> DeletedDevices { get; set; }
    }

    public class User
    {
        [JsonPropertyName("id")]
        public int Id { get; set; }
    }


    public class Device
    {
        [JsonPropertyName("city")]
        public string City { get; set; }

        [JsonPropertyName("country")]
        public string Country { get; set; }

        [JsonPropertyName("ip")]
        public string Ip { get; set; }

        [JsonPropertyName("last_unlock_method_used")]
        public string LastUnlockMethodUsed { get; set; }

        [JsonPropertyName("region")]
        public string Region { get; set; }

        [JsonPropertyName("registration_city")]
        public string RegistrationCity { get; set; }

        [JsonPropertyName("registration_country")]
        public string RegistrationCountry { get; set; }

        [JsonPropertyName("registration_device_id")]
        public object RegistrationDeviceId { get; set; }

        [JsonPropertyName("registration_ip")]
        public string RegistrationIp { get; set; }

        [JsonPropertyName("registration_method")]
        public string RegistrationMethod { get; set; }

        [JsonPropertyName("registration_region")]
        public string RegistrationRegion { get; set; }

        [JsonPropertyName("os_type")]
        public string OsType { get; set; }

        [JsonPropertyName("last_account_recovery_at")]
        public object LastAccountRecoveryAt { get; set; }

        [JsonPropertyName("multidevice_enabled")]
        public bool MultideviceEnabled { get; set; }

        [JsonPropertyName("multidevice_updated_at")]
        public object MultideviceUpdatedAt { get; set; }

        [JsonPropertyName("id")]
        public int Id { get; set; }

        [JsonPropertyName("registration_date")]
        public int RegistrationDate { get; set; }

        [JsonPropertyName("last_sync_date")]
        public int LastSyncDate { get; set; }

        [JsonPropertyName("last_unlock_date")]
        public object LastUnlockDate { get; set; }

        [JsonPropertyName("enabled_unlock_methods")]
        public string EnabledUnlockMethods { get; set; }
    }
    public class Errors
    {
        [JsonPropertyName("message")]
        public string message { get; set; }
    }
    #endregion
}