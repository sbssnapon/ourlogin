﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace PortalManager.Helpers
{
    /// <summary>
    /// Source: https://www.c-sharpcorner.com/article/simple-nslookup-implementation-in-C-Sharp/
    /// <title>nslookup</title>
    /// <description>
    /// *nix simple nslookup clone for the Win32 platform (Console Application)
    /// Does A DNS lookup by Host Name or IP. Host Name lookups can return
    /// multiple IP Ranges.
    /// </description>
    /// <author>Doug Bell</author>
    /// <version>1.0</version>
    /// <date>March 23 2002</date>
    /// 
    /// </summary>public class DNSLookup

    public static class NSLookup
    {
        /// <summary>
        /// 
        /// <description>Application entry point.</description>
        /// <param name="args">Host Address, IP Address or -help command line options</param>
        /// <return>int Return code 0 for success -1 for failure or error</return>
        /// 
        /// </summary>
        [STAThread]
        public static string GetNsLookup(string[] args)
        {
            StringBuilder sb = new StringBuilder();
            //Make sure we were passed something, otherwise return help.
            if (args.Length < 1 || args[0].Equals("-help"))
            {
                sb.AppendLine("Usage is: nslookup [Host Name] | [Host IP] | -help");
                sb.AppendLine("nslookup foo.bar.com (Returns IP Address for Host Name)");
                sb.AppendLine("nslookup 123.123.123.123 (Returns Host Name for Address)");
                sb.AppendLine("nslookup -help (Returns this Help Message)");
                //Log.Info(sb.ToString());
                return sb.ToString();
            }
            else
            {
                //We have something, try to look it up....
                try
                {
                    //The IP or Host Entry to lookup
                    IPHostEntry ipEntry;
                    //The IP Address Array. Holds an array of resolved Host Names.
                    IPAddress[] ipAddr;
                    //Value of alpha characters
                    char[] alpha = "aAbBcCdDeEfFgGhHiIjJkKlLmMnNoOpPqQrRsStTuUvVwWxXyYzZ-".ToCharArray();
                    //If alpha characters exist we know we are doing a forward lookup
                    if (args[0].IndexOfAny(alpha) != -1)
                    {
                        ipEntry = Dns.GetHostByName(args[0]);
                        ipAddr = ipEntry.AddressList;
                        sb.AppendLine("\nHost Name : " + args[0]);
                        int i = 0;
                        int len = ipAddr.Length;
                        for (i = 0; i < len; i++)
                        {
                            sb.AppendLine(String.Format("Address {0} : {1} ", i, ipAddr[i].ToString()));
                        }
                        //Log.Info(sb.ToString());
                        return sb.ToString();
                    }
                    //If no alpha characters exist we do a reverse lookup
                    else
                    {
                        ipEntry = Dns.Resolve(args[0]);
                        sb.AppendLine("Address : " + args[0]);
                        sb.AppendLine("Host Name : " + ipEntry.HostName);
                        //Log.Info(sb.ToString());
                        return sb.ToString();
                    }
                }
                catch (System.Net.Sockets.SocketException se)
                {
                    // The system had problems resolving the address passed
                    sb.AppendLine(se.Message.ToString());
                    return sb.ToString();
                }
                catch (System.FormatException fe)
                {
                    // Non unicode chars were probably passed 
                    sb.AppendLine(fe.Message.ToString());
                    return sb.ToString();
                }
            }
        }//End Main(string[])
    }//End NSLookup Class
}