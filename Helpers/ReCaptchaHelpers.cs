﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web;
using Eqs.Configuration;
using Eqs.Utility;

namespace PortalManager.Helpers
{
    public static class ReCaptchaHelpers
    {
        // static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static bool UserCompletedReCaptcha(HttpRequest request, string userId, out string errors)
        {
            errors = null;

            //start building recaptch api call
            var sb = new StringBuilder();
            sb.Append("https://www.google.com/recaptcha/api/siteverify?secret=");

            //our secret key
            var secretKey = System.Configuration.ConfigurationManager.AppSettings["GoogleReCaptchaSecretKey"];
            sb.Append(secretKey);

            //response from recaptch control
            sb.Append("&");
            sb.Append("response=");
            var reCaptchaResponse = request["g-recaptcha-response"];

            if (String.IsNullOrEmpty(reCaptchaResponse))
            {
                return false;
            }
            else
            {
                sb.Append(reCaptchaResponse);
            }

            // if localhost, i.e. developing locally, continue b/c there will be issues with network request otherwise
            if (request.IsLocal) return true;

            //client ip address
            //---- This Ip address part is optional. If you do want to send IP address you can
            //---- Uncomment below 4 lines
            //sb.Append("&");
            //sb.Append("remoteip=");
            //var clientIpAddress = GetUserIp(request);
            //sb.Append(clientIpAddress);

            string logOutput = System.Text.RegularExpressions.Regex.Replace(sb.ToString(), "secret=(.*)&response=", string.Format("secret=XXXXXX&response="));
            //log.Info("ReCaptcha API Call: " + logOutput);

            try
            {
                //make the api call and determine validity
                using (var client = new WebClient())
                {
                    var uri = sb.ToString();
                    var json = client.DownloadString(uri);
                    var serializer = new DataContractJsonSerializer(typeof(RecaptchaApiResponse));
                    var ms = new MemoryStream(Encoding.Unicode.GetBytes(json));
                    var result = serializer.ReadObject(ms) as RecaptchaApiResponse;

                    //--- Check if we are able to call api or not.
                    if (result == null)
                    {
                        //log.Info("Captcha was unable to make the api call");
                        errors = "An error occurred while trying to verify the ReCaptcha.";
                        return false;
                    }
                    else // If Yes
                    {
                        //log.Info("result: " + JsonConvert.SerializeObject(result));
                        //api call contains errors
                        if (result.ErrorCodes != null)
                        {
                            if (result.ErrorCodes.Count > 0)
                            {
                                errors = String.Join(". ", result.ErrorCodes);
                            }

                            return false;
                        }
                        else //api does not contain errors
                        {
                            if (!result.Success) //captcha was unsuccessful for some reason
                            {
                                errors = "Captcha did not pass, please try again.";
                                return false;
                            }
                            else //---- If successfully verified. Do your rest of logic.
                            {
                                return true;
                            }
                        }

                    }

                }
            }
            catch (Exception ex)
            {
                System.Web.HttpBrowserCapabilities browser = request.Browser;
                string s = "Browser Capabilities\n"
                           + "Type = " + browser.Type + "\n"
                           + "Name = " + browser.Browser + "\n"
                           + "Version = " + browser.Version + "\n"
                           + "Major Version = " + browser.MajorVersion + "\n"
                           + "Minor Version = " + browser.MinorVersion + "\n"
                           + "Platform = " + browser.Platform + "\n"
                           + "Is Beta = " + browser.Beta + "\n"
                           + "Is Crawler = " + browser.Crawler + "\n"
                           + "Is AOL = " + browser.AOL + "\n"
                           + "Is Win16 = " + browser.Win16 + "\n"
                           + "Is Win32 = " + browser.Win32 + "\n"
                           + "Supports Frames = " + browser.Frames + "\n"
                           + "Supports Tables = " + browser.Tables + "\n"
                           + "Supports Cookies = " + browser.Cookies + "\n"
                           + "Supports VBScript = " + browser.VBScript + "\n"
                           + "Supports JavaScript = " +
                           browser.EcmaScriptVersion.ToString() + "\n"
                           + "Supports Java Applets = " + browser.JavaApplets + "\n"
                           + "Supports ActiveX Controls = " + browser.ActiveXControls
                           + "\n";
                //log.Error($"Browser Info: {s}");
                //log.Error($"UserID: {userId}");
                //log.Error(ex);

                // Since the error could be the result of Google changing their I.P. address (again) which could result in an error if Snap-On DNS server hasn't caught up to the new I.P.
                // we still want to allow the user to Save
                // Send error email
                var newLine = Environment.NewLine;
                var nslookupInfo = NSLookup.GetNsLookup(new string[] { "www.google.com" });
                Emailer.SendMail(EqsConfigurationManager.ErrorEmailTo,
                    $"Error {System.Configuration.ConfigurationManager.AppSettings["SiteName"]} ReCaptchaHelpers: " + Environment.MachineName,
                    "NOTE: This error is for informational purposes because the user will not experience any error message in the UI." +
                    newLine +
                    "If this is a network error, it is likely because Google has changed their I.P. address for the ReCaptcha (again) and Snap-On DNS servers have yet to catch up." +
                    newLine +
                    "We probably only need to alert Tye of the frequency that these error email occur so that we understand the implications of using Google ReCaptcha in other sites where we cannot simply over-ride the error with try/ctach." +
                    newLine + newLine +
                    "UserID=" + userId + newLine + newLine +
                    "NSLookup Info that may be useful in debugging if google IPs have changed and can be provided to Tye and/or team " +
                    newLine +
                    nslookupInfo + newLine + newLine +
                    ex.Message + newLine + newLine +
                    ex.ToString());

                return true;
            }
        }

        public static string GetUserIp(HttpRequest request)
        {
            var visitorsIpAddr = string.Empty;

            if (request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
            {
                visitorsIpAddr = request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            }
            else if (!string.IsNullOrEmpty(request.UserHostAddress))
            {
                visitorsIpAddr = request.UserHostAddress;
            }

            return visitorsIpAddr;
        }
    }

    [DataContract]
    public class RecaptchaApiResponse
    {
        [DataMember(Name = "success")]
        public bool Success;

        [DataMember(Name = "error-codes")]
        public List<string> ErrorCodes;
    }
}