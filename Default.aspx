﻿<%@ Page Language="C#" AutoEventWireup="true" Codebehind="Default.aspx.cs" Inherits="PortalManager.OurLogin.Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Ecommerce/Catalog Applications</title>
</head>
<body>
    <form>
        <table height="100%" width="100%" style="font-family: verdana;" >
            <tr>
                <td align="center" valign="top">
                    <table border="0" id="Table1">
                        <tr>
                            <td align="center" colspan="5">
                                <img src="Images/SBS_Logo.gif" /></td>
                        </tr>
                        <tr>
                        <tr>
                            <td colspan="5">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="5" align="center" style="font-size: 12pt;font-weight: bold;">Catalog & Ecommerce Web Sites</td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td valign="top" align="center" style="border:solid 2pt black;" bgcolor="#eeebe1">
                                <table height="100%" cellspacing="0" cellpadding="0" width="285" >
                                    <tbody>
                                        <tr bgcolor="#e6dfc9"><td height="25pt" valign="middle" align="center" style="font-size: 11pt;font-weight: bold;border-bottom:solid 2pt black;">Honda</td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                        <tr><td align="center"><a id="hrefHondaAutoCatalog" runat="server" href="https://catalog.snapon.com/Honda/Login.aspx" style="font-size: 10pt;">Auto Catalog</a></td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                        <tr><td align="center"><a id="hrefHondaMarineCatalog" runat="server" href="https://catalog.snapon.com/SbsCatalogs/login.aspx?CatalogType=HondaMarine" style="font-size: 10pt;">Marine Catalog</a></td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                        <tr><td align="center"><a id="hrefHondaCycleCatalog" runat="server" href="https://catalog.snapon.com/SbsCatalogs/login.aspx?CatalogType=HondaMotorCycle" style="font-size: 10pt;">Motor Cycle Catalog</a></td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                        <tr><td align="center"><a id="hrefHondaPandECatalog" runat="server" href="https://catalog.snapon.com/SbsCatalogs/login.aspx?CatalogType=HondaPowerEquipment" style="font-size: 10pt;">Power Equipment Catalog</a></td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                        <tr><td align="center"><a id="hrefHondaEcommerce" runat="server" href="https://sbsprogramsbeta.snapon.com/Programs/Honda/HondaLogin.asp" style="font-size: 10pt;">Ecommerce</a></td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                        <tr><td align="center"><a id="hrefHondaPACT" runat="server"  href="https://honda.snapon.com/HondaAcura/Login/Login.aspx?type=pact&newuser=pact&newp=Pact01" style="font-size: 10pt;">PACT</a></td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                    </tbody>
                                </table>
                            </td>
                            <td width="30pt">&nbsp;</td>
                            <td valign="top" align="center" style="border:solid 2pt black;" bgcolor="#eeebe1">
                                <table height="100%" cellspacing="0" cellpadding="0" width="285" >
                                    <tbody>
                                        <tr bgcolor="#e6dfc9"><td height="25pt" valign="middle" align="center" style="font-size: 11pt;font-weight: bold;border-bottom:solid 2pt black;">Toyota/Lexus</td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                        <tr><td align="center"><a id="hrefToyotaCatalog" runat="server" href="https://catalog.snapon.com/SbsCatalogs/login.aspx?CatalogType=Toyota" style="font-size: 10pt;">Equipment Catalog</a></td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                        <tr><td align="center"><a href="ToyotaAdeLogin.aspx" style="font-size: 10pt;">Ecommerce</a></td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                    </tbody>
                                </table>
                            </td>  
                            <td width="30pt">&nbsp;</td>
                            <td valign="top" align="center" style="border:solid 2pt black;" bgcolor="#eeebe1">
                                <table height="100%" cellspacing="0" cellpadding="0" width="285" >
                                    <tbody>
                                        <tr bgcolor="#e6dfc9"><td height="25pt" valign="middle" align="center" style="font-size: 11pt;font-weight: bold;border-bottom:solid 2pt black;">VW/AUDI</td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                        <tr><td align="center"><a id="hrefVwEquipmentCatalog" runat="server" href="https://catalog.snapon.com/SbsCatalogs/login.aspx?CatalogType=VW" style="font-size: 10pt;">Equipment Catalog</a></td></tr>
                                        <tr><td>&nbsp;</td></tr>                                        
                                        <tr><td align="center"><a id="hrefVwToolCatalog" runat="server" href="https://catalogoem.snapon.com/VwTools/CatalogManager/default.aspx" style="font-size: 10pt;">Tool Catalog</a></td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                        <tr><td align="center"><a id="hrefVwEcommerce" runat="server" href="https://vw.snapon.com" style="font-size: 10pt;">VW Ecommerce</a></td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                        <tr><td align="center"><a id="hrefAudiEcommerce" runat="server" href="https://audi.snapon.com" style="font-size: 10pt;">AUDI Ecommerce</a></td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                    </tbody>
                                </table>
                            </td>    
                        </tr>
                        <tr><td colspan="5" height="30pt">&nbsp;</td></tr>
                        <tr>
                            <td valign="top" style="border:solid 2pt black;" bgcolor="#eeebe1">
                                <table height="100%" cellspacing="0" cellpadding="0" width="285" >
                                    <tbody>
                                        <tr bgcolor="#e6dfc9"><td height="25pt" valign="middle" align="center" style="font-size: 11pt;font-weight: bold;border-bottom:solid 2pt black;">KIA</td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                        <tr><td align="center"><a id="hrefKiaCatalog" runat="server" href="https://catalog.snapon.com/SbsCatalogs/login.aspx?CatalogType=KIA" style="font-size: 10pt;">Equipment/Tool Catalog</a></td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                        <tr><td align="center"><a id="hrefKiaEcommerce" runat="server" href="https://www.kiaspecialtools.com/" style="font-size: 10pt;">Ecommerce</a></td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                        <tr><td align="center"><a id="hrefKiaTechInfo" runat="server" href="https://kiatechinfo.snapon.com/" style="font-size: 10pt;">TechInfo</a></td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                    </tbody>
                                </table>
                            </td>
                            <td width="30pt">&nbsp;</td>
                            <td valign="top" style="border:solid 2pt black;" bgcolor="#eeebe1" >
                                <table height="100%" cellspacing="0" cellpadding="0" width="285" >
                                    <tbody>
                                        <tr bgcolor="#e6dfc9"><td height="25pt" valign="middle" align="center" style="font-size: 11pt;font-weight: bold;border-bottom:solid 2pt black;">Rivian</td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                        <tr><td align="center"><a href="https://catalog.snapon.com/SbsCatalogs/login.aspx?CatalogType=Rivian" style="font-size: 10pt;">Tool Catalog</a></td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                        <tr><td align="center"><a href="https://rivian.com/" style="font-size: 10pt;">Ecommerce</a></td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                    </tbody>
                                </table>
                            </td>
                            <td width="30pt">&nbsp;</td>
                            <td valign="top" style="border:solid 2pt black;" bgcolor="#eeebe1" >
                                <table height="100%" cellspacing="0" cellpadding="0" width="285" >
                                    <tbody>
                                        <tr bgcolor="#e6dfc9"><td height="25pt" valign="middle" align="center" style="font-size: 11pt;font-weight: bold;border-bottom:solid 2pt black;">Porsche</td></tr>
                                        <tr><td>&nbsp;</td></tr>                                        
                                        <tr><td align="center"><a id="hrefPorscheToolCatalog" runat="server" href="https://catalogoem.snapon.com/VwTools/CatalogManager/default.aspx" style="font-size: 10pt;">Tool Catalog</a></td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                        <tr><td align="center"><a id="hrefPorscheEcommerce" runat="server" href="https://porsche.snapon.com" style="font-size: 10pt;">Ecommerce</a></td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>                        
                        <tr><td colspan="5" height="30pt">&nbsp;</td></tr>
                        <tr>
                            <td valign="top" style="border:solid 2pt black;" bgcolor="#eeebe1">
                                <table height="100%" cellspacing="0" cellpadding="0" width="285" >
                                    <tbody>
                                        <tr bgcolor="#e6dfc9"><td height="25pt" valign="middle" align="center" style="font-size: 11pt;font-weight: bold;border-bottom:solid 2pt black;">Harley-Davidson</td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                        <tr><td align="center"><a id="hrefHDEquipmentCatalog" runat="server" href="https://catalog.snapon.com/SbsCatalogs/login.aspx?CatalogType=HD" style="font-size: 10pt;">Equipment Catalog</a></td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                        <tr><td align="center"><a id="hrefHDEcommerce" runat="server" href="https://hd.snapon.com/index.aspx?DealerNumber=1457&ZipCode=85635" style="font-size: 10pt;">Ecommerce</a></td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                    </tbody>
                                </table>
                            </td>
                            <td width="30pt">&nbsp;</td>
                            <td valign="top" style="border:solid 2pt black;" bgcolor="#eeebe1">
                                <table height="100%" cellspacing="0" cellpadding="0" width="285" >
                                    <tbody>
                                        <tr bgcolor="#e6dfc9"><td height="25pt" valign="middle" align="center" style="font-size: 11pt;font-weight: bold;border-bottom:solid 2pt black;">CHRYSLER</td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                        <tr><td align="center"><a id="hrefMoparCatalog" runat="server" href="https://catalog.snapon.com/SbsCatalogs/login.aspx?CatalogType=Mopar" style="font-size: 10pt;">Equipment/Tool Catalog</a></td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                        <tr><td align="center"><a id="hrefMoparEcommerce" runat="server" href="https://moparessentialtools.com/" style="font-size: 10pt;">Ecommerce</a></td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                        <tr><td align="center"><a id="hrefMoparEcommerceConnect" runat="server" href="https://moparessentialtools.com/index.aspx?DealerCode=60559&SourceURL=OurLogin" style="font-size: 10pt;">Ecommerce Dealer CONNECT</a></td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                    </tbody>
                                </table>
                            </td>

                            <td width="30pt">&nbsp;</td>
                            <td valign="top" style="border:solid 2pt black;" bgcolor="#eeebe1">
                                <table height="100%" cellspacing="0" cellpadding="0" width="285" >
                                    <tbody>
                                        <tr bgcolor="#e6dfc9"><td height="25pt" valign="middle" align="center" style="font-size: 11pt;font-weight: bold;border-bottom:solid 2pt black;">GM</td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                        <tr><td align="center"><a id="hrefGmEquipmentCatalog" runat="server" href="https://catalog.snapon.com/SbsCatalogs/login.aspx?CatalogType=GM" style="font-size: 10pt;">Equipment Catalog</a></td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                        <tr><td align="center"><a id="hrefGmEcommerce" runat="server" href="https://gm.snapon.com/" style="font-size: 10pt;">Ecommerce</a></td></tr>
                                        <tr><td>&nbsp;</td></tr>
                                    </tbody>
                                </table>
                            </td>


                        </tr>                        
                    </table>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>

