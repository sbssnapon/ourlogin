<%@ Page Language="C#" AutoEventWireup="true" Codebehind="Login.aspx.cs" Inherits="PortalManager.OurLogin.Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="/Style/default.css?version=1.1" />
    <link rel="stylesheet" type="text/css" href="/Style/jquery-ui-1.8.16.custom.css"  />
    <script type="text/javascript">
        var onloadCallback = function () {
            grecaptcha.render('gReCaptcha', {
                'sitekey': '<%=_googleRecapchaSiteKey%>'
            });
        };
    </script>
</head>
<body style="BACKGROUND-COLOR: #e1eef7 !important;">
    <form runat="server" id="frmLogin">
    <div id="container" style="display: flex; flex-direction: column;">
        <h1 id="h1Heading" runat="server">Ecommerce/Catalog Sites Login</h1>
        <div class="login_contents">
            <div class="login border padding2">
                <h2>Log in</h2>
                <div class="padding_top_bottom"><label for="UserName">UserName</label></div>
                <div class="padding_top_bottom">
                    <asp:TextBox ID="UserName" runat="server" CssClass="inputBox" autocomplete="off"/>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Required" ControlToValidate="UserName" SetFocusOnError="true"></asp:RequiredFieldValidator>
                </div>
                <div class="padding_top_bottom"><label for="Password">Password</label></div>
                <div class="padding_top_bottom">
                    <asp:TextBox ID="Password" runat="server" CssClass="inputBox" TextMode="Password" autocomplete="off" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Required" ControlToValidate="Password" SetFocusOnError="true"></asp:RequiredFieldValidator>
                </div>
                <asp:Panel runat="server" ID="OneTimePasswordPanel">
                    <div class="padding_top_bottom"><label for="OneTimePassword">One Time Password</label></div>
                    <div class="padding_top_bottom">
                        <asp:TextBox ID="OneTimePassword" runat="server" CssClass="inputBox" autocomplete="off" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Required" ControlToValidate="OneTimePassword" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </div>
                </asp:Panel>
                <div ID="reCaptchaContainer" runat="server" style="margin: 7px 0;">
                    <table>
                        <tr>
                            <td><b><asp:Label ID="lblForMessage" runat="server" /></b></td>
                        </tr>
                        <tr>
                            <td>			
                                <div id="gReCaptcha"></div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="padding_top_bottom">        
                    <asp:Button ID="btnSubmit" runat="server" CommandName="Login" Text="Log in" Width="80px" CssClass="button_Login" OnClick="btnSubmit_Click" ></asp:Button>
                </div>
            </div>
        </div>
        <div class="ui-widget ErrorMessage2" id="divErrorMessage" runat="server" align="center"  >
            <div class="ui-state-error ui-corner-all ErrorMessage">
                <span style="float: left; margin-right: .3em;" class="ui-icon ui-icon-alert"></span> 
                <strong>Alert! </strong> <asp:Label ID="lblErrorMessage" runat="server" />
            </div>
        </div>    
    </div>
    </form>
    <script type="text/javascript" src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>
</body>
</html>
