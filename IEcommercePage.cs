﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PortalManager
{
    public interface IEcommercePage
    {
        /// <summary>
        /// Check whether the image exists in the given relative url. If not return the default image.
        /// </summary>
        /// <param name="relativeUrl"></param>
        /// <returns></returns>
        string CheckImageUrl(string relativeUrl);
        bool IsControlVisible(int roleId, string controlId);
        bool IsControlEnabled(int roleId, string controlId);
    }
}