﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net.Http;
using System.Web;
using System.Web.Caching;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using Eqs.Configuration;
using Eqs.Utility;
using System.Reflection;
using log4net;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using Eqs.Core.Business;
using Eqs.Core;

namespace PortalManager
{
    public abstract class BaseEcommercePage : Page, IEcommercePage
    {
        static ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private static readonly string _defaultImage = "SiteImages/NotShown.gif";
        private static readonly string _defaultErrorPageUrl = "~/Html/Error.htm";
        private static readonly string _productMatrixFilePath = "~/SiteXmlFiles/ProductMatrix.xml";
        private static readonly string _jobCodePartsAccountPath = "~/SiteXmlFiles/JobCodes.xml";
        private static readonly string _statesFilePath = "~/SiteXmlFiles/States.xml";
        private List<string> _stateList = null;
        private List<State> _stateAttibuteList = null;
        private static readonly string _siteName = ConfigurationManager.AppSettings["SiteName"];
        private static readonly string _sku = "sku";
        private static readonly string _group = "group";
        private static readonly string _jobCode = "jobcode";
        private static string _saveSession = string.Empty;
        private string _attributeName = string.Empty;
        private const string AntiXsrfTokenKey = "__AntiXsrfToken";
        private const string AntiXsrfUserNameKey = "__AntiXsrfUserName";
        private string _antiXsrfTokenValue;


        # region Fields

        private string m_CustomerBaanNumber;
        private string m_ErrorPageUrl;
        private string m_NoImageUrl;

        private CatalogBO m_CatalogBusiness;
        private CustomerBO m_CustomerBusiness;
        private OrderBO m_OrderBusiness;
        private ShoppingCartBO m_ShoppingCartBusiness;
        private HolidayMessageBO m_HolidayMessage;
        private User m_UserObject;

        #endregion

        #region Properties

        protected bool IsPublicDealer
        {
            get
            {
                if (m_UserObject == null)
                {
                    object userObject = (User)Session["__USER"];
                    if (userObject != null)
                        m_UserObject = (User)userObject;
                }

                if (Session["__isPublicDealer"] != null)
                {
                    return true;
                }
                else if (m_UserObject != null && m_UserObject.BaanNumber.ToLower().Trim() == "public" && m_UserObject.IsDealer)
                {
                    Session["__isPublicDealer"] = "yes";
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        protected bool IsCanadaDealer
        {
            get
            {
                if (Session["__customerBusinessObject"] != null)
                {
                    if (CustomerBusiness.CustomerInfo.IsDealer && CustomerBusiness.CustomerInfo.DealerNumber.StartsWith("499"))
                        return true;
                    else
                        return false;
                }
                else
                {
                    return false;
                }
            }
        }

        protected bool IsCanadaPorscheDealer
        {
            get
            {
                if (Session["__customerBusinessObject"] != null)
                {
                    if (CustomerBusiness.CustomerInfo.IsDealer && CustomerBusiness.CustomerInfo.DealerNumber.StartsWith("952"))
                        return true;
                    else
                        return false;
                }
                else
                {
                    return false;
                }
            }
        }

        protected bool IsCanadaUser
        {
            get
            {
                if (Session["__localeLanguage"] != null && Session["__localeLanguage"].ToString().ToLower().Equals("en-ca")) // Selected Canadian flag  for English language
                {
                    return true;
                }
                else if (Session["__localeLanguage"] != null && Session["__localeLanguage"].ToString().Equals("fr-CA")) // Selected Canadian flag for French-Canadian language
                {
                    return true;
                }
                else if (Session["__customerBusinessObject"] != null && CustomerBusiness.IsCustomerExist)
                {
                    if (CustomerBusiness.ShippingAddress.Country.ToLower().Equals("can"))
                        return true;
                    else
                        return false;
                }
                else
                    return false;
            }
        }

        protected bool IsVwDealerAfterMarketUser
        {
            get
            {
                bool RC = false;

                if (CustomerBusiness.CustomerInfo.DealerNumber.ToUpper().Equals("403704") || CustomerBusiness.CustomerInfo.DealerNumber.ToUpper().Equals("403705") || CustomerBusiness.CustomerInfo.DealerNumber.ToUpper().Equals("403725") || CustomerBusiness.CustomerInfo.DealerNumber.ToUpper().Equals("403734") ||
                    CustomerBusiness.CustomerInfo.DealerNumber.ToUpper().Equals("403738") || CustomerBusiness.CustomerInfo.DealerNumber.ToUpper().Equals("403739") || CustomerBusiness.CustomerInfo.DealerNumber.ToUpper().Equals("403742") || CustomerBusiness.CustomerInfo.DealerNumber.ToUpper().Equals("403745") ||
                    CustomerBusiness.CustomerInfo.DealerNumber.ToUpper().Equals("403746") || CustomerBusiness.CustomerInfo.DealerNumber.ToUpper().Equals("403762") || CustomerBusiness.CustomerInfo.DealerNumber.ToUpper().Equals("403769") || CustomerBusiness.CustomerInfo.DealerNumber.ToUpper().Equals("403776"))
                {
                    RC = true;
                }
                return RC;
            }

        }

        protected string SaveSession
        {
            get
            {
                return _saveSession;
            }
            set
            {
                _saveSession = value;
                Session["__localeLanguage"] = "en";  // Default to English database for tables tblBillingAddress, tblOrderedItems, tblOrderedItems, tblShippingAddress, tblShoppingCartItems, tblShoppingCarts, and tblUserInfo
                                                     // This is being used for application TCI.snapon.com ONLY
            }
        }

        protected bool IsResaleDealer
        {
            get
            {
                bool RC = false;

                if (CustomerBusiness.CustomerInfo.DealerNumber.ToUpper().StartsWith("RES"))
                {
                    RC = true;
                }
                return RC;
            }

        }

        protected bool IsEducationalDealer
        {
            get
            {
                bool RC = false;

                if (CustomerBusiness.CustomerInfo.DealerNumber.ToUpper().StartsWith("EDU"))
                {
                    RC = true;
                }
                return RC;
            }

        }
        protected bool IsDistributorDealer
        {
            get
            {
                bool RC = false;

                if (CustomerBusiness.CustomerInfo.DealerNumber.ToUpper().StartsWith("DIST"))
                {
                    RC = true;
                }
                return RC;
            }

        }

        protected bool IsSpecialDealer
        {
            get
            {
                bool RC = false;
                // RES = Resales, DIST = Distributor, EDU = Education Schools, 8 = CAP Schools, TA = Technical Advisor, X = Fleet
                if (CustomerBusiness.CustomerInfo.DealerNumber.ToUpper().StartsWith("RES") || CustomerBusiness.CustomerInfo.DealerNumber.ToUpper().StartsWith("DIST") || CustomerBusiness.CustomerInfo.DealerNumber.ToUpper().StartsWith("EDU") || CustomerBusiness.CustomerInfo.DealerNumber.ToUpper().StartsWith("8") || CustomerBusiness.CustomerInfo.DealerNumber.ToUpper().StartsWith("TA") || CustomerBusiness.CustomerInfo.DealerNumber.ToUpper().StartsWith("X"))
                {
                    RC = true;
                }
                return RC;
            }
        }


        public User CustomerUser
        {
            get
            {
                if (m_UserObject == null)
                {
                    object __customerUser = Session["__USER"];
                    if (__customerUser == null)
                        m_UserObject = new User();
                    else
                        m_UserObject = (User)__customerUser;
                }
                return m_UserObject;
            }
        }

        protected string CustomerBaanNumber
        {
            get
            {
                if (m_CustomerBaanNumber == null)
                {
                    object __customerBaanNumber = Session["__customerBaanNumber"];
                    if (__customerBaanNumber == null)
                        m_CustomerBaanNumber = string.Empty;
                    else
                        m_CustomerBaanNumber = __customerBaanNumber.ToString();
                }
                return m_CustomerBaanNumber;
            }
            set { m_CustomerBaanNumber = value; }
        }

        protected int RoleId
        {
            get
            {
                /// This part should be implemented based on the User login information - Future Versions
                /// RoleId 0 denotes Anonymous. Roles are also mapped to the ControlRolesMap.xml to restrict
                /// controls being seen by Anonymous users.

                if (CustomerBaanNumber == string.Empty)
                    return 0;
                else
                    return 1;
            }
        }

        protected string ErrorPageUrl
        {
            get
            {
                if (m_ErrorPageUrl == null)
                    m_ErrorPageUrl = _defaultErrorPageUrl;
                return m_ErrorPageUrl;
            }
            set { m_ErrorPageUrl = value; }
        }

        protected string NoImageUrl
        {
            get
            {
                if (m_NoImageUrl == null)
                    m_NoImageUrl = _defaultImage;
                return m_NoImageUrl;
            }
            set { m_NoImageUrl = value; }
        }

        // This should be Cached rather than putting in the session. Once the SQL Server aspnetdb is installed to accomodated Caching
        // change Session to Cache. This would give a significant performance gain.
        public CatalogBO CatalogBusiness
        {
            get
            {
                if (m_CatalogBusiness == null)
                {
                    object __catalogBusinessObject = Session["__catalogBusinessObject"];
                    if (__catalogBusinessObject == null)
                        m_CatalogBusiness = new CatalogBO();
                    else
                        m_CatalogBusiness = (CatalogBO)__catalogBusinessObject;
                }
                return m_CatalogBusiness;
            }
        }

        public CustomerBO CustomerBusiness
        {
            get
            {
                if (m_CustomerBusiness == null)
                {
                    object __customerBusinessObject = Session["__customerBusinessObject"];
                    if (__customerBusinessObject == null)
                    {
                        m_CustomerBusiness = new CustomerBO(CustomerBaanNumber);
                        Session["__customerBusinessObject"] = m_CustomerBusiness;
                    }
                    else
                        m_CustomerBusiness = (CustomerBO)__customerBusinessObject;
                }
                return m_CustomerBusiness;
            }

        }

        public ShoppingCartBO ShoppingCartBusiness
        {
            get
            {
                if (m_ShoppingCartBusiness == null)
                {
                    object __shoppingCartBusinessObject = Session["__shoppingCartBusinessObject"];
                    if (__shoppingCartBusinessObject == null)
                        if (CustomerBaanNumber == string.Empty)
                        {
                            m_ShoppingCartBusiness = new ShoppingCartBO(); // For Anonymous User 
                            // Response.Redirect(ErrorPageUrl, false);
                            // HttpContext.Current.ApplicationInstance.CompleteRequest();
                        }
                        else
                        {
                            string dealerNumber = string.Empty;
                            string programType = Session["__dealerName"] != null ? Session["__dealerName"].ToString().ToLower() : string.Empty;

                            if (programType == "navistar")
                            {
                                if (Session["__USER"] != null)
                                {
                                    User oUser = (User)Session["__USER"];
                                    dealerNumber = oUser.UserID.Substring(0, 1); // This is for Navistar price type of "D"ealer, "Y/U" = Internal, "F"leet, or default blank for Customer
                                }
                            }
                            else if (programType == "mopar")
                            {
                                dealerNumber = Session["__customerBusinessObject"] == null ? string.Empty : IsCanadaUser ? "CAN" : string.Empty;
                            }
                            else
                            {
                                dealerNumber = Session["__customerBusinessObject"] == null ? string.Empty : CustomerBusiness.CustomerInfo.DealerNumber;
                            }
                            m_ShoppingCartBusiness = new ShoppingCartBO(CustomerBaanNumber, programType, dealerNumber);
                        }
                    else
                        m_ShoppingCartBusiness = (ShoppingCartBO)__shoppingCartBusinessObject;
                }
                return m_ShoppingCartBusiness;
            }
        }

        public OrderBO OrderBusiness
        {
            get
            {
                if (m_OrderBusiness == null)
                {
                    object orderBO = Session["__orderBusinessObject"];
                    if (orderBO == null)
                        // customerId can be stored in the cookie in the url instead of storing in the session
                        if (CustomerBaanNumber == string.Empty)
                        {
                            // Redirect to login page in future when Login functionality is added
                            Response.Redirect(ErrorPageUrl, false);
                            HttpContext.Current.ApplicationInstance.CompleteRequest();
                        }
                        else
                            m_OrderBusiness = new OrderBO(CustomerBaanNumber.ToString());
                    else
                        m_OrderBusiness = (OrderBO)orderBO;
                }
                return m_OrderBusiness;
            }
        }

        public HolidayMessageBO HolidayMessageBusiness
        {
            get
            {
                if (m_HolidayMessage == null)
                {
                    object __holidayMessageObject = Session["__holidayMessageObject"];
                    if (__holidayMessageObject == null)
                    {
                        m_HolidayMessage = new HolidayMessageBO();
                        Session["__holidayMessageObject"] = m_HolidayMessage;
                    }
                    else
                        m_HolidayMessage = (HolidayMessageBO)__holidayMessageObject;
                }
                return m_HolidayMessage;
            }
        }

        protected List<string> getStateList
        {
            get
            {
                if (_stateList == null)
                    _stateList = StateList();

                return _stateList;
            }
        }
        protected List<State> getStateAttributeList
        {
            get
            {
                if (_stateAttibuteList == null)
                    _stateAttibuteList = StateList(AttributeName);

                return _stateAttibuteList;
            }
        }
        protected string AttributeName
        {
            get
            {
                return _attributeName;
            }
            set { _attributeName = value; }
        }
        #endregion

        #region IEcommercePage Members

        public List<string> JobCodes()
        {
            object __jobCodes = Cache["__ToyotaEcommerceJobCodes"];
            if (__jobCodes == null)
            {
                List<string> jobCodes = LoadJobCodes("JobCodes", _jobCode);
                CacheDependency CacheDependency = new CacheDependency(Server.MapPath(_jobCodePartsAccountPath));
                Context.Cache.Insert("__ToyotaEcommerceJobCodes", jobCodes, CacheDependency);
                return jobCodes;
            }
            else
                return (List<string>)__jobCodes;

        }


        public List<string> ExcludedItems(string siteName)
        {
            object __excludedItems = Context.Cache["__excludedItems" + siteName];
            if (__excludedItems == null)
            {
                List<string> excludedItems = LoadExcludedItems(siteName, _sku);
                CacheDependency CacheDependency = new CacheDependency(Server.MapPath(_productMatrixFilePath));
                // System.Web.Caching.CacheItemRemovedCallback oRemove = new System.Web.Caching.CacheItemRemovedCallback(this.LoadExcludedItems);
                Context.Cache.Insert("__excludedItems" + siteName, excludedItems, CacheDependency);
                return excludedItems;
            }
            else
                return (List<string>)__excludedItems;

        }

        public List<string> ExcludedGroups(string siteName)
        {
            object __excludedGroups = Context.Cache["__excludedGroups" + siteName];
            if (__excludedGroups == null)
            {
                List<string> excludedGroups = LoadExcludedItems(siteName, _group);
                CacheDependency CacheDependency = new CacheDependency(Server.MapPath(_productMatrixFilePath));
                // System.Web.Caching.CacheItemRemovedCallback oRemove = new System.Web.Caching.CacheItemRemovedCallback(this.LoadExcludedGroups);
                Context.Cache.Insert("__excludedGroups" + siteName, excludedGroups, CacheDependency);
                return excludedGroups;
            }
            else
                return (List<string>)__excludedGroups;
        }

        public string GetPageTitle(List<Group> breadCrumbGroup, Group group)
        {
            string sTitlePage = string.Empty;
            breadCrumbGroup.Reverse();
            sTitlePage = group.GroupName + ", ";

            foreach (Group groupRoot in breadCrumbGroup)
            {
                if (!groupRoot.GroupName.ToLower().Contains("table of content"))
                    if (group.GroupId != groupRoot.GroupId)
                        sTitlePage = sTitlePage + groupRoot.GroupName + ", ";
            }
            sTitlePage = sTitlePage.Substring(0, (sTitlePage.Length - 2));

            return sTitlePage;
        }

        private List<string> StateList()
        {
            object _stateCodes = Cache["__SbsEcommerceStateCodes"];
            if (_stateCodes == null)
            {
                List<string> stateCodes = loadStates("states", "state");
                CacheDependency CacheDependency = new CacheDependency(Server.MapPath(_statesFilePath));
                Context.Cache.Insert("__SbsEcommerceStateCodes", stateCodes, CacheDependency);
                return stateCodes;
            }
            else
                return (List<string>)_stateCodes;

        }
        private List<State> StateList(string attributeName)
        {
            object _stateCodes = Cache["__SbsEcommerceStateCodes"];
            if (_stateCodes == null)
            {
                List<State> stateCodes = loadStates("states", "state", attributeName);
                CacheDependency CacheDependency = new CacheDependency(Server.MapPath(_statesFilePath));
                Context.Cache.Insert("__SbsEcommerceStateCodes", stateCodes, CacheDependency);
                return stateCodes;
            }
            else
                return (List<State>)_stateCodes;

        }

        /// <summary>
        /// Checks whether the image exists in the given location and returns the image url if it exists else returns the default image url.
        /// </summary>
        /// <param name="relativeUrl">Relative Url where the image is to be checked</param>
        /// <returns>Returns the image url if it exists else returns the default image url</returns>
        public string CheckImageUrl(string relativeUrl)
        {
            FileInfo fileInfo = new FileInfo(Server.MapPath(relativeUrl));
            if (!fileInfo.Exists)
                return NoImageUrl;
            else
                return relativeUrl;
        }


        public bool IsControlVisible(int roleId, string controlId)
        {
            //object __controlRoleMapObject = Application["controlRoleMap"];
            //if (__controlRoleMapObject == null)
            //    return false;
            //else
            //{
            //    List<RestrictedControls> restrictedControlsList = (List<RestrictedControls>)__controlRoleMapObject;
            //    foreach(RestrictedControls control in restrictedControlsList)
            //    {                    
            //        if (control.ControlId == controlId)
            //            return ! control.IsRoleExist(roleId);
            //    }
            //    return false;
            //}

            if (roleId == 0)
                return false;
            else
                return true;
        }

        public bool IsControlEnabled(int roleId, string controlId)
        {
            throw new Exception("The method or operation is not implemented.");
        }


        private List<string> loadStates(string CodeName, string fieldType)
        {
            List<string> statesList = new List<string>();


            FileStream filestream = new FileStream(Server.MapPath(_statesFilePath), FileMode.Open, FileAccess.Read);
            XmlTextReader reader = new XmlTextReader(filestream);

            try
            {
                while (reader.Read())
                {
                    if (reader.Name == CodeName)
                    {
                        statesList = ExtractItems(reader.ReadSubtree(), fieldType);
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                filestream.Close();
                reader.Close();
            }
            return statesList;

        }

        private List<State> loadStates(string CodeName, string fieldType, string attributeName)
        {
            List<State> statesList = new List<State>();


            FileStream filestream = new FileStream(Server.MapPath(_statesFilePath), FileMode.Open, FileAccess.Read);
            XmlTextReader reader = new XmlTextReader(filestream);

            try
            {
                while (reader.Read())
                {
                    if (reader.Name == CodeName)
                    {
                        statesList = ExtractItems(reader.ReadSubtree(), fieldType, attributeName);
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                filestream.Close();
                reader.Close();
            }
            return statesList;

        }

        private List<string> LoadJobCodes(string jobCodeName, string fieldType)
        {
            List<string> jobCodes = new List<string>();

            FileStream filestream = new FileStream(Server.MapPath(_jobCodePartsAccountPath), FileMode.Open, FileAccess.Read);
            XmlTextReader reader = new XmlTextReader(filestream);

            try
            {
                while (reader.Read())
                {
                    if (reader.Name == "code")
                    {
                        string _jobCodeName = reader.GetAttribute("name");
                        if (_jobCodeName == jobCodeName)
                        {
                            jobCodes = ExtractItems(reader.ReadSubtree(), fieldType);
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                filestream.Close();
                reader.Close();
            }
            return jobCodes;
        }

        // TODO: Change names generic to Items and Groups
        private List<string> LoadExcludedItems(string siteName, string fieldType)
        {
            List<string> excludedItems = new List<string>();

            FileStream filestream = new FileStream(Server.MapPath(_productMatrixFilePath), FileMode.Open, FileAccess.Read);
            XmlTextReader reader = new XmlTextReader(filestream);

            try
            {
                while (reader.Read())
                {
                    if (reader.Name == "site")
                    {
                        string site = reader.GetAttribute("name");
                        if (site == siteName)
                        {
                            excludedItems = ExtractItems(reader.ReadSubtree(), fieldType);
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                filestream.Close();
                reader.Close();
            }
            return excludedItems;
        }

        private static List<string> ExtractItems(XmlReader reader, string fieldType)
        {
            List<string> itemList = new List<string>();

            while (reader.Read())
            {
                if (reader.Name == fieldType)
                    itemList.Add(reader.ReadString());
            }
            return itemList;
        }

        private static List<State> ExtractItems(XmlReader reader, string fieldType, string attributeName)
        {
            string attribute = string.Empty;
            List<State> itemList = new List<State>();

            while (reader.Read())
            {
                if (reader.Name == fieldType)
                {
                    State state = new State();
                    state.StateAbbreviation = reader.GetAttribute(attributeName);
                    state.StateName = reader.ReadString();
                    itemList.Add(state);
                }

            }
            return itemList;
        }
        public static string GoogleTagManagerId(string applicationType)
        {
            string id = string.Empty;

            if (HttpContext.Current.Session["__isPublic"] != null && HttpContext.Current.Session["__isPublic"].ToString().ToLower() == "public")
                applicationType += "public";

            switch (applicationType.ToLower())
            {
                case "honda":
                    id = "G-3HWR27HJ1V";
                    break;

                case "hondapublic":
                    id = "G-ZPD3TPEJCZ";
                    break;

                case "acura":
                    id = "G-VPLL5Q20V4";
                    break;

                case "acurapublic":
                    id = "G-3TFDZYWTTB";
                    break;

                case "hondalac":
                    id = "G-347XJ9FDBE";
                    break;

                case "hondamarine":
                    id = "G-9NLH43ZGK0";
                    break;

                case "hondacycle":
                    id = "G-XB828Y6M29";
                    break;

                case "hondapande":
                    id = "G-S9P6Y2L91Y";
                    break;

                case "kia":
                    id = "G-YG81KRTJ2L";
                    break;

                case "kiapublic":
                    id = "G-P9X3P8Q0XN";
                    break;

                case "kiatechinfopublic":
                    id = "G-5ZG2PYZ3XK";
                    break;

                case "toyota":
                    id = "G-CW3FEZY5Z8"; // dealer
                    break;

                case "toyotapublic":
                    id = "G-2M7CHZZN1K"; // after market
                    break;

                case "toyotatechinfo":
                    id = "G-H0NWC8DBK7";
                    break;

                case "lexus":
                    id = "G-XS33894HMM";
                    break;

                case "lexuspublic":
                    id = "G-EJRZBTGYKM";
                    break;

                case "vw":
                    id = "G-F6M6TP247T";
                    break;

                case "vwpublic":
                    id = "G-FLVJ9E514D";
                    break;

                case "audi":
                    id = "G-BZPGJH5Z17";
                    break;

                case "audipublic":
                    id = "G-HYM3FTWTRN";
                    break;

                case "sbsprograms":
                    id = "G-F362DMNYNE";
                    break;

                case "allisontransmission":
                    id = "";
                    break;

                case "porsche":
                    id = "G-05FFZLDPC9";
                    break;

                case "porschepublic":
                    id = "G-LNGZ6BKM2S";
                    break;

                case "hd":
                    id = "G-VLZE9JQTKL";
                    break;

                case "hdpublic":
                    id = "G-XN965BC8FM";
                    break;

                case "gm":
                    id = "G-RQTS9K3SHH";
                    break;

                case "gmpublic":
                    id = "G-SX9DNSVJV4";
                    break;

                case "mopar":
                    id = "G-BJMBB55MR4";
                    break;

                case "moparpublic":
                    id = "G-J6P46MNG6Z";
                    break;

            }
            return id;
        }

        public string GetCcmSoldToBusinessPartner(string ProgramName)
        {
            string ccmSoldToBusinessPartner = EqsConfigurationManager.CcmSoldToBusinessPartnerNonDealer;

            if (Session["__isPublic"] != null)
            {
                switch (Session["__isPublic"].ToString().ToLower())
                {
                    case "pact":
                        if (ProgramName == "honda")
                            ccmSoldToBusinessPartner = EqsConfigurationManager.CcmSoldToBusinessPartnerHondaPACT;
                        else
                            ccmSoldToBusinessPartner = EqsConfigurationManager.CcmSoldToBusinessPartnerAcuraPACT;
                        break;

                    case "technician":
                        if (ProgramName == "honda")
                            ccmSoldToBusinessPartner = EqsConfigurationManager.CcmSoldToBusinessPartnerHondaTechnician;
                        else
                            ccmSoldToBusinessPartner = EqsConfigurationManager.CcmSoldToBusinessPartnerAcuraTechnician;
                        break;

                    case "employee":
                        if (ProgramName == "honda")
                            ccmSoldToBusinessPartner = EqsConfigurationManager.CcmSoldToBusinessPartnerHondaEmployee;
                        else
                            ccmSoldToBusinessPartner = EqsConfigurationManager.CcmSoldToBusinessPartnerAcuraEmployee;
                        break;
                }
            }

            return ccmSoldToBusinessPartner;
        }

        #endregion

        // From this https://www.owasp.org/index.php/.NET_Security_Cheat_Sheet#ASP.NET_Web_Forms_Guidance
        protected void Page_Init(object sender, EventArgs e)
        {
            //First, check for the existence of the Anti-XSS cookie
            var requestCookie = Request.Cookies[AntiXsrfTokenKey];
            Guid requestCookieGuidValue;

            //If the CSRF cookie is found, parse the token from the cookie.
            //Then, set the global page variable and view state user
            //key. The global variable will be used to validate that it matches in the view state form field in the Page.PreLoad
            //method.
            if (requestCookie != null && Guid.TryParse(requestCookie.Value, out requestCookieGuidValue))
            {
                //Set the global token variable so the cookie value can be
                //validated against the value in the view state form field in
                //the Page.PreLoad method.
                _antiXsrfTokenValue = requestCookie.Value;

                //Set the view state user key, which will be validated by the
                //framework during each request
                Page.ViewStateUserKey = _antiXsrfTokenValue;
            }
            //If the CSRF cookie is not found, then this is a new session.
            else
            {
                //Generate a new Anti-XSRF token
                _antiXsrfTokenValue = Guid.NewGuid().ToString("N");

                //Set the view state user key, which will be validated by the
                //framework during each request
                Page.ViewStateUserKey = _antiXsrfTokenValue;

                //Create the non-persistent CSRF cookie
                var responseCookie = new HttpCookie(AntiXsrfTokenKey)
                {
                    //Set the HttpOnly property to prevent the cookie from
                    //being accessed by client side script
                    HttpOnly = true,
                    SameSite = SameSiteMode.Strict,

                    //Add the Anti-XSRF token to the cookie value
                    Value = _antiXsrfTokenValue
                };

                //If we are using SSL, the cookie should be set to secure to
                //prevent it from being sent over HTTP connections
                if (FormsAuthentication.RequireSSL && Request.IsSecureConnection)
                    responseCookie.Secure = true;

                //Add the CSRF cookie to the response
                Response.Cookies.Set(responseCookie);
            }

            Page.PreLoad += master_Page_PreLoad;
        }

        // From this https://www.owasp.org/index.php/.NET_Security_Cheat_Sheet#ASP.NET_Web_Forms_Guidance but modified for Ajax requests
        protected void master_Page_PreLoad(object sender, EventArgs e)
        {
            string url = Request.Url.AbsoluteUri;
            string token, tokenCookie;

            try
            {

                // 1. Check if Ajax Post Request or if its a form submit with ViewState in the form (i.e. whether an Asp.Net PostBack or a JavaScript form submit independent of .NET controls). 
                //    It could be an ajax request from Asp.Net button click or a plain JavaScript ajax request (vanilla JS or Jquery) or a Vanilla JavaScript form submit from an Asp.Net page
                if ((EqsHelpers.IsAjaxRequest(HttpContext.Current.Request) || !String.IsNullOrWhiteSpace(Request.Form["__VIEWSTATE"])) && HttpContext.Current.Request.HttpMethod == HttpMethod.Post.Method)
                {
                    try
                    {
                        // The ajax request could be from an Asp.Net LinkButton click which submits an ajax request and has __ViewState in form field or a jquery ajax form submit
                        if (!String.IsNullOrWhiteSpace(Request.Form["__VIEWSTATE"]))
                        {
                            // The anti-CSRF token will either be in ViewState or Request.Form["__VIEWSTATE"]
                            var encryptedToken = !String.IsNullOrEmpty((string)ViewState[AntiXsrfTokenKey])
                                ? (string)ViewState[AntiXsrfTokenKey]
                                : EqsHelpers.GetTokenFromRawViewState(Request.Form["__VIEWSTATE"], AntiXsrfTokenKey);

                            var decryptedAntiXsrfToken = AES256Encryption.DecryptString(encryptedToken);

                            // The UserNameKey will either be in ViewState or Request.Form["__VIEWSTATE"]
                            var userNameKey = !String.IsNullOrEmpty((string)ViewState[AntiXsrfUserNameKey])
                                ? (string)ViewState[AntiXsrfUserNameKey]
                                : EqsHelpers.GetTokenFromRawViewState(Request.Form["__VIEWSTATE"], AntiXsrfUserNameKey);
                            // GetTokenFromRawViewState seems to return the value below when the AntiXsrfUserNameKey should be String.Empty so this is a hack to reset it
                            userNameKey = userNameKey == "?\u0001" ? String.Empty : userNameKey;

                            //Validate the Anti-XSRF token
                            // TODO: JN - re-integrate these into one if statement that throws once we know what's going on with Context.User.Identity.Name (or the ViewState[AntiXsrfUserNameKey]) and why it's erroring in TechInfo GetUid page
                            string UserName = userNameKey == null ? "null" : userNameKey;
                            string UserIdentity = Context.User.Identity.Name == null ? "null" : Context.User.Identity.Name;
                            string AntiXsrfUserName = (string)ViewState[AntiXsrfUserNameKey] == null ? "null" : (string)ViewState[AntiXsrfUserNameKey];

                            if (decryptedAntiXsrfToken != _antiXsrfTokenValue)
                            {
                                throw new XSRFException("Validation of Anti-XSRF token failed due to AntiXsrfToken. (1) \n" +
                                    "decryptedAntiXsrfToken: " + decryptedAntiXsrfToken + " ||  _antiXsrfTokenValue: " + _antiXsrfTokenValue + "\n" +
                                    "userNameKey: " + UserName + "\n" +
                                    "ViewState[AntiXsrfUserNameKey]: " + AntiXsrfUserName + "\n" +
                                    "Context.User.Identity.Name: " + UserIdentity);
                            }
                            else if ((userNameKey ?? String.Empty) != (Context.User.Identity.Name ?? String.Empty))
                            {
                                _log.Warn("Validation of Anti-XSRF token failed due to userNameToken. (2)\n" +
                                     "decryptedAntiXsrfToken: " + decryptedAntiXsrfToken + " ||  _antiXsrfTokenValue: " + _antiXsrfTokenValue + "\n" +
                                     "userNameKey: " + UserName + "\n" +
                                     "ViewState[AntiXsrfUserNameKey]: " + AntiXsrfUserName + "\n" +
                                     "Context.User.Identity.Name: " + UserIdentity);
                            }


                        }
                        else // this is an Ajax request outside of Asp.Net, i.e. a separate JS file
                        {
                            // get header token which was the ViewState value in the form but could be sent as a header in ajax request , otherwise in the body due to Header size limit restrictions                 
                            token = HttpContext.Current.Request.Headers.Get("__RequestVerificationToken");

                            if (String.IsNullOrEmpty(token))
                            {
                                try
                                {
                                    ViewStateData viewStateData = HttpContext.Current.Request.ParseJSONFromBody<ViewStateData>();
                                    token = viewStateData.__ViewState;
                                }
                                catch (Exception ex)
                                {
                                    // Possibly log this
                                    throw new XSRFException("Validation of Anti - XSRF. Attempt to Parse Anti-XSRF token from Ajax request POST body failed." + Environment.NewLine + ex.ToString());
                                }
                            }

                            var decryptedAntiXsrfToken = AES256Encryption.DecryptString(EqsHelpers.GetTokenFromRawViewState(token, AntiXsrfTokenKey));

                            // get cookie token
                            var requestCookie = HttpContext.Current.Request.Cookies["__AntiXsrfToken"];
                            tokenCookie = requestCookie.Value;

                            if (tokenCookie != decryptedAntiXsrfToken) throw new XSRFException("Validation of Anti - XSRF cookie failed from AJAX POST request.");
                        }
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                }
                // 2. If this is an Http POST that is not an ajax request and not part of a .NET PostBack, then something is rotten in Denmark
                else if (HttpContext.Current.Request.HttpMethod == HttpMethod.Post.Method)
                {
                    throw new XSRFException("Validation of Anti - XSRF. An HTTP Post was received that was not a .NET PostBack and was not an Ajax Request. This probably means a form was submitted using click jacking.");
                }
                //3. During the initial page load, add the Anti-XSRF token and user name to the ViewState
                else // is not PostBack and is not an Ajax Post
                {
                    //Set Anti-XSRF token
                    ViewState[AntiXsrfTokenKey] = AES256Encryption.EncryptString(Page.ViewStateUserKey);

                    //If a user name is assigned, set the user name
                    ViewState[AntiXsrfUserNameKey] = Context.User.Identity.Name ?? String.Empty;
                }
            }
            catch (Exception exception)
            {
                throw;
            }
        }

        protected override void OnInit(EventArgs e)
        {
            SetPublicUser();

            // this is needed to initialize its base page class 
            base.OnInit(e);
        }
        /// <summary>
        /// Added to set Session["__isPublic"] = "public" to set price as an after market customer for default
        /// The link http://vw.snapon.com/SpecialToolsDetail.aspx?itemid=4750007 was being executed from Google
        /// and defaulting to dealer pricing since Session["__isPublic"] was not being set
        /// Session["__isPublic"] is being used by VW, Toyota, and HONDA 
        /// </summary>
        private void SetPublicUser()
        {
            if (Session["__customerBaanNumber"] == null)
                if (Session["__isPublic"] == null)
                    Session["__isPublic"] = "public";
            //
            // Toyota url's "na.tis.toyota.com" or "t3.tms.toyota.com" treat as a dealer
            // so reset Session["__isPublic"] to dealer value of null
            //
            if (Session["__isPublicDealer"] != null)
                Session["__isPublic"] = null;
        }

        protected bool ValidJobCode(string jobCode)
        {
            bool RC = false;

            foreach (string _jobCode in JobCodes())
            {
                if (jobCode == _jobCode)
                {
                    RC = true;
                    break;
                }

            }
            return RC;
        }

        protected void FilterExcludedItems(List<Item> list, string siteName)
        {
            foreach (string excludedSku in ExcludedItems(siteName))
            {
                // Always start backwards do to item can be in list multiple times
                //
                for (int i = list.Count - 1; i > -1; i--)
                {
                    if (excludedSku == list[i].Sku)
                    {
                        list.RemoveAt(i);
                    }
                }
            }
        }

        protected void FilterExcludedItems(List<NewItem> list, string siteName)
        {
            foreach (string excludedSku in ExcludedItems(siteName))
            {
                // Always start backwards do to item can be in list multiple times
                //
                for (int i = list.Count - 1; i > -1; i--)
                {
                    if (excludedSku == list[i].Sku)
                    {
                        list.RemoveAt(i);
                    }
                }
            }
        }

        public void FilterExcludedItems(List<SearchResult> list, string siteName)
        {
            foreach (string excludedSku in ExcludedItems(siteName))
            {
                // Always start backwards do to item can be in list multiple times
                //
                for (int i = list.Count - 1; i > -1; i--)
                {
                    if (excludedSku == list[i].value)
                    {
                        list.RemoveAt(i);
                    }
                }
            }
        }

        protected Boolean FilterExcludedItems(string sku, string siteName)
        {
            bool RC = false;

            foreach (string excludedSku in ExcludedItems(siteName))
            {
                if (excludedSku == sku)
                {
                    RC = true;
                    break;
                }
            }
            return RC;
        }

        protected void FilterExcludedGroups(List<Group> list, string siteName)
        {
            //
            // Not being used 
            //
            foreach (string groupName in ExcludedGroups(siteName))
            {
                foreach (Group group in list)
                {
                    if (groupName.Trim() == group.GroupName.Trim())
                    {
                        list.Remove(group);
                        break;
                    }
                }
            }
        }

        protected void FilterExcludedGroups(List<Item> list, string siteName)
        {
            foreach (string groupName in ExcludedGroups(siteName))
            {
                for (int i = list.Count - 1; i > -1; i--)
                {
                    if (groupName.Trim() == list[i].GroupName.Trim())
                    {
                        list.RemoveAt(i);
                    }
                }
            }
        }

        protected void FilterExcludedGroups(List<NewItem> list, string siteName)
        {
            foreach (string groupName in ExcludedGroups(siteName))
            {
                for (int i = list.Count - 1; i > -1; i--)
                {
                    if (groupName.Trim() == list[i].GroupName.Trim())
                    {
                        list.RemoveAt(i);
                    }
                }
            }
        }
        public void FilterExcludedGroups(List<SearchResult> list, string siteName)
        {
            foreach (string groupName in ExcludedGroups(siteName))
            {
                for (int i = list.Count - 1; i > -1; i--)
                {
                    if (groupName.Trim() == list[i].groupName.Trim())
                    {
                        list.RemoveAt(i);
                    }
                }
            }
        }

        // TODO: The following kind of methods can be elminated by having a abstract class Item derive other items from that. 

        protected void FilterExcludedItems(List<Item> list)
        {
            FilterExcludedItems(list, _siteName);
        }

        protected void FilterExcludedItems(List<NewItem> list)
        {
            FilterExcludedItems(list, _siteName);
        }

        protected void FilterExcludedGroups(List<Group> list)
        {
            // Not being used
            FilterExcludedGroups(list, _siteName);
        }

        protected string FindItemMatch(List<ShoppingCartItem> list, string itemNumber)
        {
            string itemMatch = string.Empty;

            foreach (Item item in list)
            {
                if (itemNumber.ToLower() == item.Sku.ToLower())
                {
                    itemMatch = itemNumber.ToLower();
                    break;
                }
            }
            return itemMatch;
        }

        public static string setProgramType(string url)
        {
            string returnString = string.Empty;
            url = url.ToLower();

            if (url.Contains("vwrd.snapon.com") || url.Contains("vwbeta.snapon.com") || url.Contains("vwtest.snapon.com") || url.Contains("vw.snapon.com"))
                returnString = "VW";
            else if (url.Contains("audird.snapon.com") || url.Contains("audibeta.snapon.com") || url.Contains("auditest.snapon.com") || url.Contains("audi.snapon.com"))
                returnString = "AUDI";
            else if (url.Contains("toyotaaderd.snapon.com") || url.Contains("toyotaadebeta.snapon.com") || url.Contains("toyotaadetest.snapon.com") || url.Contains("toyotaade.snapon.com"))
                returnString = "Toyota";
            else if (url.Contains("lexusaderd.snapon.com") || url.Contains("lexusadebeta.snapon.com") || url.Contains("lexusadetest.snapon.com") || url.Contains("lexusade.snapon.com"))
                returnString = "Lexus";
            else if (url.Contains("acurard.snapon.com") || url.Contains("acurabeta.snapon.com") || url.Contains("acuratest.snapon.com") || url.Contains("acura.snapon.com"))
                returnString = "Acura";
            else if (url.Contains("hondard.snapon.com") || url.Contains("hondabeta.snapon.com") || url.Contains("hondatest.snapon.com") || url.Contains("honda.snapon.com"))
                returnString = "Honda";
            else if (url.Contains("navistarrd.snapon.com") || url.Contains("navistarbeta.snapon.com") || url.Contains("navistartest.snapon.com") || url.Contains("navistar.snapon.com"))
                returnString = "Navistar";
            else if (url.Contains("porscherd.snapon.com") || url.Contains("porschebeta.snapon.com") || url.Contains("porschetest.snapon.com") || url.Contains("porsche.snapon.com"))
                returnString = "Porsche";
            else if (url.Contains("hdrd.snapon.com") || url.Contains("hdbeta.snapon.com") || url.Contains("hdtest.snapon.com") || url.Contains("hd.snapon.com"))
                returnString = "HD";
            else if (url.Contains("moparrd.snapon.com") || url.Contains("moparbeta.snapon.com") || url.Contains("mopartest.snapon.com") || url.Contains("mopar.snapon.com"))
                returnString = "Mopar";
            else if (url.Contains("gmrd.snapon.com") || url.Contains("gmbeta.snapon.com") || url.Contains("gmtest.snapon.com") || url.Contains("gm.snapon.com"))
                returnString = "GM";
            else if (url.Contains("tcird.snapon.com") || url.Contains("tcibeta.snapon.com") || url.Contains("tcitest.snapon.com") || url.Contains("tci.snapon.com"))
                returnString = "TCI";
            return returnString;
        }

        protected bool IsValidCID(string CardIdNumber)
        {
            string todayMonth = DateTime.Now.Month.ToString();
            string todayDay = DateTime.Now.Day.ToString();
            string todayYear = DateTime.Now.Year.ToString();
            string CID = todayMonth.Substring(todayMonth.Length - 1, 1) + todayDay.Substring(todayDay.Length - 1, 1) + todayYear.Substring(todayYear.Length - 1, 1);

            if (CardIdNumber == CID)
                return true;
            else
                return false;
        }

        protected void clearMessage(Label lblMessage, HtmlTableCell tdMessage)
        {
            lblMessage.Text = "";
            tdMessage.Style.Add("display", "none");
        }
        protected void showMessage(Label lblMessage, HtmlTableCell tdMessage, string msg)
        {
            lblMessage.Text = msg;
            tdMessage.Style.Add("display", "block");
        }

        protected void clearMessage(Label lblMessage, HtmlGenericControl divMessage)
        {
            lblMessage.Text = "";
            divMessage.Style.Add("display", "none");
        }
        protected void showMessage(Label lblMessage, HtmlGenericControl divMessage, string msg)
        {
            lblMessage.Text = msg;
            divMessage.Style.Add("display", "block");
        }
    }

    public class SearchResult
    {
        private String _value;
        private String _label;
        private int _groupId;
        private String _groupName;

        public string value
        {
            get
            {
                if (_value == null)
                    _value = string.Empty;
                return _value;
            }
            set
            {
                if (value != null)
                    _value = value.Trim();
            }
        }

        public string label
        {
            get
            {
                if (_label == null)
                    _label = string.Empty;
                return _label;
            }
            set
            {
                if (value != null)
                    _label = value.Trim();
            }
        }

        public int groupId
        {
            get
            {
                return _groupId;
            }
            set
            {
                _groupId = value;
            }
        }
        public string groupName
        {
            get
            {
                if (_groupName == null)
                    _groupName = string.Empty;
                return _groupName;
            }
            set
            {
                if (value != null)
                    _groupName = value.Trim();
            }
        }
        public SearchResult()
        {
            _value = String.Empty;
            _label = String.Empty;
            _groupId = 0;
            _groupName = String.Empty;
        }

        public SearchResult(String _value, String _label)
        {
            value = _value;
            label = _label;
        }

    }
}