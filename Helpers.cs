﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace PortalManager
{
    public static class EqsHelpers
    {
        public static bool IsAjaxRequest(this HttpRequest request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }

            var context = HttpContext.Current;
            var isCallbackRequest = false;// callback requests are ajax requests

            if (context != null && context.CurrentHandler != null && context.CurrentHandler is System.Web.UI.Page)
            {
                isCallbackRequest = ((System.Web.UI.Page)context.CurrentHandler).IsCallback;
            }

            return isCallbackRequest || (request["X-Requested-With"] == "XMLHttpRequest") || (request.Headers["X-Requested-With"] == "XMLHttpRequest");
        }

        public static ViewStateData ParseJSONFromBody<ViewStateData>(this System.Web.HttpRequest request)
        {
            string documentContents;

            // must copy the request.InputStream to a MemoryStream so we can read it without making the InputStream unavailable for later reading
            // per this StackOverflow answer https://stackoverflow.com/questions/21971467/why-cant-i-read-http-request-input-stream-twice/21972422
            using (var output = new MemoryStream())
            {
                request.InputStream.CopyTo(output);
                output.Position = 0;
                documentContents = Encoding.UTF8.GetString((output as MemoryStream).ToArray());
            }

            // make sure to reset position since the InputStream may have been read already (for instance in Eqs.Web.Helpers.ParseJSONFromBody)
            request.InputStream.Position = 0;

            return JsonConvert.DeserializeObject<ViewStateData>(documentContents);
        }

        /// <summary>
        /// Get the AntiXsrfTokenKey or the AntiXsrfUserNameKey from the ViewState. If using for anything else, the extensive control value Replace logic will have to be updated
        /// </summary>
        /// <param name="rawViewState"></param>
        /// <param name="tokenKey"></param>
        /// <returns></returns>
        public static string GetTokenFromRawViewState(string rawViewState, string tokenKey)
        {
            var dencryptedToken = "";

            string viewStateString = System.Text.Encoding.ASCII.GetString(Convert.FromBase64String(rawViewState));
            // these are control characters that are used as separators
            var sanitizedViewState = viewStateString.Replace("\u0005@", "|").Replace("\u001e", "|").Replace("\u0016", "|").Replace("\u000f", "|")
                .Replace("\u0012", "|").Replace("e\u0016", "|").Replace("\u0005", "|").Replace("\u001b", "|");
            //.Replace("\u001", "|");
            string[] keysAndValues = sanitizedViewState.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);
            int indexOfAjaxTokenKey = Array.IndexOf(keysAndValues, tokenKey);
            dencryptedToken = keysAndValues[indexOfAjaxTokenKey + 1];

            return dencryptedToken;
        }
    }

    public class ViewStateData
    {
        public string __ViewState { get; set; }  // yyyyMMddHHmmss
    }
}