﻿using System;
using System.Web.Security;
using System.Web;
using System.Web.UI.WebControls;

namespace PortalManager.OurLogin
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
			if (Session["OurLoginValid"] == null)
			{
				Response.Redirect("/Login.aspx", false);
				HttpContext.Current.ApplicationInstance.CompleteRequest();
			}
			else
			{
				// OURLOGIN IS BEING EXECUTED FROM BETA SO LEAVE URL'S AS PRODUCTION
				//
				//string environment = string.Empty;

				//if (Request.Url.AbsoluteUri.ToLower().Contains("rd.snapon.com"))
				//	environment = "RD";
				//else if (Request.Url.AbsoluteUri.ToLower().Contains("beta.snapon.com"))
				//	environment = "BETA";

				//// Honda
				//hrefHondaAutoCatalog.HRef = "http://catalog" + environment + ".snapon.com/Honda/Login.aspx";
				//hrefHondaMarineCatalog.HRef = "http://catalog" + environment + ".snapon.com/SbsCatalogs/login.aspx?CatalogType=HondaMarine";
				//hrefHondaCycleCatalog.HRef = "http://catalog" + environment + ".snapon.com/SbsCatalogs/login.aspx?CatalogType=HondaMotorCycle";
				//hrefHondaPandECatalog.HRef = "http://catalog" + environment + ".snapon.com/SbsCatalogs/login.aspx?CatalogType=HondaPowerEquipment";
				//hrefHondaEcommerce.HRef = "https://sbsprogramsbeta.snapon.com/Programs/Honda/HondaLogin.asp";
				//hrefHondaPACT.HRef = "http://honda" + environment + ".snapon.com/HondaAcura/Login/Login.aspx?type=pact&newuser=pact&newp=Pact01";
				//// Toyota
				//hrefToyotaCatalog.HRef = "http://catalog" + environment + ".snapon.com/SbsCatalogs/login.aspx?CatalogType=Toyota";
				//// VW/AUDI
				//hrefVwEquipmentCatalog.HRef = "http://catalog" + environment + ".snapon.com/SbsCatalogs/login.aspx?CatalogType=VW";
				//hrefVwToolCatalog.HRef = "http://catalog" + environment + ".snapon.com/SbsCatalogVwTools/CatalogManager/default.aspx";
				//hrefVwEcommerce.HRef = "http://vw" + environment + ".snapon.com";
				//hrefAudiEcommerce.HRef = "http://audi" + environment + ".snapon.com";
				//// KIA
				//hrefKiaCatalog.HRef = "http://catalog" + environment + ".snapon.com/SbsCatalogs/login.aspx?CatalogType=KIA";
				//hrefKiaEcommerce.HRef = "http://www" + environment + ".kiaspecialtools.com/";
				//hrefKiaTechInfo.HRef = "http://kiatechinfo" + environment + ".snapon.com";
				//// Navistar
				//hrefNavistarEcommerceUser.HRef = "http://navistar" + environment + ".snapon.com/index.aspx?UserID=U00TS77&Firstname=Corp&Lastname=Version1&Country=840&Lang=en&AccountID=000000000";
				//hrefNavistarEcommerceEmployee.HRef = "http://navistar" + environment + ".snapon.com/index.aspx?UserID=YYYDLS1&Firstname=Corp&Lastname=Version2&Country=840&Lang=en&AccountID=000000000";
				//// Porsche
				//hrefPorscheToolCatalog.HRef = "http://catalog" + environment + ".snapon.com/SbsCatalogVwTools/CatalogManager/default.aspx";
				//hrefPorscheEcommerce.HRef = "http://porsche" + environment + ".snapon.com";
				//// Harley-Davidson
				//hrefHDEquipmentCatalog.HRef = "http://catalog" + environment + ".snapon.com/SbsCatalogs/login.aspx?CatalogType=HD";
				//hrefHDEcommerce.HRef = "http://hd" + environment + ".snapon.com/index.aspx?DealerNumber=1457&ZipCode=85635";
				//// Mopar
				//hrefMoparCatalog.HRef = "http://catalog" + environment + ".snapon.com/SbsCatalogs/login.aspx?CatalogType=Mopar";
				//hrefMoparEcommerce.HRef = "http://mopar" + environment + ".snapon.com/";
				//hrefMoparEcommerceConnect.HRef = "http://mopar" + environment + ".snapon.com/index.aspx?DealerCode=60559";
				//// General Motors
				//hrefGmEcommerce.HRef = "http://gm" + environment + ".snapon.com/";
				//hrefGmEquipmentCatalog.HRef = "http://catalog" + environment + ".snapon.com/SbsCatalogs/login.aspx?CatalogType=GM";
			}
        }
    }
}