<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Error</title>
    <link rel="stylesheet" type="text/css" href="/Style/default.css?version=1.1" />
</head>
<body>
    <div id="container" style="display: flex; flex-direction: column;">
        <h1 id="h1Heading" runat="server">Ecommerce/Catalog Sites Login</h1>
        <div style="width: 100%;" align="center">
            <div class="GeneralLabel">
                <br />
                :( Sorry. The page you are looking for cannot be found.
            <br />
            </div>
            <br />
            <div class="GeneralLabel">To locate what you were looking for, please:</div>
            <div style="width: 240pt;" align="left" class="GeneralLabel5">
                <ul>
                    <li>Call us at 1-800-426-6260</li>
                    <li>Email us at <a href="mailto:equipmentsolutions@snapon.com">equipmentsolutions@snapon.com</a></li>
                </ul>
            </div>
            <br />
        </div>
    </div>
</body>
</html>
